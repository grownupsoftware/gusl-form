package gusl.report;

import gusl.annotations.form.ReportColumnType;
import gusl.annotations.form.UiReportCol;
import gusl.annotations.form.UiReportRow;
import gusl.annotations.form.UiReportTable;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@UiReportTable(title = "hello world", css = "backgroundColor:red")
public class XTest {

    @UiReportCol(type = ReportColumnType.text, label = "X1")
    private String fieldX1;

    @UiReportCol(type = ReportColumnType.text, label = "X2", input = true)
    private String fieldX2;

//    @UiReportRow(css = "border: 1px solid orange")
//    @UiReportCol(type = ReportColumnType.text)
//    private String field3;

}
