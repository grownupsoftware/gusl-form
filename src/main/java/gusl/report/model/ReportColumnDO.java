package gusl.report.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import gusl.annotations.form.breadcrumb.BreadCrumbsDO;
import gusl.chart.ChartDO;
import gusl.form.model.ActionConfigDO;
import gusl.model.decimal.DecimalDO;
import gusl.model.decimal.TotalDO;
import gusl.model.money.MoneyDTO;
import gusl.model.odds.OddsDO;
import lombok.*;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportColumnDO {

    @JsonProperty("f")
    private List<Format> cellFormat;

    @JsonProperty("v")
    private String value;

    @JsonProperty("l")
    private String link;

    @JsonProperty("i")
    private String image;

    @JsonProperty("s")
    private Float scaleFactor;

    @JsonProperty("co")
    private gusl.model.chart.ChartDO oldChart;

    @JsonProperty("c")
    private ChartDO chart;

    @JsonProperty("cjs")
    private Map<String, Object> chartJs;

    @JsonProperty("w")
    private Integer width;

    @JsonProperty("h")
    private Integer height;

    @JsonProperty("css")
    private String css;

    @JsonProperty("ocss")
    private String outerCss;

    @JsonProperty("icss")
    private String innerCss;

    @JsonProperty("p")
    private Long movement;

    @JsonProperty("id")
    private String id;

    @JsonProperty("n")
    private String name;

    @JsonProperty("r")
    private ReportDO report;

    @JsonProperty("m")
    private MoneyDTO money;

    @JsonProperty("d")
    private DecimalDO decimal;

    @JsonProperty("nv")
    private Long numberValue;

    @JsonProperty("dv")
    private Double doubleValue;

    @JsonProperty("ep")
    private String extendedProperties;

    @JsonProperty("cir")
    private Boolean circle;

    @JsonProperty("a")
    private ActionConfigDO action;

    @JsonProperty("pcent")
    private Double percentage;

    @JsonProperty("dt")
    private String date;

    @JsonProperty("df")
    private String dateFormat;

    @JsonProperty("bc")
    private BreadCrumbsDO breadCrumbs;

    @JsonProperty("t")
    private TotalDO total;

    @JsonProperty("if")
    private String iframe;

    @JsonProperty("el")
    private String externalLink;

    @JsonProperty("inp")
    private Boolean input;

    @JsonProperty("inpid")
    private String inputIdentifier;

    @JsonProperty("odds")
    private OddsDO odds;


    public ReportColumnDO(String value) {
        this.value = value;
    }

    public ReportColumnDO(List<Format> cellFormat, String value) {
        this.cellFormat = cellFormat;
        this.value = value;
    }
}
