package gusl.report.model;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;

public class ReportResponseDTO {

//    @UiPosition(fxFlex = 100, nested = true, header = "This is a report")
//    private ReportPanelDTO panel;

    // @UiField(type = FieldType.report, label = "how about this", ignoreIfNotPresent = false)
    @UiField(type = FieldType.report, noLabel = true, ignoreIfNotPresent = false)
    private ReportDO report;

    public ReportResponseDTO() {
    }

    public ReportResponseDTO(ReportDO report) {
        // this.panel = new ReportPanelDTO(report);
        this.report = report;
    }

//    public ReportPanelDTO getPanel() {
//        return panel;
//    }
//
//    public void setPanel(ReportPanelDTO panel) {
//        this.panel = panel;
//    }

    public ReportDO getReport() {
        return report;
    }

    public void setReport(ReportDO report) {
        this.report = report;
    }
}
