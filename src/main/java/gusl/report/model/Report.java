package gusl.report.model;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.form.model.MenuDO;

import javax.ws.rs.core.Response;
import java.util.Map;

public interface Report<QueryDTO> {

    String getCode();

    boolean checkRole(LoggedInUserDO loggedInUser);

    MenuDO getMenuItem(LoggedInUserDO loggedInUser);

    QueryDTO getCriteriaTemplate();

    ReportResponseDTO generateReport(LoggedInUserDO loggedInUser, Map<String, Object> dataMap) throws GUSLErrorException;

    Response exportReportAsPdf(LoggedInUserDO loggedInUser, Map<String, Object> dataMap) throws GUSLErrorException;

    Response exportReportAsExcel(LoggedInUserDO loggedInUser, Map<String, Object> dataMap) throws GUSLErrorException;

    int getMenuOrder();

}
