package gusl.report.model;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;

public class ReportPanelDTO {

    @UiField(type = FieldType.report, label = "how about this", ignoreIfNotPresent = false)
    // @UiField(type = FieldType.report, noLabel = true, ignoreIfNotPresent = false)
    private ReportDO report;

    public ReportPanelDTO() {
    }

    public ReportPanelDTO(ReportDO report) {
        this.report = report;
    }

    public ReportDO getReport() {
        return report;
    }

    public void setReport(ReportDO report) {
        this.report = report;
    }
}
