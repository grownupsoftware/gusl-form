package gusl.report.model;

public enum Format {

    NONE,
    DEFAULT,
    SMALL,
    TITLE,
    COL_HDR,
    COL_HDR_RIGHT,
    TEXT_RIGHT,
    BLANK_ROW,
    BANDED,
    TEXT_LEFT,
    UNDERLINE,
    BORDER_RIGHT,
    GRID,
    SUB_TOTAL,
    NUMBER,
    TOTAL,
    BOLD,
    TOTAL_RIGHT,
    LARGE,
    SUB_HEADER,
    SPECIAL
}
