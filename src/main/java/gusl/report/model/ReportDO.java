package gusl.report.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportDO {

    @JsonProperty("tbl")
    @Singular
    private List<ReportTableDO> tables;

    @JsonProperty("f")
    private List<Format> defaultFormat;

    @JsonProperty("t")
    private String title;

    @JsonProperty("l")
    private String logo;

    @JsonProperty("s")
    private boolean portrait;

    @JsonProperty("i")
    private String backgroundImage;

    @JsonProperty("u")
    private String exportPdfUrl;

    @JsonProperty("e")
    private String exportExcelUrl;

    @JsonProperty("c")
    private String exportCsvUrl;

    @JsonProperty("n")
    private String exportFilename;

}
