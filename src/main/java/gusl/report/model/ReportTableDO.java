package gusl.report.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportTableDO {

    @JsonProperty("t")
    private String title;

    @JsonProperty("f")
    private List<Format> defaultFormat;

    @Singular
    @JsonProperty("r")
    private List<ReportRowDO> rows;

    @JsonProperty("w")
    private float width;

    @JsonProperty("n")
    private int numerOfColumns;

    @JsonProperty("cw")
    private float[] columnWidths;

    @JsonProperty("css")
    private String css;

    @JsonProperty("bdc")
    private String blastDeltaCommand;

    @JsonProperty("k")
    private String keyField;
    @JsonProperty("kv")
    private String keyValue;

    @JsonProperty("ocss")
    private String outerCss;

    @JsonProperty("icss")
    private String innerCss;

}
