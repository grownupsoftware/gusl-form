package gusl.report.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportRowDO {

    @Singular
    @JsonProperty("c")
    private List<ReportColumnDO> columns;

    @JsonProperty("f")
    private List<Format> defaultFormat;

    @JsonProperty("h")
    private Float height;

    @JsonProperty("css")
    private String css;

    @JsonProperty("ocss")
    private String outerCss;

    @JsonProperty("icss")
    private String innerCss;

    public ReportRowDO(List<ReportColumnDO> columns, List<Format> defaultFormat) {
        this.columns = columns;
        this.defaultFormat = defaultFormat;
    }

    public void setValue(int col, String value) {
        // Will throw a runtime exception if out of bounds
        ReportColumnDO column = columns.get(col);
        column.setValue(value);
    }

    public void setValue(int col, String value, Format format) {
        ReportColumnDO column = columns.get(col);
        column.setValue(value);
        if (column.getCellFormat() == null) {
            column.setCellFormat(new ArrayList<>(1));
        }
        column.getCellFormat().add(format);

    }
}
