package gusl.report;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.DateFormats;
import gusl.report.builder.TableBuilder;
import gusl.report.errors.ReportErrors;
import gusl.report.model.Format;
import gusl.report.model.ReportTableDO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static gusl.report.builder.Builder.*;

public class ReportBuilder {

    private static SimpleDateFormat dateFormatter = DateFormats.getFormatFor("dd-MMM HH:mm");

    private static SimpleDateFormat dateRangeFormatter = DateFormats.getFormatFor("dd/MM/yy HH:mm:ss");

    private static SimpleDateFormat repRangeFormatter = DateFormats.getFormatFor("dd-MMM-yy HH:mm:ss");

    public static Date getDate(String date) throws GUSLErrorException {
        try {
            return dateRangeFormatter.parse(date);
        } catch (ParseException ex) {
            throw new GUSLErrorException(ReportErrors.ENCODING_ERROR.getError());
        }
    }

    public static ReportTableDO subHeader(String label) {
        TableBuilder.Builder builder = table()
                .width(35.0f)
                .addRow(row()
                        .addCol(col().value(label).format(Format.SUB_HEADER).build())
                        .build());

        return builder.build();
    }

    public static ReportTableDO smallHeader(String label) {
        TableBuilder.Builder builder = table()
                .width(35.0f)
                .addRow(row()
                        .addCol(col().value(label).format(Format.BOLD, Format.TEXT_LEFT).build())
                        .build());

        return builder.build();
    }

    public static ReportTableDO rangeHeading(String fromDate, String toDate) throws GUSLErrorException {
        return rangeHeading(getDate(fromDate), getDate(toDate));
    }

    public static ReportTableDO rangeHeading(Date fromDate, Date toDate) {
        TableBuilder.Builder builder = table().columns(25.0f, 75.0f)
                .width(35.0f)
                .addRow(row()
                        .addCol(col().value("Range:").format(Format.BOLD).build())
                        .addCol(col().value(repRangeFormatter.format(fromDate) + " to " + repRangeFormatter.format(toDate)).format(Format.BOLD).build())
                        .build());

        return builder.build();
    }

    public static ReportTableDO rangeHeadingAsOne(Date fromDate, Date toDate) {
        TableBuilder.Builder builder = table()
                .addRow(row()
                        .addCol(col().value("Range: " + repRangeFormatter.format(fromDate) + " to " + repRangeFormatter.format(toDate)).format(Format.BOLD, Format.TEXT_LEFT).build())
                        .build());

        return builder.build();
    }

    public static List<ReportTableDO> generateFooter() {
        List<ReportTableDO> tables = new LinkedList<>();

        TableBuilder.Builder iconBuilder = table()
                .addRow(row()
                        .defaultFormat(Format.SMALL)
                        .addCol(col().image("images/footer-plus18.png", 20.0f).build())
                        .addCol(col().image("images/footer-ukgc.png", 20.0f).build())
                        .addCol(col().image("images/footer-gib.png", 20.0f).build())
                        .addCol(col().image("images/footer-gambleaware.png", 20.0f).build())
                        .addCol(col().image("images/footer-secure.png", 30.0f).build())
                        .build());

        String text1_1 = "Lottomart is licensed by the UK Gambling Commission (UKGC), licence 000-051833-R-329042-001, issued 20 Jul 2018.";
        String text1_2 = "Lottomart is operated by Maple International Ventures Limited, registered in Gibraltar at 741C Europort. Company No. 115236.";
        String text2 = "This is a real-money gambling app. Please play responsibly and only bet what you can afford. Gambling can be addictive; learn to recognise the signs and get support by calling +44 808 8020 133 or visiting BeGambleAware.org";

        TableBuilder.Builder textBuilder = table()
                .addRow(row()
                        .defaultFormat(Format.SMALL)
                        .addCol(col().value(text1_1).build())
                        .addCol(col().value(text2).build())
                        .build())
                .addRow(row()
                        .defaultFormat(Format.SMALL)
                        .addCol(col().value(text1_2).build())
                        .addCol(col().value("").build())
                        .build());

        tables.add(iconBuilder.build());
        tables.add(textBuilder.build());

        return tables;

    }

    public static List<ReportTableDO> getHeader() {

        List<ReportTableDO> tables = new LinkedList<>();

        tables.add(generateReportDate(new Date()));

        return tables;

    }

    public static ReportTableDO generateReportDate(Date reportDate) {
        TableBuilder.Builder builder = table().columns(25.0f, 75.0f)
                .width(35.0f)
                .addRow(row()
                        .addCol(col().value("Report Date:").format(Format.BOLD).build())
                        .addCol(col().value(dateFormatter.format(reportDate)).format(Format.BOLD).build())
                        .build());

        return builder.build();
    }

}
