package gusl.report.builder;

import gusl.chart.ChartDO;
import gusl.model.decimal.DecimalDO;
import gusl.model.money.MoneyDTO;
import gusl.report.model.Format;
import gusl.report.model.ReportColumnDO;
import gusl.report.model.ReportDO;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ColumnBuilder {

    public static class Builder {

        private List<Format> defaultFormat;

        private String value = "";

        private Long numberValue;

        private Double doubleValue;

        private String link = "";

        private String image;
        private float scaleFactor = 1.0f;
        private Integer width;
        private Integer height;
        private String css;

        private Long movement;

        private String id;

        private String name;

        private MoneyDTO money;

        private String extendedProperties;

        private DecimalDO decimal;

        private gusl.model.chart.ChartDO oldChart;

        private Map<String, Object> chartJs;

        // Report in a column
        private ReportDO report;

        private ChartDO chart;

        public Builder() {
            defaultFormat = new ArrayList<>();
        }

        public Builder format(Format... cellFormats) {
            if (cellFormats != null) {
                this.defaultFormat.addAll(Arrays.asList(cellFormats));
            }
            return this;
        }

        public Builder report(ReportDO report) {
            this.report = report;
            return this;
        }

        public Builder value(String value) {
            if (value != null) {
                this.value = value;
            }
            return this;
        }

        public Builder css(String css) {
            if (css != null) {
                this.css = css;
            }
            return this;
        }

        public Builder id(String id) {
            if (id != null) {
                this.id = id;
            }
            return this;
        }

        public Builder movement(Long movement) {
            if (movement != null) {
                this.movement = movement;
            } else {
                this.movement = 0L;
            }
            return this;
        }

        public Builder name(String name) {
            if (name != null) {
                this.name = name;
            }
            return this;
        }

        public Builder money(MoneyDTO money) {
            if (money != null) {
                this.money = money;
            }
            return this;
        }

        public Builder money(MoneyDTO money, String extendedProperties) {
            this.money = money;
            this.extendedProperties = extendedProperties;
            return this;
        }

        public Builder extendedProperties(String extendedProperties) {
            this.extendedProperties = extendedProperties;
            return this;
        }

        public Builder decimal(DecimalDO decimal) {
            if (decimal != null) {
                this.decimal = decimal;
            }
            return this;
        }

        public Builder link(String value, String link) {
            if (value != null) {
                this.value = value;
            }
            this.link = link;
            return this;
        }

        public Builder oldChart(gusl.model.chart.ChartDO chart) {
            this.oldChart = chart;
            return this;
        }

        public Builder chart(ChartDO chart) {
            this.chart = chart;
            return this;
        }

        public Builder chartJs(Map<String, Object> chartJs) {
            this.chartJs = chartJs;
            return this;
        }

        public Builder image(String image, int width, int height) {
            this.image = image;
            this.width = width;
            this.height = height;
            return this;
        }

        public Builder image(String image, float scaleFactor) {
            this.image = image;
            this.scaleFactor = scaleFactor;
            return this;
        }

        public Builder value(Integer value) {
            if (value != null) {
                this.numberValue = value.longValue();
            }
            return this;
        }

        public Builder value(Long value) {
            this.numberValue = value;
            return this;
        }

        public Builder value(Double value) {
            this.doubleValue = value;
            return this;
        }

        public Builder value(MoneyDTO value) {
            if (value != null) {
                this.value = formatMoney(value);
                // this.value = String.format("%.2f", value.getValue() / Math.pow(10, value.getCurrency().getDecimalPlaces()));
            }
            return this;
        }

        public Builder value(MoneyDTO value, Format... cellFormats) {
            if (cellFormats != null) {
                this.defaultFormat.addAll(Arrays.asList(cellFormats));
            }
            if (value != null) {
                this.value = formatMoney(value);
                // this.value = String.format("%.2f", value.getValue() / Math.pow(10, value.getCurrency().getDecimalPlaces()));
            }
            return this;
        }

        protected String formatMoney(MoneyDTO value) {
            NumberFormat formatter = NumberFormat.getInstance();
            formatter.setMaximumFractionDigits(value.getCurrency().getDecimalPlaces());
            formatter.setMinimumFractionDigits(value.getCurrency().getDecimalPlaces());
            return formatter.format(value.getValue() / Math.pow(10, value.getCurrency().getDecimalPlaces()));
        }

        public Builder value(String value, Format... cellFormats) {
            if (cellFormats != null) {
                this.defaultFormat.addAll(Arrays.asList(cellFormats));
            }
            if (value != null) {
                this.value = value;
            }
            return this;
        }

        public Builder value(Integer value, Format... cellFormats) {
            if (cellFormats != null) {
                this.defaultFormat.addAll(Arrays.asList(cellFormats));
            }
            if (value != null) {
                this.value = value.toString();
            }
            return this;
        }

        public Builder value(Long value, Format... cellFormats) {
            if (cellFormats != null) {
                this.defaultFormat.addAll(Arrays.asList(cellFormats));
            }
            if (value != null) {
                this.value = value.toString();
            }
            return this;
        }

        public Builder value(Double value, Format... cellFormats) {
            if (cellFormats != null) {
                this.defaultFormat.addAll(Arrays.asList(cellFormats));
            }
            if (value != null) {
                this.value = value.toString();
            }
            return this;
        }

        public ReportColumnDO build() {
            ReportColumnDO cell = new ReportColumnDO(defaultFormat, value);
            cell.setChart(chart);
            cell.setOldChart(oldChart);
            cell.setChartJs(chartJs);
            cell.setImage(image);
            cell.setScaleFactor(scaleFactor);
            cell.setLink(link);
            cell.setHeight(height);
            cell.setWidth(width);
            cell.setCss(css);
            cell.setReport(report);
            cell.setMovement(movement);
            cell.setId(id);
            cell.setName(name);
            cell.setMoney(money);
            cell.setDecimal(decimal);
            cell.setExtendedProperties(extendedProperties);
            cell.setNumberValue(numberValue);
            cell.setDoubleValue(doubleValue);
            return cell;
        }
    }

}
