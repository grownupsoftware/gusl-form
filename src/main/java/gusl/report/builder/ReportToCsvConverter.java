package gusl.report.builder;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.report.model.ReportDO;
import gusl.report.model.ReportRowDO;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static gusl.core.utils.LambdaExceptionHelper.rethrowConsumer;
import static gusl.core.utils.Utils.safeStream;

public class ReportToCsvConverter {
    private static GUSLLogger logger = GUSLLogManager.getLogger(ReportToCsvConverter.class);

    private static final String DELIM = "\t";
    private static final String NL = "\n";

    public static void convertToCsv(ReportDO reportDo, String filename) {
        try {
            StringBuilder csvBuilder = new StringBuilder();

            safeStream(reportDo.getTables())
                    .forEach(rethrowConsumer(table -> {
                        safeStream(table.getRows()).forEach(row -> convertRow(csvBuilder, row));
                    }));

            writeToFile(csvBuilder, filename);

        } catch (Throwable t) {
            logger.error("Failed to convert report", t);
        }
    }

    private static void convertRow(StringBuilder csvBuilder, ReportRowDO headerRow) {

        safeStream(headerRow.getColumns())
                .forEach(col -> {
                    csvBuilder.append(col.getValue()).append(DELIM);
                });
        csvBuilder.append(NL);
    }

    protected static void writeToFile(StringBuilder builder, String fileName) {
        try {
            File file = new File(fileName);
            file.getParentFile().mkdirs();

            Files.write(Paths.get(fileName), builder.toString().getBytes());
            logger.info("Written to: {}", file.getAbsolutePath());
        } catch (IOException ex) {
            logger.error("failed to write file: {}", fileName);
        }
    }

}
