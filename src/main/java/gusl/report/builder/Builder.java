package gusl.report.builder;

public class Builder {

    public static ReportBuilder.Builder report() {
        return new ReportBuilder.Builder();
    }

    public static TableBuilder.Builder table() {
        return new TableBuilder.Builder();
    }

    public static RowBuilder.Builder row() {
        return new RowBuilder.Builder();
    }

    public static ColumnBuilder.Builder col() {
        return new ColumnBuilder.Builder();
    }

}
