package gusl.report.builder;

import gusl.core.utils.Utils;
import gusl.report.model.Format;
import gusl.report.model.ReportColumnDO;
import gusl.report.model.ReportRowDO;
import gusl.report.model.ReportTableDO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TableBuilder {

    public static class Builder {

        private String title;
        private final List<ReportRowDO> rows = new LinkedList<>();
        private List<Format> defaultFormat = new ArrayList<>();
        private float width = 100.0f;
        private float[] columnWidths;
        private String css;

        private String blastDeltaCommand;

        private String keyField;

        private String keyValue;

        public Builder addBlankRow(float height) {
            ReportRowDO row = new ReportRowDO();
            row.setHeight(height);
            //row.setColumns(Collections.singletonList(new ReportColumnDO("")));
            this.rows.add(row);
            return this;
        }

        public Builder addDivider(float height) {
            this.columnWidths = new float[]{48.0f, 4.0f, 48.0f};
            ReportRowDO row = new ReportRowDO();
            row.setHeight(height);
            List<ReportColumnDO> columns = new ArrayList<>();
            columns.add(new ReportColumnDO(" "));
            columns.add(new ReportColumnDO(" --- "));
            columns.add(new ReportColumnDO(" "));
            row.setColumns(columns);
            this.rows.add(row);
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder blastDeltaCommand(String blastDeltaCommand) {
            this.blastDeltaCommand = blastDeltaCommand;
            return this;
        }

        public Builder keyField(String keyField) {
            this.keyField = keyField;
            return this;
        }

        public Builder keyValue(String keyValue) {
            this.keyValue = keyValue;
            return this;
        }

        public Builder columns(float... columnWidths) {
            this.columnWidths = columnWidths;
            return this;
        }

        public Builder width(float width) {
            this.width = width;
            return this;
        }

        public Builder css(String css) {
            this.css = css;
            return this;
        }

        public Builder addRow(ReportRowDO row) {
            this.rows.add(row);
            return this;
        }

        public Builder addRows(List<ReportRowDO> rows) {
            this.rows.addAll(rows);
            return this;
        }

        public Builder defaultFormat(Format... defaultFormats) {
            if (defaultFormats != null) {
                this.defaultFormat.addAll(Arrays.asList(defaultFormats));
            }
            return this;
        }

        public ReportTableDO build() {
            ReportTableDO reportTableDO = new ReportTableDO();
            reportTableDO.setDefaultFormat(defaultFormat);
            reportTableDO.setRows(rows);
            reportTableDO.setTitle(title);
            reportTableDO.setWidth(width);
            reportTableDO.setColumnWidths(columnWidths);
            reportTableDO.setCss(css);
            reportTableDO.setBlastDeltaCommand(blastDeltaCommand);
            reportTableDO.setKeyField(keyField);
            reportTableDO.setKeyValue(keyValue);

            AtomicInteger numCols = new AtomicInteger(0);
            Utils.safeStream(rows).forEach(row -> {
                if (row.getColumns() != null && !row.getColumns().isEmpty() && row.getColumns().size() > numCols.get()) {
                    numCols.set(row.getColumns().size());
                }
            });
            reportTableDO.setNumerOfColumns(numCols.get());

            return reportTableDO;
        }
    }
}
