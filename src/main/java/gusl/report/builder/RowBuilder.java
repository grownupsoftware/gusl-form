package gusl.report.builder;

import gusl.report.model.Format;
import gusl.report.model.ReportColumnDO;
import gusl.report.model.ReportRowDO;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RowBuilder {

    public static class Builder {

        private final List<ReportColumnDO> columns = new LinkedList<>();
        private List<Format> defaultFormat = new ArrayList<>();
        private String css;

        public Builder addCol(ReportColumnDO col) {
            this.columns.add(col);
            return this;

        }

        public Builder addCols(List<ReportColumnDO> columns) {
            this.columns.addAll(columns);
            return this;

        }

        public Builder css(String css) {
            this.css = css;
            return this;
        }

        public Builder defaultFormat(Format... defaultFormats) {
            if (defaultFormats != null) {
                for (Format format : defaultFormats) {
                    this.defaultFormat.add(format);
                }
            }
            return this;
        }

        public ReportRowDO build() {
            ReportRowDO rowDo = new ReportRowDO(columns, defaultFormat);
            rowDo.setCss(css);
            return rowDo;
        }
    }
}
