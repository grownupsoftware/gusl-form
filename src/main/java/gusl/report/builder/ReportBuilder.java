package gusl.report.builder;

import gusl.report.model.Format;
import gusl.report.model.ReportDO;
import gusl.report.model.ReportRowDO;
import gusl.report.model.ReportTableDO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ReportBuilder {

    public static class Builder {

        private String title;
        private String logo;
        private boolean portrait = true;
        private String exportUrl;
        private String exportFilename;

        private final List<ReportTableDO> tables = new LinkedList<>();
        List<Format> defaultFormat = new ArrayList<>();

        private String backgroundImage;

        public Builder portrait() {
            this.portrait = true;
            return this;
        }

        public Builder landscape() {
            this.portrait = false;
            return this;
        }

        public Builder backgroundImage(String backgroundImage) {
            this.backgroundImage = backgroundImage;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder exportUrl(String exportUrl) {
            this.exportUrl = exportUrl;
            return this;
        }

        public Builder exportFilename(String exportFilename) {
            this.exportFilename = exportFilename;
            return this;
        }

        public Builder logo(String logo) {
            this.logo = logo;
            return this;
        }

        public Builder gap(float height) {
            ReportTableDO table = new ReportTableDO();
            ReportRowDO row = new ReportRowDO();
            row.setHeight(height);
            table.getRows().add(row);
            this.tables.add(table);
            return this;

        }

        public Builder addTable(ReportTableDO table) {
            this.tables.add(table);
            return this;
        }

        public Builder addTables(List<ReportTableDO> table) {
            this.tables.addAll(table);
            return this;
        }

        public Builder defaultFormat(Format... defaultFormats) {
            if (defaultFormats != null) {
                this.defaultFormat.addAll(Arrays.asList(defaultFormats));
            }
            return this;
        }

        public ReportDO build() {
            ReportDO reportDo = new ReportDO();
            reportDo.setDefaultFormat(defaultFormat);
            reportDo.setTables(tables);
            reportDo.setLogo(logo);
            reportDo.setTitle(title);
            reportDo.setPortrait(portrait);
            reportDo.setBackgroundImage(backgroundImage);
            reportDo.setExportPdfUrl(exportUrl);
            reportDo.setExportExcelUrl(exportUrl);
            reportDo.setExportCsvUrl(exportUrl);
            reportDo.setExportFilename(exportFilename);
            return reportDo;
        }

    }
}
