package gusl.report.builder;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.report.errors.ReportErrors;
import gusl.report.model.ReportDO;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.write.*;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipOutputStream;

public class ReportToExcelConverter {

    protected static final GUSLLogger logger = GUSLLogManager.getLogger(ReportConverter.class);

    public static final int ID_COLUMN_WIDTH = 20;
    public static final int AMOUNT_COLUMN_WIDTH = 15;
    public static final int NAME_COLUMN_WIDTH = 25;
    public static final int TYPE_COLUMN_WIDTH = 15;
    public static final int DATE_COLUMN_WIDTH = 20;
    public static final int SMALL_COLUMN_WIDTH = 12;
    public static final int LARGE_COLUMN_WIDTH = 60;

    private static WritableCellFormat AMOUNT_FORMAT;
    private static WritableCellFormat NUMBER_FORMAT;
    private static WritableCellFormat PERC_FORMAT;
    private static WritableCellFormat RATE_FORMAT;
    private static WritableCellFormat HEADER_CELL;
    private static WritableCellFormat MAIN_HEADER_CELL;
    private static WritableCellFormat RIGHT_JUSTIFY_HEADER_CELL;
    private static WritableCellFormat LEFT_JUSTIFY_CELL;
    private static WritableCellFormat DATE_FORMAT;

    private static boolean initialised = false;

    public static void initialise() {
        if (!initialised) {
            initialised = true;
            AMOUNT_FORMAT = new WritableCellFormat(new NumberFormat("#,###,###,##0.00"));
            NUMBER_FORMAT = new WritableCellFormat(new NumberFormat("0"));
            PERC_FORMAT = new WritableCellFormat(new NumberFormat("0.00%"));
            RATE_FORMAT = new WritableCellFormat(new NumberFormat("0.000"));

            try {
                WritableFont cellFont = new WritableFont(WritableFont.ARIAL, 10);
                cellFont.setBoldStyle(WritableFont.BOLD);
                HEADER_CELL = new WritableCellFormat(cellFont);

                WritableFont mainHdrCellFont = new WritableFont(WritableFont.ARIAL, 12);
                mainHdrCellFont.setBoldStyle(WritableFont.BOLD);
                MAIN_HEADER_CELL = new WritableCellFormat(mainHdrCellFont);

                WritableFont amountHdrCellFont = new WritableFont(WritableFont.ARIAL, 10);
                amountHdrCellFont.setBoldStyle(WritableFont.BOLD);
                RIGHT_JUSTIFY_HEADER_CELL = new WritableCellFormat(amountHdrCellFont);
                RIGHT_JUSTIFY_HEADER_CELL.setAlignment(jxl.format.Alignment.RIGHT);

                WritableFont leftJustifyCellFont = new WritableFont(WritableFont.ARIAL, 10);
                LEFT_JUSTIFY_CELL = new WritableCellFormat(leftJustifyCellFont);
                LEFT_JUSTIFY_CELL.setAlignment(Alignment.LEFT);

                DateFormat customDateFormat = new DateFormat("dd MMM yyyy hh:mm:ss");
                DATE_FORMAT = new WritableCellFormat(customDateFormat);

            } catch (WriteException ex) {
                // This will not happen
                logger.warn(ex);
            }
        }
    }

    public static void convertToExcel(ReportDO reportDo, String filename) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            convertToExcel(reportDo, baos);
            try (OutputStream outputStream = new FileOutputStream(filename)) {
                baos.writeTo(outputStream);
            }
        } catch (Throwable t) {
            logger.error("Failed to convert report", t);
        }
    }

    public static void convertToExcel(ReportDO reportDo, ZipOutputStream outputStream) throws GUSLErrorException {
        try {
            initialise();
            WritableWorkbook workbook = Workbook.createWorkbook(outputStream);
            convertReport(reportDo, workbook);
            workbook.write();
            workbook.close();
            logger.info("Written excel");

        } catch (WriteException | IOException ex) {
            logger.error("Error generating profit and loss summary", ex);
            throw new GUSLErrorException(ReportErrors.GENERAL_IO_ERROR.getError());
        }
    }

    public static void convertToExcel(ReportDO reportDo, ByteArrayOutputStream baos) throws GUSLErrorException {
        try {
            initialise();
            WritableWorkbook workbook = Workbook.createWorkbook(baos);
            convertReport(reportDo, workbook);
            workbook.write();
            workbook.close();
            logger.info("Written excel");

        } catch (WriteException | IOException ex) {
            logger.error("Error generating profit and loss summary", ex);
            throw new GUSLErrorException(ReportErrors.GENERAL_IO_ERROR.getError());
        }
    }

    private static void convertReport(ReportDO report, WritableWorkbook workbook) {
        try {
            int sheetNo = 1;
            WritableSheet sheet = workbook.createSheet("Sheet 1", sheetNo);

            int colCnt = 0;
            int rowCnt = 0;

            sheet.setColumnView(0, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(1, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(2, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(3, SMALL_COLUMN_WIDTH);
            sheet.setColumnView(4, SMALL_COLUMN_WIDTH);
            sheet.setColumnView(5, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(6, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(7, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(8, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(9, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(10, AMOUNT_COLUMN_WIDTH);
            sheet.setColumnView(11, SMALL_COLUMN_WIDTH);

            sheet.addCell(new Label(0, rowCnt, "ReportConfig Date", HEADER_CELL));

        } catch (WriteException ex) {
            logger.warn(ex);
        }
    }

}
