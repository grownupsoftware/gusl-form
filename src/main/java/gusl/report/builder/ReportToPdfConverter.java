package gusl.report.builder;

import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import gusl.core.lambda.MutableInteger;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.IOUtils;
import gusl.model.chart.ChartDO;
import gusl.model.chart.ChartDatasetDO;
import gusl.report.model.Format;
import gusl.report.model.ReportColumnDO;
import gusl.report.model.ReportDO;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static gusl.core.utils.LambdaExceptionHelper.rethrowConsumer;
import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;

public class ReportToPdfConverter {
    protected static final GUSLLogger logger = GUSLLogManager.getLogger(ReportConverter.class);

    public static void convertToPdf(ReportDO reportDo, String filename) {
        try {

            Document document = new Document(reportDo.isPortrait() ? PageSize.A4 : PageSize.A4.rotate());
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(filename));
            document.open();
            if (reportDo.getBackgroundImage() != null && !reportDo.getBackgroundImage().isEmpty()) {
                pdfWriter.setPageEvent(new ImageBackgroundHelper(reportDo.getBackgroundImage()));
            }
            convertToPdf(reportDo, pdfWriter, document);

            document.close();
        } catch (Throwable t) {
            logger.error("Failed to convert report", t);
        }
    }

    public static void convertToPdf(ReportDO reportDo, ByteArrayOutputStream baos) {
        try {

            Document document = new Document(reportDo.isPortrait() ? PageSize.A4 : PageSize.A4.rotate());
            PdfWriter pdfWriter = PdfWriter.getInstance(document, baos);
            document.open();
            if (reportDo.getBackgroundImage() != null && !reportDo.getBackgroundImage().isEmpty()) {
                pdfWriter.setPageEvent(new ImageBackgroundHelper(reportDo.getBackgroundImage()));
            }

            convertToPdf(reportDo, pdfWriter, document);
            document.close();
        } catch (Throwable t) {
            logger.error("Failed to convert report", t);
        }
    }

    private static void convertToPdf(ReportDO reportDo, PdfWriter pdfWriter, Document document) throws Exception {
        Font defaultFont = getFont(reportDo.getDefaultFormat(), true);
        int defaultAlignment = getAlignment(reportDo.getDefaultFormat());
        int defaultBorder = getBorder(reportDo.getDefaultFormat());

        doHeading(document, reportDo.getTitle(), reportDo.getLogo());

        if (reportDo.getTables() != null && !reportDo.getTables().isEmpty()) {
            safeStream(reportDo.getTables()).forEach(rethrowConsumer(table -> {

                Font tableFont = getFont(defaultFont, table.getDefaultFormat());
                int tableAlignment = getAlignment(defaultAlignment, table.getDefaultFormat());
                int tableBorder = getBorder(defaultBorder, table.getDefaultFormat());

                if (table.getTitle() != null) {
                    addTitle(document, tableFont, tableAlignment, table.getTitle());
                }

                if (table.getRows() != null && !table.getRows().isEmpty()) {
                    int numCols = table.getNumerOfColumns();

                    if (numCols == 0 && table.getColumnWidths() == null) {
                        if (table.getRows() != null && !table.getRows().isEmpty() && table.getRows().get(0).getHeight() > 0.0f) {
                            blankRow(document, table.getRows().get(0).getHeight());
                        }
                    } else {

                        PdfPTable pdfTable;
                        if (table.getColumnWidths() != null) {
                            pdfTable = new PdfPTable(table.getColumnWidths());
                        } else {
                            pdfTable = new PdfPTable(numCols);
                        }
                        // fixme - should be able to align anywhere
                        pdfTable.setHorizontalAlignment(Element.ALIGN_LEFT);

                        pdfTable.setWidthPercentage(table.getWidth());

                        safeStream(table.getRows()).forEach(rethrowConsumer(row -> {
                            Font rowFont = getFont(tableFont, row.getDefaultFormat());
                            int rowAlignment = getAlignment(tableAlignment, row.getDefaultFormat());
                            int rowBorder = getBorder(tableBorder, row.getDefaultFormat());

                            safeStream(row.getColumns()).forEach(rethrowConsumer(col -> {
                                Font colFont = getFont(rowFont, col.getCellFormat());
                                int colAlignment = getAlignment(rowAlignment, col.getCellFormat());
                                int colBorder = getBorder(rowBorder, col.getCellFormat());

                                addCell(pdfWriter, pdfTable, colFont, colAlignment, colBorder, col);
                            }));
                        }));
                        document.add(pdfTable);
                    }
                }
            }));
        }

    }

    private static JFreeChart convertChart(ChartDO chart) {
        switch (chart.getType()) {
            case "bar":
                // return generatePieChart();
                return convertBarChart(chart, false);
            case "horizontalbar":
                // return generatePieChart();
                return convertBarChart(chart, true);
            case "line":
                // return generatePieChart();
                return convertLineChart(chart);
            case "pie":
                return convertPieChart(chart);
        }

        return null;
    }

    private static DefaultCategoryDataset createDataset(ChartDO chart) {
        final String[] labels = chart.getLabels().toArray(new String[chart.getLabels().size()]);

        DefaultCategoryDataset jfDataset = new DefaultCategoryDataset();
        safeStream(chart.getDatasets()).forEach(dataset -> {
            MutableInteger counter = new MutableInteger(0);
            safeStream(dataset.getData()).forEach(value -> {
                if (labels.length > counter.get()) {
                    jfDataset.addValue(value, dataset.getLabel(), labels[counter.get()]);
                } else {
                    jfDataset.addValue(value, dataset.getLabel(), " ");
                }
                counter.increment();
            });
        });

        return jfDataset;
    }

    private static JFreeChart convertBarChart(ChartDO chart, boolean horizontal) {
        boolean hasTitle = chart.getTitle() != null && !chart.getTitle().isEmpty();

        JFreeChart barChart = ChartFactory.createBarChart(
                chart.getTitle() == null ? "" : chart.getTitle(),
                chart.getXAxisLabel(), chart.getYAxisLabel(),
                createDataset(chart),
                horizontal ? PlotOrientation.HORIZONTAL : PlotOrientation.VERTICAL, hasTitle, false, false);

        barChart.setBorderVisible(false);
        barChart.getPlot().setBackgroundPaint(Color.WHITE);
        barChart.getPlot().setOutlinePaint(Color.WHITE);

        return barChart;
    }

    private static JFreeChart convertLineChart(ChartDO chart) {

        boolean hasTitle = chart.getTitle() != null && !chart.getTitle().isEmpty();

        JFreeChart lineChart = ChartFactory.createLineChart(
                chart.getTitle() == null ? "" : chart.getTitle(),
                chart.getXAxisLabel(), chart.getYAxisLabel(),
                createDataset(chart),
                PlotOrientation.VERTICAL,
                hasTitle, true, false);

        lineChart.setBorderVisible(false);
        lineChart.getPlot().setBackgroundPaint(Color.WHITE);
        lineChart.getPlot().setOutlinePaint(Color.WHITE);

        return lineChart;
    }

    private static JFreeChart convertPieChart(ChartDO chart) {
        boolean hasTitle = chart.getTitle() != null && !chart.getTitle().isEmpty();

        final String[] labels = chart.getData().getLabels();

        if (chart.getData().getDatasets() == null || chart.getData().getDatasets().isEmpty()) {
            logger.error("No dataset defined for pie chart");
            return null;
        }
        final ChartDatasetDO dataset = chart.getData().getDatasets().get(0);

        DefaultPieDataset dataSet = new DefaultPieDataset();

        MutableInteger counter = new MutableInteger(0);
        safeStream(dataset.getData()).forEach(value -> {
            if (labels.length > counter.get()) {
                dataSet.setValue(labels[counter.get()], value);
            } else {
                dataSet.setValue("", value);
            }
            counter.increment();
        });

        JFreeChart pieChart = ChartFactory.createPieChart(
                chart.getTitle() == null ? "" : chart.getTitle(),
                dataSet, hasTitle, true, false);

        pieChart.setBorderVisible(false);
        pieChart.getPlot().setBackgroundPaint(Color.WHITE);
        pieChart.getPlot().setOutlinePaint(Color.WHITE);

        return pieChart;
    }

    public static JFreeChart generatePieChart() {
        DefaultPieDataset dataSet = new DefaultPieDataset();
        dataSet.setValue("China", 19.64);
        dataSet.setValue("India", 17.3);
        dataSet.setValue("United States", 4.54);
        dataSet.setValue("Indonesia", 3.4);
        dataSet.setValue("Brazil", 2.83);
        dataSet.setValue("Pakistan", 2.48);
        dataSet.setValue("Bangladesh", 2.38);

        JFreeChart chart = ChartFactory.createPieChart(
                "World Population by countries", dataSet, true, true, false);

        return chart;
    }

    public static JFreeChart generateBarChart() {
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        dataSet.setValue(791, "Population", "1750 AD");
        dataSet.setValue(978, "Population", "1800 AD");
        dataSet.setValue(1262, "Population", "1850 AD");
        dataSet.setValue(1650, "Population", "1900 AD");
        dataSet.setValue(2519, "Population", "1950 AD");
        dataSet.setValue(6070, "Population", "2000 AD");

        JFreeChart chart = ChartFactory.createBarChart(
                "World Population growth", "Year", "Population in millions",
                dataSet, PlotOrientation.VERTICAL, false, true, false);

        return chart;
    }

//    public static void writeChartToPDF(JFreeChart chart, int width, int height, PdfWriter writer, Document document) {
//
//        try {
//            PdfContentByte contentByte = writer.getDirectContent();
//            PdfTemplate template = contentByte.createTemplate(width, height);
//            Graphics2D graphics2d = template.createGraphics(width, height,
//                    new DefaultFontMapper());
//            Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width,
//                    height);
//
//            chart.draw(graphics2d, rectangle2d);
//
//            graphics2d.dispose();
//            contentByte.addTemplate(template, 0, 0);
//
//        } catch (Exception ex) {
//            logger.error("Error saving chart to pdf", ex);
//        }
//    }

    private static void blankRow(Document document, float height) throws DocumentException {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100.0f);
        PdfPCell cell = new PdfPCell(new Phrase(""));
        cell.setFixedHeight(height);
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell);
        document.add(table);
    }

    private static void addTitle(Document document, Font font, int colAlignment, String title) throws DocumentException {
        Font bold = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 14, BaseColor.BLACK);
        PdfPTable pdfTable = new PdfPTable(1);
        pdfTable.setWidthPercentage(100.0f);
        PdfPCell cell = new PdfPCell(new Phrase(title, bold));
        cell.setBorder(Rectangle.NO_BORDER);
        if (colAlignment != 0) {
            cell.setHorizontalAlignment(colAlignment);
        }
        pdfTable.addCell(cell);
        PdfPCell blankCell = new PdfPCell(new Phrase(""));
        blankCell.setFixedHeight(5.0f);
        blankCell.setBorder(Rectangle.NO_BORDER);
        pdfTable.addCell(blankCell);
        document.add(pdfTable);
    }

    private static void addCell(PdfWriter writer, PdfPTable table, Font colFont, int colAlignment, int colBorder, ReportColumnDO column) throws BadElementException, IOException, URISyntaxException {
        if (column.getLink() != null && !column.getLink().isEmpty()) {
            PdfPCell cell = new PdfPCell(new Phrase(column.getValue(), colFont));
            cell.setBorder(colBorder);
            if (colAlignment != 0) {
                cell.setHorizontalAlignment(colAlignment);
            }
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setCellEvent(new LinkInCell(column.getLink()));
            table.addCell(cell);

        } else if (column.getImage() != null && !column.getImage().isEmpty()) {

            com.itextpdf.text.Image img;
            if (column.getImage().startsWith("http")) {
                img = Image.getInstance(new URL(column.getImage()));
            } else {
                URL imageResource = IOUtils.getResourceAsURL(column.getImage(), ReportConverter.class.getClassLoader());
                if (imageResource == null) {
                    logger.error("Image not found");
                    return;
                }
                Path path = Paths.get(imageResource.toURI());
                img = Image.getInstance(path.toAbsolutePath().toString());
            }

            // img.scalePercent(column.getScaleFactor());
            if (column.getWidth() != null) {
                img.scaleToFit(column.getWidth() * 1.0f, column.getHeight() * 1.0f);
            } else {
                img.setScaleToFitHeight(true);
            }

            PdfPCell cell1 = new PdfPCell(img);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.addCell(cell1);
        } else if (column.getChart() != null) {
            final JFreeChart chart = convertChart(column.getOldChart());

            if (chart != null) {
                int width = column.getOldChart().getWidth() == null ? 200 : column.getOldChart().getWidth();
                int height = column.getOldChart().getHeight() == null ? 200 : column.getOldChart().getHeight();
                BufferedImage ChartBufferedImage = chart.createBufferedImage(width, height);
                com.itextpdf.text.Image chartImage = Image.getInstance(writer, ChartBufferedImage, 1.0f);
                // chartImage.scalePercent(column.getScaleFactor());

                PdfPCell cell1 = new PdfPCell(chartImage);
                cell1.setBorder(Rectangle.NO_BORDER);
                cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                table.addCell(cell1);
            } else {
                PdfPCell cell1 = new PdfPCell(new Phrase("Failed to convert chart", colFont));
                cell1.setBorder(Rectangle.NO_BORDER);
                cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                table.addCell(cell1);

            }
        } else {
            PdfPCell cell = new PdfPCell(new Phrase(column.getValue(), colFont));
            cell.setBorder(colBorder);
            if (colAlignment != 0) {
                cell.setHorizontalAlignment(colAlignment);
            }
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.addCell(cell);
        }

    }

    private static int getAlignment(int defaultAlignment, java.util.List<Format> formats) {
        int alignment = getAlignment(formats);
        if (alignment != 0) {
            return alignment;
        } else {
            return defaultAlignment;
        }
    }

    private static Font getFont(Font defaultFont, java.util.List<Format> formats) {
        Font font = getFont(formats);
        if (font != null) {
            return font;
        } else {
            return defaultFont;
        }
    }

    private static int getAlignment(java.util.List<Format> formats) {
        for (Format format : formats) {
            int alignment = convertAlignment(format);
            if (alignment != 0) {
                return alignment;
            }
        }
        return 0;
    }

    private static int getBorder(int defaultBorder, java.util.List<Format> formats) {
        int border = getBorder(formats);
        if (border != 0) {
            return border;
        } else {
            return defaultBorder;
        }
    }

    private static int getBorder(java.util.List<Format> formats) {
        for (Format format : formats) {
            int border = convertBorder(format);
            if (border != 0) {
                return border;
            }
        }
        return 0;
    }

    private static int convertBorder(Format format) {
        switch (format) {
            case TOTAL_RIGHT:
            case TOTAL:
                return Rectangle.TOP;
            case UNDERLINE:
                return Rectangle.BOTTOM;
        }
        return 0;
    }

    private static int convertAlignment(Format format) {
        switch (format) {
            case NONE:
            case DEFAULT:
            case COL_HDR:
            case SUB_HEADER:
            case TEXT_LEFT:
            case TOTAL:
                return Element.ALIGN_LEFT;
            case COL_HDR_RIGHT:
            case TEXT_RIGHT:
            case BORDER_RIGHT:
            case SUB_TOTAL:
            case NUMBER:
            case TOTAL_RIGHT:
                return Element.ALIGN_RIGHT;
        }
        return 0;
    }

    private static Font convertFont(Format format) {
        switch (format) {
            case NONE:
            case DEFAULT:
            case NUMBER:
            case TEXT_RIGHT:
            case BLANK_ROW:
            case TEXT_LEFT:
            case UNDERLINE:
            case BORDER_RIGHT:
            case GRID:
            case SUB_TOTAL:
                return FontFactory.getFont(FontFactory.HELVETICA, 6, BaseColor.BLACK);
            case SMALL:
                return FontFactory.getFont(FontFactory.HELVETICA, 6, BaseColor.BLACK);
            case TITLE:
                return FontFactory.getFont(FontFactory.HELVETICA_BOLD, 22, BaseColor.BLACK);
            case BOLD:
            case COL_HDR:
            case COL_HDR_RIGHT:
                return FontFactory.getFont(FontFactory.HELVETICA_BOLD, 8, BaseColor.BLACK);
            case SUB_HEADER:
                return FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, BaseColor.BLACK);
            case SPECIAL:
                return FontFactory.getFont(FontFactory.HELVETICA_BOLD, 16, BaseColor.DARK_GRAY);
            case TOTAL:
            case TOTAL_RIGHT:
                return FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, BaseColor.BLACK);
            case LARGE:
                return FontFactory.getFont(FontFactory.HELVETICA_BOLD, 16, BaseColor.BLACK);
            default:
                return FontFactory.getFont(FontFactory.HELVETICA, 10, BaseColor.RED);
        }
    }

    private static Font getFont(java.util.List<Format> formats) {
        return getFont(formats, false);
    }

    private static Font getFont(List<Format> formats, boolean withDefault) {
        Font font;
        if (formats == null || formats.isEmpty()) {
            if (withDefault) {
                return FontFactory.getFont(FontFactory.HELVETICA_BOLD, 12, BaseColor.BLACK);
            } else {
                return null;
            }
        } else {
            // loop until we find first font
            for (Format format : formats) {
                font = convertFont(format);
                if (font != null) {
                    return font;
                }
            }
        }
        return null;
    }

    //    private Font getReportDefaultFont(ReportDO reportDo) {
//        Font defaultFont = getFont(reportDo.getDefaultFormat(),true);
//        return defaultFont;
//    }
    private static void doHeading(Document document, String title, String logo) throws BadElementException, IOException, DocumentException, URISyntaxException {

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100.0f);
        Font titleFont = convertFont(Format.TITLE);
        PdfPCell cell2 = new PdfPCell(new Phrase(title, titleFont));
        cell2.setBorder(Rectangle.NO_BORDER);
        cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(cell2);

        if (!isNull(logo)) {
            URL imageResource = IOUtils.getResourceAsURL(logo, ReportConverter.class.getClassLoader());

            if (imageResource == null) {
                logger.error("Image not found: {}", logo);
                return;
            }
            Path path = Paths.get(imageResource.toURI());

            com.itextpdf.text.Image img = Image.getInstance(path.toAbsolutePath().toString());

            img.scalePercent(20.0f);

            PdfPCell cell1 = new PdfPCell(img);
            cell1.setBorder(Rectangle.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell1);

        }

        document.add(table);

    }

    static class LinkInCell implements PdfPCellEvent {

        protected String url;

        public LinkInCell(String url) {
            this.url = url;
        }

        @Override
        public void cellLayout(PdfPCell cell, com.itextpdf.text.Rectangle position,
                               PdfContentByte[] canvases) {
            PdfWriter writer = canvases[0].getPdfWriter();
            PdfAction action = new PdfAction(url);
            PdfAnnotation link = PdfAnnotation.createLink(
                    writer, position, PdfAnnotation.HIGHLIGHT_INVERT, action);
            writer.addAnnotation(link);
        }
    }

    static class ImageBackgroundHelper extends PdfPageEventHelper {

        private com.itextpdf.text.Image backgroundImage;

        public ImageBackgroundHelper() {
        }

        public ImageBackgroundHelper(String imageName) throws URISyntaxException, BadElementException, IOException {
            URL imageResource = IOUtils.getResourceAsURL(imageName, ReportConverter.class.getClassLoader());
            // URL imageResource = ClassLoader.getSystemResource(imageName);
            if (imageResource == null) {
                logger.error("Failed to find image: {}", imageName);
                return;
            }
            Path path = Paths.get(imageResource.toURI());
            this.backgroundImage = Image.getInstance(path.toAbsolutePath().toString());

            backgroundImage.scaleAbsolute(PageSize.A4.rotate());
            backgroundImage.setAbsolutePosition(0, 0);
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            if (backgroundImage == null) {
                return;
            }
            // This scales the image to the page,
            // use the image's width & height if you don't want to scale.
            float width = document.getPageSize().getWidth();
            float height = document.getPageSize().getHeight();
            try {
                writer.getDirectContentUnder().addImage(backgroundImage, width, 0, 0, height, 0, 0);
            } catch (DocumentException ex) {
                logger.error("Faled to add image");
            }
        }
    }

}
