package gusl.report.builder;

import gusl.core.exceptions.GUSLErrorException;
import gusl.report.model.ReportDO;

import java.io.ByteArrayOutputStream;
import java.util.zip.ZipOutputStream;

public class ReportConverter {

    public static void convertToExcel(ReportDO reportDo, String filename) {
        ReportToExcelConverter.convertToExcel(reportDo, filename);
    }

    public static void convertToExcel(ReportDO reportDo, ByteArrayOutputStream baos) throws GUSLErrorException {
        ReportToExcelConverter.convertToExcel(reportDo, baos);
    }

    public static void convertToExcel(ReportDO reportDo, ZipOutputStream out) throws GUSLErrorException {
        ReportToExcelConverter.convertToExcel(reportDo, out);
    }

    public static void convertToPdf(ReportDO reportDo, String filename) {
        ReportToPdfConverter.convertToPdf(reportDo, filename);
    }

    public static void convertToCsv(ReportDO reportDo, String filename) {
        ReportToCsvConverter.convertToCsv(reportDo, filename);
    }

    public static void convertToPdf(ReportDO reportDo, ByteArrayOutputStream baos) {
        ReportToPdfConverter.convertToPdf(reportDo, baos);
    }

}
