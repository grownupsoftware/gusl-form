package gusl.report.builder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.report.model.Format;
import gusl.report.model.ReportDO;
import lombok.CustomLog;

@CustomLog
public class MyReportTest {

    private ReportDO doIt() {
        ReportDO reportDo = Builder.report()
                .title("My ReportConfig")
                .logo("images/lottomart-logo-black.png")
                .landscape()
                .defaultFormat(Format.SMALL)
                .gap(50.0f)
                .addTable(Builder.table()
                        .title("My first table")
                        .addRow(Builder.row()
                                .addCol(Builder.col().value("hello").build())
                                .addCol(Builder.col().value("there").build())
                                .build())
                        .addRow(Builder.row()
                                .addCol(Builder.col().value("good").build())
                                .addCol(Builder.col().value("bye").build())
                                .build())
                        .build())
                .build();

        return reportDo;
    }

    public static void main(String... args) throws JsonProcessingException {
        MyReportTest myReportTest = new MyReportTest();
        ReportDO reportDo = myReportTest.doIt();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        logger.info("ReportConfig: {}", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(reportDo));
    }

}
