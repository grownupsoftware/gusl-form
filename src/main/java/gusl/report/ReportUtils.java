package gusl.report;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import gusl.annotations.form.*;
import gusl.annotations.form.breadcrumb.BreadCrumbsDO;
import gusl.chart.ChartDO;
import gusl.core.json.JsonUtils;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.DateFormats;
import gusl.core.utils.IdGeneratorV1;
import gusl.core.utils.StringUtils;
import gusl.form.builder.FormBuildHelper;
import gusl.form.builder.FormBuilder;
import gusl.form.model.ActionConfigDO;
import gusl.form.model.MediaTypeFilterer;
import gusl.model.decimal.DecimalDO;
import gusl.model.decimal.TotalDO;
import gusl.model.money.MoneyDTO;
import gusl.model.odds.OddsDO;
import gusl.report.model.ReportColumnDO;
import gusl.report.model.ReportDO;
import gusl.report.model.ReportRowDO;
import gusl.report.model.ReportTableDO;

import java.lang.reflect.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class ReportUtils {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(ReportUtils.class);

    private static final DateTimeFormatter ISO_DATE = DateTimeFormatter.ofPattern(DateFormats.ISO_DATE_FORMAT);

    public static <T> Optional<ReportDO> build(T data, Class<?> doClass, String keyValue) {
        return build(data, doClass, MediaType.Desktop, keyValue);
    }

    public static <T> Optional<ReportDO> build(T data, Class<?> doClass) {
        return build(data, doClass, MediaType.Desktop, null);
    }

    public static <T> Optional<ReportDO> build(T data, Class<?> doClass, MediaType mediaType) {
        return build(data, doClass, mediaType, null);
    }

    public static <T> Optional<ReportDO> build(T data, Class<?> doClass, MediaType mediaType, String keyValue) {
        if (data instanceof List) {
            return buildReportFromList((List<T>) data, doClass, mediaType, null, null, keyValue);
        } else {
            return buildReportFromList(Collections.singletonList(data), doClass, mediaType, null, null, keyValue);
        }
    }

    public static <T> Optional<ReportDO> buildReportFromList(
            List<T> data,
            Class<?> doClass,
            MediaType mediaType,
            String blastDeltaCommand,
            String keyField,
            String keyValue
    ) {
        if (!doClass.isAnnotationPresent(UiReportTable.class)) {
            logger.warn("Class is not annotated with UiReportTable {}", doClass.getCanonicalName());
            return Optional.empty();
        }
        final UiReportTable uiReportTable = doClass.getAnnotation(UiReportTable.class);

        if (!MediaTypeFilterer.canShow(mediaType, uiReportTable.mediaType())) {
            logger.info("Not adding table - filtered by media type {} vs {}", mediaType, uiReportTable.mediaType());
            return Optional.empty();
        }
        return Optional.of(ReportDO.builder().table(buildTable(data, doClass, mediaType, blastDeltaCommand, keyField, keyValue)).build());

    }

    public static <T> Optional<ReportDO> buildReport(
            T data, Class<?> doClass) {
        return buildReport(data, doClass, MediaType.Laptop); // <-- default media type
    }

    public static <T> Optional<ReportDO> buildReport(
            T data, Class<?> doClass,
            MediaType mediaType) {
        return buildReport(data, doClass, mediaType, null, null, null);
    }

    public static <T> Optional<ReportDO> buildReport(
            T data, Class<?> doClass,
            MediaType mediaType,
            String blastDeltaCommand,
            String keyField,
            String keyValue) {
        return buildReportFromList(Collections.singletonList(data), doClass, mediaType, blastDeltaCommand, keyField, keyValue);
    }

    public static <T> ReportTableDO buildTable(
            List<T> data,
            Class<?> doClass,
            MediaType mediaType,
            String blastDeltaCommand,
            String keyField,
            String keyValue
    ) {
        final ReportTableDO.ReportTableDOBuilder tableBuilder = ReportTableDO.builder();

        boolean withHeaderRow = true;
        boolean withColumnLabels = false;

        boolean debug = false;

        if (doClass.isAnnotationPresent(UiReportTable.class)) {
            final UiReportTable uiReportTable = doClass.getAnnotation(UiReportTable.class);
            if (!MediaTypeFilterer.canShow(mediaType, uiReportTable.mediaType())) {
                return tableBuilder.build();
            }
            if (StringUtils.isNotBlank(uiReportTable.keyField())) {
                keyField = Strings.emptyToNull(uiReportTable.keyField());
            }
            if (StringUtils.isNotBlank(uiReportTable.blastDeltaCommand())) {
                blastDeltaCommand = Strings.emptyToNull(uiReportTable.blastDeltaCommand());
            }
            if ("fred".equals(uiReportTable.title())) {
                debug = true;
                logger.temp("uiReportTable css {} {} ", uiReportTable.title(), uiReportTable.css());
            }

            tableBuilder.css(Strings.emptyToNull(uiReportTable.css()))
                    .innerCss(Strings.emptyToNull(uiReportTable.valueInnerCss()))
                    .outerCss(Strings.emptyToNull(uiReportTable.valueOuterCss()))
                    .title(Strings.emptyToNull(uiReportTable.title()))
                    .width(uiReportTable.width())
                    .columnWidths(uiReportTable.columnWidths())
                    .blastDeltaCommand(blastDeltaCommand)
                    .keyField(keyField)
                    .keyValue(keyValue);

            withHeaderRow = uiReportTable.withHeaderRow();
            withColumnLabels = uiReportTable.withColumnLabels();
        }
        List<Field> fields = FormBuildHelper.getFieldsFor(doClass);

        if (withHeaderRow) {
            T firstRowData = nonNull(data) && data.size() > 0 ? data.get(0) : null;
            addHeaderRow(tableBuilder, fields, mediaType, firstRowData);
        }
        for (T entity : data) {
            addDataRow(tableBuilder, fields, mediaType, entity, doClass, withColumnLabels, blastDeltaCommand, keyField, keyValue);
        }
        final ReportTableDO table = tableBuilder.build();

        if (debug) {
            logger.temp("uiReportTable {} ", JsonUtils.prettyPrint(table));
        }

        return table;
    }

    public static Float[] getColumnWidths(float[] widths) {
        if (widths.length < 1) {
            return null;
        }
        Float[] newWidths = new Float[widths.length];
        int index = 0;
        for (float f : widths) {
            newWidths[index] = Float.valueOf(widths[index]);
        }
        return newWidths;
    }

    public static <T> void addHeaderRow(
            final ReportTableDO.ReportTableDOBuilder tableBuilder,
            List<Field> fields,
            MediaType mediaType,
            T firsRowData
    ) {
        boolean isFirstRow = true;
        boolean canAddRow = true;
        ReportRowDO.ReportRowDOBuilder rowBuilder = ReportRowDO.builder();
        for (Field field : fields) {
            if (field.isAnnotationPresent(UiReportRow.class)) {
                // only want table headers for 1st row
                continue;
//                final UiReportRow uiReportRow = field.getAnnotation(UiReportRow.class);
//                if (!MediaTypeFilterer.canShow(mediaType, uiReportRow.mediaType())) {
//                    canAddRow = false;
//                } else {
//                    canAddRow = true;
//                }
//                if (canAddRow) {
//                    if (!isFirstRow) {
//                        tableBuilder.row(rowBuilder.build());
//                        rowBuilder = ReportRowDO.builder();
//                    }
//                    rowBuilder.css(Strings.emptyToNull(uiReportRow.css()));
//                    rowBuilder.height(uiReportRow.height() > 0 ? uiReportRow.height() : null);
//                }
            }
            isFirstRow = false;
            if (canAddRow) {
                if (field.isAnnotationPresent(UiReportCol.class)) {
                    final UiReportCol uiReportCol = field.getAnnotation(UiReportCol.class);
                    if (MediaTypeFilterer.canShow(mediaType, uiReportCol.mediaType())) {
                        final ReportColumnDO.ReportColumnDOBuilder colBuilder = ReportColumnDO.builder();
                        final boolean canAdd = canAdd(firsRowData, field, uiReportCol);
                        if (canAdd) {
                            colBuilder
                                    .css(Strings.emptyToNull(uiReportCol.labelCss()))
                                    .outerCss(Strings.emptyToNull(uiReportCol.labelCss()))
                                    .innerCss(Strings.emptyToNull(uiReportCol.labelCss()))
                                    .value(uiReportCol.label());
                            // .input(uiReportCol.input() ? true : null);
                            rowBuilder.column(colBuilder.build());
                        }
                    }
                }
            }
        }
        tableBuilder.row(rowBuilder.build());
    }

    public static <T> void addDataRow(
            final ReportTableDO.ReportTableDOBuilder tableBuilder,
            List<Field> fields,
            MediaType mediaType,
            T data,
            Class<?> doClass,
            boolean withColumnLabels,
            String blastDeltaCommand,
            String keyField,
            String keyValue
    ) {

        boolean isFirstRow = true;
        boolean canAddRow = true;
        ReportRowDO.ReportRowDOBuilder rowBuilder = ReportRowDO.builder();
        for (Field field : fields) {
            if (field.isAnnotationPresent(UiReportRow.class)) {
                final UiReportRow uiReportRow = field.getAnnotation(UiReportRow.class);
                if (!MediaTypeFilterer.canShow(mediaType, uiReportRow.mediaType())) {
                    canAddRow = false;
                } else {
                    canAddRow = true;
                }
                if (canAddRow) {
                    if (!isFirstRow) {
                        tableBuilder.row(rowBuilder.build());
                        rowBuilder = ReportRowDO.builder();
                    }

                    rowBuilder.innerCss(Strings.emptyToNull(uiReportRow.valueInnerCss()))
                            .outerCss(Strings.emptyToNull(uiReportRow.valueOuterCss()));

                    rowBuilder.css(Strings.emptyToNull(uiReportRow.css()));
                    rowBuilder.height(uiReportRow.height() > 0 ? uiReportRow.height() : null);
                }
            }
            isFirstRow = false;
            if (canAddRow) {
                if (field.isAnnotationPresent(UiReportCol.class)) {
                    final UiReportCol uiReportCol = field.getAnnotation(UiReportCol.class);
                    if (MediaTypeFilterer.canShow(mediaType, uiReportCol.mediaType())) {
                        final boolean canAdd = canAdd(data, field, uiReportCol);
                        if (canAdd && withColumnLabels) {
                            // column label
                            final ReportColumnDO.ReportColumnDOBuilder colBuilder = ReportColumnDO.builder();
                            colBuilder
                                    .css(Strings.emptyToNull(uiReportCol.labelCss()))
                                    .value(uiReportCol.label())
                                    .inputIdentifier(replaceIdentifier(uiReportCol.inputIdentifier(), data))
                                    .input(uiReportCol.input() ? true : null);
                            rowBuilder.column(colBuilder.build());
                        }
                        // column field
                        if (canAdd) {
                            rowBuilder.column(buildColumn(data, doClass, field, uiReportCol, mediaType, blastDeltaCommand, keyField, keyValue));
                        }
                    }
                }
            }
        }
        tableBuilder.row(rowBuilder.build());
    }

    protected final static ObjectMapper theObjectMapper = ObjectMapperFactory.getDefaultObjectMapper();

    protected final static TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
    };

    private static <T> String replaceIdentifier(String inputIdentifier, T data) {
        String result = inputIdentifier;
        if (StringUtils.isBlank(inputIdentifier)) {
            return null;
        }
        final HashMap<String, Object> dataMap = theObjectMapper.convertValue(data, typeRef);
        final String[] splits = inputIdentifier.split("_");
        for (String split : splits) {
            if (nonNull(split) && split.startsWith("{")) {
                String fieldName = split.replace("{", "").replace("}", "");
                result = result.replace(split, "" + dataMap.get(fieldName));
            }
        }
        return result;
    }

    public static <T> ReportColumnDO buildColumn(
            T data,
            Class<?> doClass,
            Field field,
            UiReportCol uiReportCol,
            MediaType mediaType,
            String blastDeltaCommand,
            String keyField,
            String keyValue

    ) {
        final ReportColumnDO.ReportColumnDOBuilder builder = ReportColumnDO.builder();
        builder.innerCss(Strings.emptyToNull(uiReportCol.valueInnerCss()))
                .outerCss(Strings.emptyToNull(uiReportCol.valueOuterCss()))
                .name(field.getName())
                .inputIdentifier(replaceIdentifier(uiReportCol.inputIdentifier(), data))
                .input(uiReportCol.input() ? true : null)
                .extendedProperties(Strings.emptyToNull(uiReportCol.properties()));
        setValue(builder, doClass, data, field, uiReportCol, mediaType, blastDeltaCommand, keyField, keyValue);
        return builder.build();
    }

    public static <T> boolean canAdd(T data, Field field, UiReportCol uiReportCol) {
        if (!uiReportCol.hideNoValue() && StringUtils.isBlank(uiReportCol.condition())) {
            return true;
        }
        if (isNull(data)) {
            return false;
        }
        Method getterMethod = findGetterMethod(field.getName(), data.getClass());
        if (isNull(getterMethod)) {
            return false;
        }
        Object value;
        try {
            value = getterMethod.invoke(data);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn("No getter method [{}] on data class [{}]", getterMethod.getName(), data.getClass().getCanonicalName());
            return false;
        }
        if (uiReportCol.hideNoValue()) {
            if (isNull(value)) {
                return false;
            }
        }
        if (StringUtils.isNotBlank(uiReportCol.condition())) {
            logger.warn("not implemented - need an example {}", uiReportCol.condition());
        }

        return true;
    }

    public static <T> void setValue(
            final ReportColumnDO.ReportColumnDOBuilder builder,
            Class<?> doClass,
            T data,
            Field field,
            UiReportCol uiReportCol,
            MediaType mediaType,
            String blastDeltaCommand,
            String keyField,
            String keyValue
    ) {

        if (nonNull(uiReportCol.action())) {
            final ActionConfigDO action = FormBuilder.createAdminActionConfigFromAnnotation(uiReportCol.action());
            if (nonNull(action.getActionType()) && action.getActionType() != ActionType.NOT_DEFINED) {
                builder.action(action);
            }
        }

        switch (uiReportCol.type()) {
            case report:
                final Optional<ReportDO> report = buildReport(data, field.getType(), mediaType, blastDeltaCommand, keyField, keyValue);
                if (report.isPresent()) {
                    builder.report(report.get());
                }
                return;
            case blank:
                builder.value(" ");
                return;
        }

        Method getterMethod = findGetterMethod(field.getName(), data.getClass());
        if (isNull(getterMethod)) {
            return;
        }

        Object value;
        try {
            value = getterMethod.invoke(data);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn("No getter method [{}] on data class [{}]", getterMethod.getName(), data.getClass().getCanonicalName());
            return;
        }

//        if (isNull(value)) {
//            builder.value(" ");
//            return;
//        }
        switch (uiReportCol.type()) {

            case chart:
                // builder.chartJs((Map<String, Object>) value);
                builder.chart((ChartDO) value);
                break;
            case decimal:
                builder.decimal((DecimalDO) value);
                break;
            case image:
                builder.image((String) value);
                break;
            case money:
                builder.money((MoneyDTO) value);
                break;
            case position:
            case movement:
                if (isNull(value)) {
                    builder.movement(0L);
                } else {
                    if (value instanceof Integer) {
                        builder.movement(((Integer) value).longValue());
                    } else if (value instanceof Long) {
                        builder.movement((Long) value);
                    } else if (value instanceof Double) {
                        builder.movement(((Double) value).longValue());
                    }
                }
                break;
            case number:
                if (isNull(value)) {
                    builder.numberValue(0L);
                } else {
                    if (value instanceof Integer) {
                        builder.numberValue(((Integer) value).longValue());
                    } else if (value instanceof Long) {
                        builder.numberValue((Long) value);
                    } else if (value instanceof Double) {
                        builder.doubleValue((Double) value);
                    }
                }
                break;
            case text:
                if (value instanceof Enum) {
                    builder.value(((Enum) value).name());
                } else {
                    builder.value((String) value);
                }
                break;
            case circle:
                if (isNull(value)) {
                    builder.circle(false);
                } else {
                    builder.circle((boolean) value);
                }
                break;
            case breadcrumb:
                if (isNull(value)) {
                    builder.breadCrumbs(null);
                } else {
                    builder.breadCrumbs((BreadCrumbsDO) value);
                }
                break;
            case iframe:
                if (isNull(value)) {
                    builder.iframe(null);
                } else {
                    builder.iframe((String) value);
                }
                break;
            case external_link:
                if (isNull(value)) {
                    builder.externalLink(null);
                } else {
                    builder.externalLink((String) value);
                }
                break;
            case total:
                if (isNull(value)) {
                    builder.total(null);
                } else {
                    builder.total((TotalDO) value);
                }
                break;
            case percentage:
                if (isNull(value)) {
                    builder.percentage(null);
                } else {
                    if (value instanceof Integer) {
                        builder.percentage(((Integer) value).doubleValue());
                    } else if (value instanceof Long) {
                        builder.percentage(((Long) value).doubleValue());
                    } else if (value instanceof Double) {
                        builder.percentage((Double) value);
                    }
                }
            case date_time:
                if (isNull(value)) {
                    builder.date(" ");
                } else {
                    if (value instanceof Date) {
                        builder.date(DateFormats.formatDate(DateFormats.ISO_DATE_FORMAT, (Date) value));
                    } else if (value instanceof LocalDate) {
                        builder.date(((LocalDate) value).format(ISO_DATE));
                    } else if (value instanceof LocalDateTime) {
                        builder.date(((LocalDateTime) value).format(ISO_DATE));
                    } else if (value instanceof ZonedDateTime) {
                        builder.date(((ZonedDateTime) value).format(ISO_DATE));
                    }
                }
                if (nonNull(uiReportCol.dateFormat())) {
                    builder.dateFormat(uiReportCol.dateFormat().getFormat());
                }
                break;
            case nested_table:
                buildNestedTable(builder, value, field, mediaType, blastDeltaCommand, keyField, keyValue);
                break;
            case odds:
                if (isNull(value)) {
                    builder.odds(null);
                } else {
                    builder.odds((OddsDO) value);
                }
                break;

        }

    }

    private static <T> void buildNestedTable(
            ReportColumnDO.ReportColumnDOBuilder builder,
            T data,
            Field field,
            MediaType mediaType,
            String blastDeltaCommand,
            String keyField,
            String keyValue
    ) {
        ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
        Type actualTypeArgument = stringListType.getActualTypeArguments()[0];

        Class doClass = null;
        if (actualTypeArgument.getClass().equals(Class.class)) {
            doClass = (Class) actualTypeArgument;
        }
        if (isNull(doClass)) {
            logger.warn("Class unlikely to be a list {}", field.getName());
            return;
        }

        Optional<ReportDO> report = Optional.empty();
        if (data instanceof List) {
            report = buildReportFromList((List<T>) data, doClass, mediaType, null, null, keyValue);
        } else {
            report = buildReportFromList(Collections.singletonList(data), doClass, mediaType, null, null, keyValue);
        }
        if (report.isPresent()) {
            builder.report(report.get());
        }
    }

    public static String getMethodName(String prefix, String name) {
        return prefix + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    public static Method findGetterMethod(String name, Class<?> theDataClass) {

        Method getterMethod;
        try {
            getterMethod = theDataClass.getMethod(getMethodName("get", name));
        } catch (NoSuchMethodException ex) {
            try {
                getterMethod = theDataClass.getMethod(getMethodName("is", name));
            } catch (NoSuchMethodException e) {
                logger.warn("Failed to find getter " + getMethodName("get or is ", name) + " in Class: " + theDataClass.getCanonicalName()
                        + " ignoring and carrying on");
                return null;
            }
        }
        if (getterMethod != null) {
            return getterMethod;
        }
        logger.warn("Failed to find getter " + getMethodName("get or is", name) + " in Class: " + theDataClass.getCanonicalName()
                + " ignoring and carrying on");

        return null;
    }

//    public static <T> ReportTableDO buildTable_old(T data, Class<?> doClass) {
//        final ReportTableConfigDO.ReportTableConfigDOBuilder tableBuilder = ReportTableConfigDO.builder();
//
//        if (doClass.isAnnotationPresent(UiReportTable.class)) {
//            final UiReportTable uiReportTable = doClass.getAnnotation(UiReportTable.class);
//            tableBuilder.css(uiReportTable.css()).title(uiReportTable.title());
//        }
//        List<Field> fields = FormBuildHelper.getFieldsFor(doClass);
//
//        boolean isFirstField = true;
//        int rowOrder = 1;
//        int colOrder = 1;
//        ReportRowConfigDO.ReportRowConfigDOBuilder rowBuilder = ReportRowConfigDO.builder();
//        rowBuilder.displayOrder(rowOrder++);
//
//        for (Field field : fields) {
//            if (field.isAnnotationPresent(UiReportRow.class)) {
//                final UiReportRow uiReportRow = field.getAnnotation(UiReportRow.class);
//                if (!isFirstField) {
//                    tableBuilder.row(rowBuilder.build());
//                    rowBuilder = ReportRowConfigDO.builder();
//                    rowBuilder.displayOrder(rowOrder++);
//                    colOrder = 1;
//                }
//                rowBuilder.css(uiReportRow.css());
//            }
//            if (field.isAnnotationPresent(UiReportCol.class)) {
//                final UiReportCol uiReportCol = field.getAnnotation(UiReportCol.class);
//                rowBuilder.column(getColumnConfig(field, uiReportCol, colOrder++));
//            }
//
//            isFirstField = false;
//        }
//        tableBuilder.row(rowBuilder.build());
//
//        final ReportTableConfigDO reportTableConfigDO = tableBuilder.build();
//        logger.info("{}", JsonUtils.prettyPrint(reportTableConfigDO));
//
//        return ReportTableDO.builder().build();
//    }
//
//    public static ReportColumnConfigDO getColumnConfig(Field field, UiReportCol uiReportCol, int displayOrder) {
//        String label = null;
//        if (!uiReportCol.noLabel()) {
//            label = StringUtils.isNotBlank(uiReportCol.label()) ? uiReportCol.label() : parseFieldToLabel(field);
//        }
//        return ReportColumnConfigDO.builder()
//                .displayOrder(displayOrder)
//                .type(uiReportCol.type())
//                .label(label)
//                .labelCss(uiReportCol.labelCss())
//                .valueCss(uiReportCol.valueCss())
//                .dateFormat(uiReportCol.dateFormat())
//                .mediaType(uiReportCol.mediaType())
//                .build();
//    }

    public static String parseFieldToLabel(Field field) {
        String name = field.getName();
        if (name.length() <= 1) {
            return name;
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name.charAt(0));
        for (int i = 0; i < name.length() - 1; i++) {
            char c1 = name.charAt(i);
            char c2 = name.charAt(i + 1);

            if (Character.isLowerCase(c1) && Character.isUpperCase(c2)) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(c2);
        }

        String s = stringBuilder.toString();
        return Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }

    public static ZTest generate() {
        return ZTest.builder()
                .field1(IdGeneratorV1.getUUID())
                .field2(IdGeneratorV1.getUUID())
                .field3(IdGeneratorV1.getUUID())
                .field4(IdGeneratorV1.getUUID())
                .field5(IdGeneratorV1.getUUID())
                .field6(IdGeneratorV1.getUUID())
                .fieldSeven(IdGeneratorV1.getUUID())
                .counter(Long.valueOf(IdGeneratorV1.generateCorrelationId()).intValue())
                .build();
    }

    public static List<ZTest> generate(int number) {
        List<ZTest> list = new ArrayList<>(number);
        for (int x = 0; x < number; x++) {
            list.add(generate());
        }
        return list;
    }

    public static XTest generateX() {
        return XTest.builder()
                .fieldX1(IdGeneratorV1.getUUID())
                .fieldX2(IdGeneratorV1.getUUID())
                // .field3(IdGeneratorV1.getUUID())
                .build();
    }

    public static List<XTest> generateXItems(int number) {
        List<XTest> list = new ArrayList<>(number);
        for (int x = 0; x < number; x++) {
            list.add(generateX());
        }
        return list;
    }

    public static YTest generateTestItems() {
        return YTest.builder()
                .field1(IdGeneratorV1.getUUID())
                .items(generateXItems(3))
                .build();
    }

    public static void main(String[] args) {
        // final Optional<ReportDO> report = build(generate(), YTest.class, MediaType.Desktop);
        final Optional<ReportDO> report = build(generateTestItems(), YTest.class, MediaType.Desktop);
        // final Optional<ReportDO> report = build(generate(5), YTest.class, MediaType.Tablet);
        if (report.isPresent()) {
            logger.info("Report: {}", JsonUtils.prettyPrint(report));
        } else {
            logger.warn("no report");
        }
    }

}
