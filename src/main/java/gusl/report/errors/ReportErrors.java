package gusl.report.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

public enum ReportErrors {

    ENCODING_ERROR("REPE001 Request must be un encoded or use gzip", "reports.error.request.content_encoding"),
    GENERAL_IO_ERROR("REPE002 General IO error", "reports.error.general.io");

    private String field;
    private final String message;
    private final String messageKey;

    ReportErrors(String field, String message, String messageKey) {
        this.field = field;
        this.message = message;
        this.messageKey = messageKey;
    }

    ReportErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        if (field != null) {
            return new ErrorDO(field, message, messageKey);
        } else {
            return new ErrorDO(message, messageKey);
        }

    }

    public ErrorDO getError(Long id) {
        if (field != null) {
            if (id != null) {
                return new ErrorDO(field, message, messageKey, String.valueOf(id));
            } else {
                return new ErrorDO(field, message, messageKey);
            }
        } else {
            if (id != null) {
                return new ErrorDO(null, message, messageKey, String.valueOf(id));
            } else {
                return new ErrorDO(message, messageKey);
            }
        }
    }

    public ErrorDO getError(String... params) {
        if (field != null) {
            if (params != null) {
                return new ErrorDO(field, message, messageKey, params);
            } else {
                return new ErrorDO(field, message, messageKey);
            }
        } else {
            if (params != null) {
                return new ErrorDO(null, message, messageKey, params);
            } else {
                return new ErrorDO(message, messageKey);
            }
        }
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }
}
