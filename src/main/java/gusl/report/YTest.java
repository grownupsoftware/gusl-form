package gusl.report;

import gusl.annotations.form.*;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@UiReportTable(title = "hello world", css = "backgroundColor:red", mediaType = MediaType.Tablet, withHeaderRow = true,
        blastDeltaCommand = "positions", keyField = "id")
public class YTest {

    @UiReportRow(css = "border: 1px solid blue")
    @UiReportCol(type = ReportColumnType.text)
    private String field1;

    @UiReportRow(css = "border: 1px solid blue")
    @UiReportCol(type = ReportColumnType.nested_table)
    private List<XTest> items;

//    @UiReportCol(type = ReportColumnType.text)
//    private String field2;
//
//    @UiReportRow(css = "border: 1px solid orange", mediaType = MediaType.Tablet)
//    @UiReportCol(type = ReportColumnType.text)
//    private String field3;
//
//    @UiReportCol(type = ReportColumnType.text)
//    private String field4;
//
//    @UiReportCol(type = ReportColumnType.text)
//    private String field5;
//
//    @UiReportCol(type = ReportColumnType.text)
//    private String field6;
//
//    @UiReportCol(type = ReportColumnType.text, mediaType = MediaType.Tablet)
//    private String fieldSeven;
//
//    @UiReportCol(type = ReportColumnType.number)
//    private Integer counter;

}
