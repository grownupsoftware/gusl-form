package gusl.report;

import gusl.annotations.form.*;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@UiReportTable(title = "hello world", css = "backgroundColor:red")
public class ZTest {

    @UiReportRow(css = "border: 1px solid blue")
    @UiReportCol(type = ReportColumnType.text)
    private String field1;

    @UiReportCol(type = ReportColumnType.text)
    private String field2;

    @UiReportRow(css = "border: 1px solid orange")
    @UiReportCol(type = ReportColumnType.text)
    private String field3;

    @UiReportCol(type = ReportColumnType.text)
    private String field4;

    @UiReportCol(type = ReportColumnType.text)
    private String field5;

    @UiReportCol(type = ReportColumnType.text)
    private String field6;

    @UiReportCol(type = ReportColumnType.text, mediaType = MediaType.Laptop)
    private String fieldSeven;

    @UiReportCol(type = ReportColumnType.number)
    private Integer counter;

}
