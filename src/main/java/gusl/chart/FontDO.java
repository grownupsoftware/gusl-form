package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FontDO {

    private Integer size;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
