package gusl.chart;

public enum TimeUnitType {
    year,
    month,
    week,
    days,
    datetime_local
}
