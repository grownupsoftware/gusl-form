package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ScaleDO {
    private boolean display;
    private ScaleGridDO grid;
    private TicksDO ticks;
    private TitleDO title;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
