package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class LegendDO {

    private boolean display;
    private LabelsDO labels;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
