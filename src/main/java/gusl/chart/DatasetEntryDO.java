package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DatasetEntryDO {

    private String name;
    private Number value;

    public static DatasetEntryDO of(String name, Number value) {
        return DatasetEntryDO.builder().name(name).value(value).build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
