package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LabelsDO {
    private Double boxHeight;
    private Double boxWidth;
    private FontDO font;
    private PointStyleType pointStyle;
    private boolean usePointStyle;
    private String color;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
