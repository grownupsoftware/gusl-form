package gusl.chart;

public enum TitleAlign {
    start,
    center,
    end
}
