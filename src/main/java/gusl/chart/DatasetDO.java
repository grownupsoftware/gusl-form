package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DatasetDO {

    private String label;

    @Singular
    private List<AccumulativeSet> datasets;

    private boolean fill;
    private String pointStyle;
    private Integer pointRadius;
    private Integer pointHoverRadius;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
