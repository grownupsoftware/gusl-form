package gusl.chart;

public enum ChartType {
    line,
    bar,
    radar,
    bubble,
    polarArea,
    doughnut,
    pie
}
