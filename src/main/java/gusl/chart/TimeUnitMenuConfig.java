package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TimeUnitMenuConfig {

    private boolean year;
    private boolean month;
    private boolean week;
    private boolean days;
    private boolean localHours;
    private boolean localMinutes;
    private boolean localSeconds;
    private boolean localMilliSeconds;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
