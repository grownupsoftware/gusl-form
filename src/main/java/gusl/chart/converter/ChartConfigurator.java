package gusl.chart.converter;

import gusl.node.converter.ModelConverterConfigurator;
import lombok.CustomLog;
import ma.glasnost.orika.MapperFactory;
import org.jvnet.hk2.annotations.Service;

@Service
@CustomLog
public class ChartConfigurator implements ModelConverterConfigurator {

    @Override
    public void configureMapperFactory(MapperFactory mapperFactory) {
//        mapperFactory.classMap(AccumulativeSet.class, AccumulativeSet.class)
//                .customize(new CustomMapper<AccumulativeSet, AccumulativeSet>() {
//                    @Override
//                    public void mapAtoB(AccumulativeSet from, AccumulativeSet to, MappingContext context) {
//                        try {
//                            logger.info("Im here {}", from.getTheMap().entrySet());
//                            to.setName(from.getName());
//                            safeStream(from.getTheMap().entrySet())
//                                    .forEach(entry -> {
//                                        to.accumulate(entry.getKey(), entry.getValue());
//                                    });
//                        } catch (Throwable t) {
//                            logger.error("Error performing mapping a=>B", t);
//                        }
//                    }
//                })
//                .register();

//        mapperFactory.classMap(DatasetDO.class, DatasetDTO.class)
//                .customize(new CustomMapper<DatasetDO, DatasetDTO>() {
//                    @Override
//                    public void mapAtoB(DatasetDO from, DatasetDTO to, MappingContext context) {
//                        try {
//                            to.setDatasets(safeStream(from.getDatasets())
//                                    .map(accumalativeSet -> accumalativeSet.getDataEntries())
//                                    .flatMap(Collection::stream)
//                                    .collect(Collectors.toList())
//                            );
//                        } catch (Throwable t) {
//                            logger.error("Error performing mapping a=>B", t);
//                        }
//                    }
//                })
//                .register();

    }
}
