package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DatasetEntriesDTO {

    private Map<String, Double> data;
    private String name;

    public DatasetEntriesDTO(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
