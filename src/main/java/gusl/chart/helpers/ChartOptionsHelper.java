package gusl.chart.helpers;

import gusl.chart.*;

public class ChartOptionsHelper {

    public static ChartOptionsDO defaultChartTypeMenu() {
        return build(ChartOptionsConfigDO.builder()
                .line(true)
                .bar(true)
                .bubble(false)
                .doughnut(false)
                .pie(false)
                .polarArea(false)
                .radar(false)
                .build());
    }

    public static ChartOptionsDO build(ChartOptionsConfigDO config) {
        return ChartOptionsDO.builder()
                .bar(config.isBar() ? barChartOptions() : null)
                .line(config.isLine() ? lineChartOptions() : null)
                .radar(config.isRadar() ? radarChartOptions() : null)
                .bubble(config.isBubble() ? bubbleChartOptions() : null)
                .polarArea(config.isPolarArea() ? polarAreaChartOptions() : null)
                .doughnut(config.isDoughnut() ? doughnutChartOptions() : null)
                .pie(config.isPie() ? pieChartOptions() : null)

                .build();
    }

    private static ChartOptionDO barChartOptions() {
        return ChartOptionDO.builder()
                .barPercentage(0.5d)
                .barThickness(5.0d)
                .chart(ChartCanvasDO.builder()
                        .canvas(CanvasDO.builder()
                                .height(150d)
                                .build())
                        .build())
                .maintainAspectRatio(false)
                .plugins(defaultPlugins())
                .responsive(true)
                .scales(ScalesDO.builder()
                        .x(ScaleDO.builder()
                                .display(true)
                                .grid(ScaleGridDO.builder()
                                        .display(true)
                                        .build())
                                .ticks(TicksDO.builder().color("#fff").font(FontDO.builder().size(10).build()).build())
                                .build())
                        .y(ScaleDO.builder()
                                .display(true)
                                .grid(ScaleGridDO.builder()
                                        .display(true)
                                        .build())
                                .ticks(TicksDO.builder().color("#fff").font(FontDO.builder().size(10).build()).build())
                                .build())
                        .build())
                .build();
    }

    private static PluginsDO defaultPlugins() {
        return PluginsDO.builder()
                .legend(LegendDO.builder()
                        .display(true)
                        .labels(LabelsDO.builder()
                                .boxHeight(8d)
                                .boxWidth(8d)
                                .color("#fff")
                                .font(FontDO.builder().size(10).build())
                                .pointStyle(PointStyleType.triangle)
                                .usePointStyle(true)
                                .build())
                        .build())
                .build();
    }

    private static ChartOptionDO lineChartOptions() {
        return ChartOptionDO.builder()
                .borderWidth(1.0d)
                .chart(ChartCanvasDO.builder()
                        .canvas(CanvasDO.builder().height(100.0d).build())
                        .build())
                .fill(true)
                .maintainAspectRatio(false)
                .plugins(defaultPlugins())
                .pointRadius(2.0d)
                .responsive(true)
                .scales(ScalesDO.builder()
                        .x(ScaleDO.builder()
                                .display(true)
                                .grid(ScaleGridDO.builder().display(true).build())
                                .ticks(TicksDO.builder()
                                        .color("#11ffff")
                                        .font(FontDO.builder().size(8).build())
                                        .build())
                                .build())
                        .y(ScaleDO.builder()
                                .display(true)
                                .grid(ScaleGridDO.builder().display(true).build())
                                .build())
                        .build())
                .tension(0.2d)
                .build();
    }

    private static ChartOptionDO radarChartOptions() {
        return ChartOptionDO.builder()
                .build();
    }

    private static ChartOptionDO bubbleChartOptions() {
        return ChartOptionDO.builder()
                .build();
    }

    private static ChartOptionDO polarAreaChartOptions() {
        return ChartOptionDO.builder()
                .build();
    }

    private static ChartOptionDO doughnutChartOptions() {
        return ChartOptionDO.builder()
                .build();
    }

    private static ChartOptionDO pieChartOptions() {
        return ChartOptionDO.builder()
                .build();
    }

}
