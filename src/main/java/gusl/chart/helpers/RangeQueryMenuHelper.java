package gusl.chart.helpers;

import gusl.chart.RangeQueryMenuConfigDO;
import gusl.chart.RangeQueryMenuDO;
import gusl.query.DateRangeQuery;
import lombok.SneakyThrows;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class RangeQueryMenuHelper {

    public static List<RangeQueryMenuDO> defaultRangeQueryMenu() {
        return build(RangeQueryMenuConfigDO.builder()
                .byDay(true)
                .byWeek(true)
                .byMonth(true)
                .build());
    }

    public static List<RangeQueryMenuDO> build(final RangeQueryMenuConfigDO config) {
        List<RangeQueryMenuDO> list = new ArrayList<>();
        if (config.isByDay()) {
            list.add(byDayMenu(config.getFieldName()));
        }
        if (config.isByWeek()) {
            list.add(byWeekMenu(config.getFieldName()));
        }
        if (config.isByMonth()) {
            list.add(byMonthMenu(config.getFieldName()));
        }
        return list;
    }

    @SneakyThrows
    private static RangeQueryMenuDO byDayMenu(final String fieldName) {
        return RangeQueryMenuDO.builder()
                .label("1 day")
                .rangeQuery(DateRangeQuery.of(fieldName, convert("now-1d/d"), convert("now/d")))
                .build();
    }

    @SneakyThrows
    private static RangeQueryMenuDO byWeekMenu(final String fieldName) {
        return RangeQueryMenuDO.builder()
                .label("7 days")
                .rangeQuery(DateRangeQuery.of(fieldName, convert("now-7d/d"), convert("now/d")))
                .build();
    }

    @SneakyThrows
    private static RangeQueryMenuDO byMonthMenu(final String fieldName) {
        return RangeQueryMenuDO.builder()
                .label("30 days")
                .rangeQuery(DateRangeQuery.of(fieldName, convert("now-30d/d"), convert("now/d")))
                .build();
    }

    @SneakyThrows
    private static LocalDateTime convert(String value) {
        return LocalDateTime.now(ZoneId.of("UTC"));
        // return LocalDateTime.from(DateMathParser.parse(value));
    }
}
