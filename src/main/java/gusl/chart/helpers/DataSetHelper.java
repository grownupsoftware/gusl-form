package gusl.chart.helpers;

import gusl.chart.AccumulativeSet;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

import static gusl.core.utils.Utils.safeStream;

public class DataSetHelper {

    public static AccumulativeSet accumulativeSet(String label, List<Pair<String, Number>> data) {
        AccumulativeSet accumulativeSet = new AccumulativeSet();
        accumulativeSet.setName(label);
        safeStream(data).forEach(pair -> accumulativeSet.accumulate(pair.getLeft(), pair.getRight()));
        return accumulativeSet;
    }

    public static AccumulativeSet accumulativeSet(String label, Pair<String, Number>[] data) {
        AccumulativeSet accumulativeSet = new AccumulativeSet();
        accumulativeSet.setName(label);
        safeStream(data).forEach(pair -> accumulativeSet.accumulate(pair.getLeft(), pair.getRight()));
        return accumulativeSet;
    }

}
