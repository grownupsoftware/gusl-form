package gusl.chart.helpers;

import gusl.chart.TimeUnitMenuConfig;
import gusl.chart.TimeUnitType;
import gusl.chart.TimeUnitsMenuDO;

import java.util.ArrayList;
import java.util.List;

public class TimeUnitHelper {

    public static List<TimeUnitsMenuDO> defaultTimeUnitsMenu() {
        return build(TimeUnitMenuConfig.builder()
                .year(true)
                .year(true)
                .month(true)
                .week(true)
                .days(true)
                .localHours(true)
                .localMinutes(true)
                .localSeconds(true)
                .localMilliSeconds(true)
                .build());
    }

    public static List<TimeUnitsMenuDO> build(final TimeUnitMenuConfig config) {
        List<TimeUnitsMenuDO> list = new ArrayList<>();
        if (config.isYear()) {
            list.add(yearUnit());
        }
        if (config.isMonth()) {
            list.add(monthUnit());
        }
        if (config.isWeek()) {
            list.add(weekUnit());
        }
        if (config.isDays()) {
            list.add(dayUnit());
        }
        if (config.isLocalHours()) {
            list.add(localHoursUnit());
        }
        if (config.isLocalMinutes()) {
            list.add(localMinutesUnit());
        }
        if (config.isLocalSeconds()) {
            list.add(localSecondsUnit());
        }
        if (config.isLocalMilliSeconds()) {
            list.add(localMilliSecondsUnit());
        }
        return list;

    }

    public static TimeUnitsMenuDO yearUnit() {
        return TimeUnitsMenuDO.builder()
                .inputType(TimeUnitType.year)
                .label("Years")
                .rangeQuery("y")
                .build();
    }

    public static TimeUnitsMenuDO monthUnit() {
        return TimeUnitsMenuDO.builder()
                .inputType(TimeUnitType.month)
                .label("Months")
                .rangeQuery("M")
                .build();

    }

    public static TimeUnitsMenuDO weekUnit() {
        return TimeUnitsMenuDO.builder()
                .inputType(TimeUnitType.week)
                .label("Weeks")
                .rangeQuery("w")
                .build();

    }

    public static TimeUnitsMenuDO dayUnit() {
        return TimeUnitsMenuDO.builder()
                .inputType(TimeUnitType.days)
                .label("Days")
                .rangeQuery("d")
                .build();

    }

    public static TimeUnitsMenuDO localHoursUnit() {
        return TimeUnitsMenuDO.builder()
                .inputType(TimeUnitType.datetime_local)
                .label("Hours")
                .rangeQuery("h")
                .build();
    }

    public static TimeUnitsMenuDO localMinutesUnit() {
        return TimeUnitsMenuDO.builder()
                .inputType(TimeUnitType.datetime_local)
                .label("Minutes")
                .rangeQuery("m")
                .build();
    }

    public static TimeUnitsMenuDO localSecondsUnit() {
        return TimeUnitsMenuDO.builder()
                .inputType(TimeUnitType.datetime_local)
                .label("Seconds")
                .rangeQuery("s")
                .build();

    }

    public static TimeUnitsMenuDO localMilliSecondsUnit() {
        return TimeUnitsMenuDO.builder()
                .inputType(TimeUnitType.datetime_local)
                .label("Milliseconds")
                .rangeQuery("ms")
                .build();

    }

}
