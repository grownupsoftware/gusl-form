package gusl.chart.helpers;

import gusl.chart.ChartType;
import gusl.chart.ChartTypeMenuConfigDO;
import gusl.chart.ChartTypeMenuDO;

import java.util.ArrayList;
import java.util.List;

public class ChartTypeMenuHelper {

    public static List<ChartTypeMenuDO> defaultChartTypeMenu() {
        return build(ChartTypeMenuConfigDO.builder()
                .line(true)
                .bar(true)
                .build());
    }

    public static List<ChartTypeMenuDO> build(ChartTypeMenuConfigDO config) {
        List<ChartTypeMenuDO> list = new ArrayList<>();

        if (config.isBar()) {
            list.add(barMenu());
        }

        if (config.isLine()) {
            list.add(lineMenu());
        }

        if (config.isRadar()) {
            list.add(radarMenu());
        }
        if (config.isBubble()) {
            list.add(bubbleMenu());
        }
        if (config.isPolarArea()) {
            list.add(polarAreaMenu());
        }
        if (config.isDoughnut()) {
            list.add(doughnutMenu());
        }
        if (config.isPie()) {
            list.add(pieMenu());
        }

        return list;
    }

    public static ChartTypeMenuDO barMenu() {
        return ChartTypeMenuDO.builder()
                .type(ChartType.bar)
                .icon("fa-solid fa-chart-simple")
                .label("Bar")
                .build();
    }

    public static ChartTypeMenuDO lineMenu() {
        return ChartTypeMenuDO.builder()
                .type(ChartType.line)
                .icon("fa-solid fa-chart-line")
                .label("Line")
                .build();
    }

    public static ChartTypeMenuDO polarAreaMenu() {
        return ChartTypeMenuDO.builder()
                .type(ChartType.polarArea)
                .icon("fa-solid fa-chart-pie fa-rotate-90")
                .label("Polar area")
                .build();
    }

    public static ChartTypeMenuDO radarMenu() {
        return ChartTypeMenuDO.builder()
                .type(ChartType.radar)
                .icon("fa-solid fa-burst")
                .label("Radar")
                .build();
    }

    public static ChartTypeMenuDO bubbleMenu() {
        return ChartTypeMenuDO.builder()
                .type(ChartType.bubble)
                .icon("fa-solid fa-circle")
                .label("Bubble")
                .build();
    }

    public static ChartTypeMenuDO doughnutMenu() {
        return ChartTypeMenuDO.builder()
                .type(ChartType.doughnut)
                .icon("fa-solid fa-circle-dot")
                .label("Doughnut")
                .build();
    }

    public static ChartTypeMenuDO pieMenu() {
        return ChartTypeMenuDO.builder()
                .type(ChartType.pie)
                .icon("fa-solid fa-chart-pie")
                .label("Pie")
                .build();
    }
}
