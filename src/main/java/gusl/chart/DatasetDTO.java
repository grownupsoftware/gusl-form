package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;
import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DatasetDTO {

    private String label;

//    @Singular
//    private List<AccumulativeSet> datasets;

    private List<List<Map<String, Number>>> datasets;

    private boolean fill;
    private String pointStyle;
    private Integer pointRadius;
    private Integer pointHoverRadius;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
