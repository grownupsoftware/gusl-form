package gusl.chart;

import gusl.core.tostring.ToString;
import gusl.query.RangeQuery;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RangeQueryMenuDO {

    private String label;
    private RangeQuery rangeQuery;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
