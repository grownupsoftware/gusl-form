package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TitleDO {
    private boolean display;
    private String text;
    private TitleAlign align;
    private FontDO font;
    private String color;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
