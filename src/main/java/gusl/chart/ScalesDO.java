package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ScalesDO {
    private ScaleDO x;
    private ScaleDO y;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
