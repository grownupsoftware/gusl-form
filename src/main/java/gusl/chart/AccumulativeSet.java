package gusl.chart;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.core.lambda.MutableObject;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.function.BinaryOperator;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;

@Getter
@Setter
public class AccumulativeSet {

    @JsonIgnore
    private Map<String, Number> theData; //  = new LinkedHashMap<>();
    private String name;

    private boolean fill;

    private String pointStyle;
    private Integer pointRadius;
    private Integer pointHoverRadius;

    @ToStringIgnore
    @JsonIgnore
    private BinaryOperator<Number> adder;

    public AccumulativeSet() {
        theData = new LinkedHashMap<>();
    }

    public AccumulativeSet(String name) {
        this.name = name;
        theData = new LinkedHashMap<>();
    }

    public AccumulativeSet(String... values) {
        for (String value : values) {
            theData.put(value, 0);
        }
    }

    public void accumulate(String key) {
        if (key != null) {
            Number value = theData.get(key);
            if (value == null) {
                theData.put(key, 1);
            } else {
                theData.put(key, value.intValue() + 1);
            }
        }
    }

    public void accumulate(String key, String nullKey) {
        if (key != null) {
            accumulate(key);
        } else {
            accumulate(nullKey);
        }
    }

    public void accumulate(String key, Number value) {
        if (key != null) {
            Number current = theData.get(key);
            if (current == null) {
                theData.put(key, value);
                createAddr(value);
            } else {
                theData.put(key, adder.apply(current, value));
            }
        }
    }

    // Used to pass data to UI
    public List<Map<String, Number>> getData() {
        final List<Map<String, Number>> list = new ArrayList<>();
        list.add(theData);
        return list;
    }

    @JsonIgnore
    public List<String> getLabels() {
        return new ArrayList<>(theData.keySet());
    }

    @JsonIgnore
    public List<Number> getValues() {
        return new ArrayList<>(theData.values());
    }

    @JsonIgnore
    public int[] getValueIntArray() {
        final int[] values = new int[theData.size()];
        int i = 0;
        for (Number value : getValues()) {
            values[i++] = value.intValue();
        }
        return values;
    }

    @JsonIgnore
    public double[] getValueDoubleArray() {
        final double[] values = new double[theData.size()];
        int i = 0;
        for (Number value : getValues()) {
            values[i++] = value.doubleValue();
        }
        return values;
    }

    @JsonIgnore
    public Map<Object, Number> getObjectMap() {
        HashMap<Object, Number> objectMap = new HashMap<>(theData.size());
        for (Map.Entry<String, Number> entry : theData.entrySet()) {
            objectMap.put(entry.getKey(), entry.getValue());
        }
        return objectMap;
    }

    @JsonIgnore
    public int size() {
        return theData.size();
    }

    @JsonIgnore
    public boolean isEmpty() {
        return theData.isEmpty();
    }

    private void createAddr(Number number) {
        Class<? extends Number> type = number.getClass();

        if (type == Integer.class || type == int.class) {
            adder = (a, b) -> a.intValue() + b.intValue();
            return;
        }

        if (type == Double.class || type == double.class) {
            adder = (a, b) -> a.doubleValue() + b.doubleValue();
            return;
        }

        if (type == Long.class || type == long.class) {
            adder = (a, b) -> a.longValue() + b.longValue();
            return;
        }

        if (type == Float.class || type == float.class) {
            adder = (a, b) -> a.floatValue() + b.floatValue();
            return;
        }
    }

    @JsonIgnore
    public Number getTotal() {
        if (isNull(adder)) {
            return 0;
        }

        MutableObject<Number> total = new MutableObject<>(0);
        safeStream(theData.values()).forEach(val -> total.set(adder.apply(total.get(), val)));
        return total.get();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @JsonIgnore
    public boolean totalNotZero() {
        try {
            return getTotal().doubleValue() != 0.0d;
        } catch (Throwable t) {
            return false;
        }
    }
}
