package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ScaleGridDO {
    private boolean display;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
