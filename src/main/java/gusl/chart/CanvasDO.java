package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class CanvasDO {
    private Double height;
    private Double width;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
