package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DatasetEntriesDO {

    private List<DatasetEntryDO> data;
    private String name;

    public DatasetEntriesDO(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
