package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class ChartOptionsDO {
    private ChartOptionDO line;
    private ChartOptionDO bar;
    private ChartOptionDO radar;
    private ChartOptionDO bubble;
    private ChartOptionDO polarArea;
    private ChartOptionDO doughnut;
    private ChartOptionDO pie;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
