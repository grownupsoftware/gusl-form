package gusl.chart;

import gusl.core.tostring.ToString;
import gusl.query.QueryParams;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChartDO {

    private String chartTitle;
    private QueryParams query;
    private ChartType initialChartType;
    private String initialDataset;
    private TimeUnitsMenuDO initialTimeUnit;
    private boolean customSelectRange;
    private List<ChartTypeMenuDO> chartTypeMenu;
    private List<RangeQueryMenuDO> rangeQueryMenu;
    private List<TimeUnitsMenuDO> timeUnitsMenu;
    private ChartOptionsDO chartOptions;
    private boolean hidePanel;

    @Singular
    private List<DatasetDO> newDatasets;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
