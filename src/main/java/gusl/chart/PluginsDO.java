package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PluginsDO {

    private LegendDO legend;

    private TitleDO title;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
