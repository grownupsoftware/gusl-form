package gusl.chart.old.builders;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.IdGenerator;
import gusl.core.utils.Utils;
import gusl.model.chart.ChartDO;
import gusl.model.chart.ChartDatasetDO;
import gusl.model.chart.DataPointDO;
import gusl.model.chart.PieDataDO;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PieChartBuilder extends AbstractChartBuilder {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(PieChartBuilder.class);

    public static class Builder {

        private Integer height = 300;
        private Integer width = 300;
        private List<DataPointDO<String, Long>> dataPoints;
        private String title;
        private boolean doughnut = false;
        private boolean showLegend = true;
        public String legendColor = "rgb(98, 93, 93)";
        public int legendFontSize = 10;
        public String legendPosition = "bottom";

        public Builder legendPosition(String legendPosition) {
            this.legendPosition = legendPosition;
            return this;
        }

        public Builder legendColor(String legendColor) {
            this.legendColor = legendColor;
            return this;
        }

        public Builder legendFontSize(int legendFontSize) {
            this.legendFontSize = legendFontSize;
            return this;
        }

        public Builder data(List<DataPointDO<String, Long>> dataPoints) {
            this.dataPoints = dataPoints;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder doughnut() {
            this.doughnut = true;
            return this;
        }

        public Builder noLegend() {
            this.showLegend = false;
            return this;
        }

        public Builder showLegend(boolean showLegend) {
            this.showLegend = showLegend;
            return this;
        }

        public Builder small() {
            this.width = 300;
            this.height = 300;
            return this;
        }

        public Builder width(Integer width) {
            this.width = width;
            return this;
        }

        public Builder height(Integer height) {
            this.height = height;
            return this;
        }

        public ChartDO build() {
            Long chartId = IdGenerator.generateCorrelationId();
            ChartDO chartDO = new ChartDO();

            String chartKey = "p_" + String.valueOf(chartId);
            chartDO.setId(chartId);
            chartDO.setType("pie");
            chartDO.setKey(chartKey);
            chartDO.setTitle(title);

            ChartDatasetDO dataset = new ChartDatasetDO();
            dataset.setBackgroundColor(COLORS);
            dataset.setBorderColor(BORDERS);
            dataset.setBorderWidth(BORDER_WIDTH);
            dataset.setKey(chartKey);
            dataset.setChartKey(chartKey);

            String[] labels = Utils.safeStream(dataPoints).map(dataPoint -> dataPoint.getX()).collect(Collectors.toList()).toArray(new String[dataPoints.size()]);
            Long[] values = Utils.safeStream(dataPoints).map(dataPoint -> dataPoint.getY()).collect(Collectors.toList()).toArray(new Long[dataPoints.size()]);

            dataset.setData(values);

            chartDO.setData(new PieDataDO(labels, Collections.singletonList(dataset)));

//            ChartOptionsDO options = new ChartOptionsDO();
//            options.setAnimation(true);
//            chartDO.setOptions(options);
            StringBuilder builder = new StringBuilder();
            builder.append(" {");

            if (doughnut) {
                builder.append(" cutoutPercentage: 50,");
            }
            builder.append("plugins: {colorschemes: {scheme: 'brewer.Paired12'}},");

            builder.append(" legend: {display: ").append(showLegend).append(",position: '")
                    .append(legendPosition)
                    .append("', labels: { fontColor: '")
                    .append(legendColor)
                    .append("', fontSize: ")
                    .append(legendFontSize)
                    .append("} } ");
            builder.append(" }");
            try {
                chartDO.setOptions(theObjectMapper.readValue(builder.toString(), typeRef));
            } catch (IOException ex) {
                logger.error("Failed to parse options", ex);
            }

            chartDO.setHeight(height);
            chartDO.setWidth(width);

            return chartDO;
        }
    }

}
