package gusl.chart.old.builders;

import gusl.core.utils.IdGenerator;
import gusl.core.utils.Utils;
import gusl.model.chart.ChartCategoryDO;
import gusl.model.chart.ChartDO;
import gusl.model.chart.ChartDatasetDO;
import gusl.model.chart.ChartOptionsDO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class ChartBuilder {

    public static class Builder {

        private String key;
        private String title;
        private final List<ChartDatasetDO> datasets = new LinkedList<>();
        private final List<ChartCategoryDO> categories = new LinkedList<>();
        private ChartOptionsDO options;
        private String type = "line";

        public Builder key(String key) {
            this.key = key;
            return this;
        }

        public Builder bar() {
            this.type = "bar";
            return this;
        }

        public Builder pie() {
            this.type = "pie";
            return this;
        }

        public Builder line() {
            this.type = "line";
            return this;
        }

        public Builder addDataset(ChartDatasetDO dataset) {
            this.datasets.add(dataset);
            return this;
        }

        public Builder addCategory(ChartCategoryDO category) {
            this.categories.add(category);
            return this;
        }

        public Builder addCategory(List<ChartCategoryDO> categories) {
            this.categories.addAll(categories);
            return this;
        }

        public Builder addDataset(List<ChartDatasetDO> datasets) {
            this.datasets.addAll(datasets);
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder options(ChartOptionsDO options) {
            this.options = options;
            return this;
        }

        public ChartDO build() {

            // ensure all dataset s have the same key as the chart
            ChartDO chartDO = new ChartDO();

            Long chartId = IdGenerator.generateCorrelationId();
            chartDO.setId(chartId);

            //AtomicInteger datasetCount = new AtomicInteger(0);
            Utils.safeStream(datasets).forEach(dataset -> {
                dataset.setChartKey(key);
                dataset.setChartId(chartId);
                //dataset.setRank(datasetCount.incrementAndGet());
            });

            //AtomicInteger categoryCount = new AtomicInteger(0);
            Utils.safeStream(categories).forEach(category -> {
                category.setChartKey(key);
                category.setChartId(chartId);
                //category.setRank(categoryCount.incrementAndGet());
            });

            chartDO.setDatasets(datasets);
            chartDO.setCategories(categories);
            chartDO.setKey(key);
            chartDO.setType(type);
            // chartDO.setOptions(options);
            chartDO.setTitle(title);

            return chartDO;

        }

    }
}
