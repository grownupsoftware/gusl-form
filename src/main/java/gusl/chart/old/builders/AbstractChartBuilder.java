package gusl.chart.old.builders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.json.ObjectMapperFactory;

import java.util.HashMap;

public abstract class AbstractChartBuilder {

    protected final static ObjectMapper theObjectMapper = ObjectMapperFactory.getDefaultObjectMapper();

    protected final static TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
    };

    //    protected static final String COLORS[] = new String[]{
//        "rgba(255, 99, 132, 0.2)",
//        "rgba(54, 162, 235, 0.2)",
//        "rgba(255, 206, 86, 0.2)",
//        "rgba(75, 192, 192, 0.2)",
//        "rgba(153, 102, 255, 0.2)",
//        "rgba(255, 159, 64, 0.2)"
//    };
//
//    protected static final String BORDERS[] = new String[]{
//        "rgba(255, 99, 132, 1)",
//        "rgba(54, 162, 235, 1)",
//        "rgba(255, 206, 86, 1)",
//        "rgba(75, 192, 192, 1)",
//        "rgba(153, 102, 255, 1)",
//        "rgba(255, 159, 64, 1)"
//    };
    protected static final int BORDER_WIDTH[] = new int[]{
            1
    };
    protected static final String COLORS[] = new String[]{"#00205a", "#0185b7", "#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", "#ffff99", "#b15928"};
    protected static final String BORDERS[] = new String[]{"#00205a", "#0185b7", "#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", "#ffff99", "#b15928"};

}
