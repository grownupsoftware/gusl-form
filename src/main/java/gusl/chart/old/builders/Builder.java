package gusl.chart.old.builders;

public class Builder {

    public static ChartBuilder.Builder chartDef() {
        return new ChartBuilder.Builder();
    }

    public static PieChartBuilder.Builder pieChart() {
        return new PieChartBuilder.Builder();
    }

    public static GenericChartBuilder.Builder lineChart() {
        return new GenericChartBuilder.Builder("line");
    }

    public static GenericChartBuilder.Builder barChart() {
        return new GenericChartBuilder.Builder("bar");
    }

    public static GenericChartBuilder.Builder horizontalBarChart() {
        return new GenericChartBuilder.Builder("horizontalBar");
    }

    public static ChartOptionsBuilder.Builder options() {
        return new ChartOptionsBuilder.Builder();
    }

    public static ChartDatasetBuilder.Builder dataset() {
        return new ChartDatasetBuilder.Builder();
    }

    public static ChartCategoryBuilder.Builder category() {
        return new ChartCategoryBuilder.Builder();
    }

}
