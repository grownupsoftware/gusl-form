package gusl.chart.old.builders;

import gusl.model.chart.ChartOptionsDO;

public class ChartOptionsBuilder {

    public static class Builder {

        boolean animation = true;
        boolean scaleOverride = true;
        int scaleSteps = 10;
        int scaleStepWidth = 10;
        int scaleStartValue = 0;

        public Builder noAnimation() {
            this.animation = false;
            return this;
        }

        public Builder noScaleOverride() {
            this.scaleOverride = false;
            return this;
        }

        public Builder scaleSteps(int scaleSteps) {
            this.scaleSteps = scaleSteps;
            return this;
        }

        public Builder scaleStepWidth(int scaleStepWidth) {
            this.scaleStepWidth = scaleStepWidth;
            return this;
        }

        public Builder scaleStartValue(int scaleStartValue) {
            this.scaleStartValue = scaleStartValue;
            return this;
        }

        public ChartOptionsDO build() {
            ChartOptionsDO chartOptionsDO = new ChartOptionsDO();
            chartOptionsDO.setAnimation(animation);
            chartOptionsDO.setScaleOverride(scaleOverride);
            chartOptionsDO.setScaleStartValue(scaleStartValue);
            chartOptionsDO.setScaleStepWidth(scaleStepWidth);
            chartOptionsDO.setScaleSteps(scaleSteps);
            return chartOptionsDO;
        }

    }
}
