package gusl.chart.old.builders;

import gusl.core.utils.IdGenerator;
import gusl.model.chart.ChartCategoryDO;

public class ChartCategoryBuilder {

    public static class Builder {

        private String categoryKey;

        private int rank;

        private String label;

        public Builder category(String categoryKey) {
            this.categoryKey = categoryKey;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder rank(int rank) {
            this.rank = rank;
            return this;
        }

        public ChartCategoryDO build() {
            ChartCategoryDO chartCategoryDO = new ChartCategoryDO();
            chartCategoryDO.setId(IdGenerator.generateCorrelationId());
            chartCategoryDO.setCategoryKey(categoryKey);
            chartCategoryDO.setLabel(label);
            chartCategoryDO.setRank(rank);
            return chartCategoryDO;

        }

    }
}
