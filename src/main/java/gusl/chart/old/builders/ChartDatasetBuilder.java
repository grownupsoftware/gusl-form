package gusl.chart.old.builders;

import gusl.core.utils.IdGenerator;
import gusl.model.chart.ChartDatasetDO;

public class ChartDatasetBuilder {

    public static class Builder {

        private String key;
        private String label;
        private int rank;

        private String[] backgroundColor = new String[]{"rgba(151,187,205,0.5)"};
        private String[] borderColor = new String[]{"rgba(151,187,205,0.5)"};
        private int[] borderWidth = new int[]{1};
        private String[] hoverBackgroundColor = new String[]{"rgba(151,187,205,1)"};
        private String[] hoverBorderColor = new String[]{"rgba(151,187,205,1)"};
        private int[] hoverBorderWidth = new int[]{2};

        public Builder key(String key) {
            this.key = key;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder rank(int rank) {
            this.rank = rank;
            return this;
        }

        public Builder backgroundColor(String... backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        public Builder borderColor(String... borderColor) {
            this.borderColor = borderColor;
            return this;
        }

        public Builder hoverBackgroundColor(String... hoverBackgroundColor) {
            this.hoverBackgroundColor = hoverBackgroundColor;
            return this;
        }

        public Builder hoverBorderColor(String... hoverBorderColor) {
            this.hoverBorderColor = hoverBorderColor;
            return this;
        }

        public Builder hoverBorderWidth(int... hoverBorderWidth) {
            this.hoverBorderWidth = hoverBorderWidth;
            return this;
        }

        public Builder borderWidth(int... borderWidth) {
            this.borderWidth = borderWidth;
            return this;
        }

        public ChartDatasetDO build() {
            ChartDatasetDO chartDatasetDO = new ChartDatasetDO();
            chartDatasetDO.setId(IdGenerator.generateCorrelationId());
            chartDatasetDO.setKey(key);
            chartDatasetDO.setLabel(label);
            chartDatasetDO.setBackgroundColor(backgroundColor);
            chartDatasetDO.setBorderColor(borderColor);
            chartDatasetDO.setBorderWidth(borderWidth);
            chartDatasetDO.setHoverBackgroundColor(hoverBackgroundColor);
            chartDatasetDO.setHoverBorderColor(hoverBorderColor);
            chartDatasetDO.setHoverBorderWidth(hoverBorderWidth);
            chartDatasetDO.setRank(rank);
            return chartDatasetDO;
        }

    }
}
