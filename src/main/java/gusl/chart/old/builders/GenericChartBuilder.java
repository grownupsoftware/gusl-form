package gusl.chart.old.builders;

import gusl.core.lambda.MutableInteger;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.IdGenerator;
import gusl.core.utils.Utils;
import gusl.model.chart.ChartDO;
import gusl.model.chart.ChartDatasetDO;
import gusl.model.chart.DatasetDO;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class GenericChartBuilder extends AbstractChartBuilder {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(GenericChartBuilder.class);

    public static class Builder {

        private final String type;

        private Integer height = 400;
        private Integer width = 800;
        private List<DatasetDO> data = new LinkedList<>();
        private List<String> labels = new LinkedList<>();
        private String title;
        private boolean showLegend = true;
        private boolean fill = false;
        private boolean stacked = false;
        private boolean noLines = false;
        public String legendColor = "rgb(98, 93, 93)";
        public int strokeWidth = 1;
        public int legendFontSize = 10;
        public String legendPosition = "bottom";
        private Integer pointRadius = 2;

        private String xAxisLabel = "";

        private String yAxisLabel = "";

        private Integer minX;
        private Integer maxX;
        private Integer stepX;

        private Integer minY;
        private Integer maxY;
        private Integer stepY;

        public Builder(String type) {
            this.type = type;
        }

        public Builder minX(int minX) {
            this.minX = minX;
            return this;
        }

        public Builder xAxisLabel(String xAxisLabel) {
            this.xAxisLabel = xAxisLabel;
            return this;
        }

        public Builder yAxisLabel(String yAxisLabel) {
            this.yAxisLabel = yAxisLabel;
            return this;
        }

        public Builder maxX(int maxX) {
            this.maxX = maxX;
            return this;
        }

        public Builder stepX(int stepX) {
            this.stepX = stepX;
            return this;
        }

        public Builder minY(int minY) {
            this.minY = minY;
            return this;
        }

        public Builder maxY(int maxY) {
            this.maxY = maxY;
            return this;
        }

        public Builder stepY(int stepY) {
            this.stepY = stepY;
            return this;
        }

        public Builder legendPosition(String legendPosition) {
            this.legendPosition = legendPosition;
            return this;
        }

        public Builder legendColor(String legendColor) {
            this.legendColor = legendColor;
            return this;
        }

        public Builder legendFontSize(int legendFontSize) {
            this.legendFontSize = legendFontSize;
            return this;
        }

        public Builder pointRadius(int pointRadius) {
            this.pointRadius = pointRadius;
            return this;
        }

        public Builder noPointRadius() {
            this.pointRadius = null;
            return this;
        }

        public Builder strokeWidth(int strokeWidth) {
            this.strokeWidth = strokeWidth;
            return this;
        }

        public Builder fill() {
            this.fill = true;
            return this;
        }

        public Builder stacked() {
            this.stacked = true;
            return this;
        }

        public Builder noLines() {
            this.noLines = true;
            return this;
        }

        public Builder noLegend() {
            this.showLegend = false;
            return this;
        }

        public Builder labels(List<String> labels) {
            this.labels.addAll(labels);
            return this;
        }

        public Builder labels(String... labels) {
            if (labels != null) {
                this.labels.addAll(Arrays.asList(labels));
            }
            return this;
        }

        public Builder addLabel(String... labels) {
            if (labels != null) {
                this.labels.addAll(Arrays.asList(labels));
            }
            return this;
        }

        public Builder addDataset(DatasetDO line) {
            this.data.add(line);
            return this;
        }

        public Builder addDataset(List<DatasetDO> lines) {
            this.data.addAll(lines);
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder showLegend(boolean showLegend) {
            this.showLegend = showLegend;
            return this;
        }

        public Builder small() {
            this.width = 300;
            this.height = 300;
            return this;
        }

        public Builder width(Integer width) {
            this.width = width;
            return this;
        }

        public Builder height(Integer height) {
            this.height = height;
            return this;
        }

        public ChartDO build() {
            Long chartId = IdGenerator.generateCorrelationId();
            ChartDO chartDO = new ChartDO();

            String chartKey = "l_" + String.valueOf(chartId);
            chartDO.setId(chartId);
            chartDO.setType(type);
            chartDO.setKey(chartKey);
            chartDO.setTitle(title);

            chartDO.setLabels(labels);

            chartDO.setXAxisLabel(xAxisLabel);
            chartDO.setYAxisLabel(yAxisLabel);

            MutableInteger index = new MutableInteger(1);

            chartDO.setDatasets(Utils.safeStream(data).map(line -> {
                if (index.get() > COLORS.length) {
                    index.set(1);
                }

                ChartDatasetDO dataset = new ChartDatasetDO();
                dataset.setFill(fill);
                if ("line".equals(type)) {
                    dataset.setBackgroundColor(new String[]{COLORS[index.get() - 1]});
                    dataset.setBorderColor(new String[]{BORDERS[index.get() - 1]});
                    dataset.setBorderWidth(new int[]{strokeWidth});
                } else {
                    dataset.setBackgroundColor(COLORS);
                    dataset.setBorderColor(BORDERS);
                    dataset.setBorderWidth(new int[]{strokeWidth});
                }
                // dataset.setBorderWidth(new int[]{strokeWidth, strokeWidth, strokeWidth, strokeWidth, strokeWidth, strokeWidth});
                dataset.setKey(chartKey + index.getAndIncrement());
                dataset.setChartKey(chartKey);
                dataset.setLabel(line.getLabel());
                dataset.setPointRadius(pointRadius);

                dataset.setData(line.getData().toArray(new Long[line.getData().size()]));

                return dataset;
            }).collect(Collectors.toList()));

            StringBuilder builder = new StringBuilder();
            builder.append(" {");
            builder.append(" animation: { duration: 500 },");

            if (noLines || stacked || minX != null || maxX != null || stepX != null || minY != null || maxY != null || stepY != null) {
                StringBuilder xBuilder = new StringBuilder();
                StringBuilder yBuilder = new StringBuilder();

                xBuilder.append(" scales: { xAxes: [{");
                yBuilder.append(" scales: { yAxes: [{");

                if (stacked) {
                    xBuilder.append(" stacked: true, ");
                    yBuilder.append(" stacked: true, ");
                }
                if (noLines) {
                    xBuilder.append(" gridLines: {display:false}, ");
                    yBuilder.append(" gridLines: {display:false}, ");
                }

                if (minX != null || maxX != null || stepX != null) {
                    xBuilder.append(" ticks: {");
                    if (minX != null) {
                        xBuilder.append(" min:").append(minX);
                    }
                    if (maxX != null) {
                        xBuilder.append(" max:").append(maxX);
                    }
                    if (stepX != null) {
                        xBuilder.append(" stepSize:").append(stepX);
                    }
                    xBuilder.append(" }, ");
                }
                if (minY != null || maxY != null || stepY != null) {
                    yBuilder.append(" ticks: {");
                    if (minX != null) {
                        yBuilder.append(" min:").append(minY);
                    }
                    if (maxX != null) {
                        yBuilder.append(" max:").append(maxY);
                    }
                    if (stepX != null) {
                        yBuilder.append(" stepSize:").append(stepY);
                    }
                    yBuilder.append(" }, ");
                }

                xBuilder.append("}] },");
                yBuilder.append("}] },");
            }

            // builder.append("scales: {yAxes: [{ticks: {beginAtZero: true}}]}, ");
            // themes found at https://nagix.github.io/chartjs-plugin-colorschemes/colorchart.html
            // builder.append("plugins: {colorschemes: {scheme: 'brewer.Paired12'}},");
            builder.append(" legend: {display: ").append(showLegend).append(",position: '")
                    .append(legendPosition)
                    .append("', labels: { fontColor: '")
                    .append(legendColor)
                    .append("', fontSize: ")
                    .append(legendFontSize)
                    .append("} } ");

            builder.append(" }");

            try {
                chartDO.setOptions(theObjectMapper.readValue(builder.toString(), typeRef));
            } catch (IOException ex) {
                logger.error("Failed to parse options", ex);
            }

            chartDO.setHeight(height);
            chartDO.setWidth(width);

            return chartDO;
        }
    }

}
