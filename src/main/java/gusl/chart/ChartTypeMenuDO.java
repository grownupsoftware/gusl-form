package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChartTypeMenuDO {
    private ChartType type;
    private String command;
    private String icon;
    private String label;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
