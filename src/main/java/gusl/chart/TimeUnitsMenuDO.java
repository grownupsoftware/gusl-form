package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TimeUnitsMenuDO {

    private TimeUnitType inputType;
    private String label;
    private String rangeQuery;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
