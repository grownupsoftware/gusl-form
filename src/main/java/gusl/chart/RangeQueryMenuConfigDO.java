package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RangeQueryMenuConfigDO {
    private String fieldName;
    private boolean byDay;
    private boolean byWeek;
    private boolean byMonth;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
