package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TicksDO {
    private String color;
    private FontDO font;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
