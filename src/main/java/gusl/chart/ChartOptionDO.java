package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class ChartOptionDO {

    private Double borderWidth;
    private Double barPercentage;
    private Double barThickness;
    private ChartCanvasDO chart;
    private boolean maintainAspectRatio;
    private boolean responsive;
    private boolean fill;
    private PluginsDO plugins;
    private ScalesDO scales;
    private Double pointRadius;
    private Double tension;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
