package gusl.chart;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChartTypeMenuConfigDO {

    private boolean line;
    private boolean bar;
    private boolean radar;
    private boolean bubble;
    private boolean polarArea;
    private boolean doughnut;
    private boolean pie;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
