package gusl.form.security.filters;

import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.exceptions.ValidationException;
import gusl.model.googlesso.GoogleLoginRequestDO;
import gusl.model.googlesso.GoogleLoginResponseDO;
import org.jvnet.hk2.annotations.Contract;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.SecurityContext;
import java.util.Map;
import java.util.Optional;

@Contract
public interface GUSLSecurityService {

    // static final String GOOGLE_SSO_TOKEN_NAME = "access_token";

    GoogleLoginResponseDO login(GoogleLoginRequestDO loginRequest, HttpServletRequest request) throws ValidationException, GUSLErrorException;

    ILoggedInUser getLoggedInDetails(HttpServletRequest request) throws GUSLErrorException;

    Optional<ILoggedInUser> getLoggedInForToken(String token);

    SecurityContext getSecurityContextFor(Map<String, String> headers) throws ValidationException;

}
