package gusl.form.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

import static java.util.Objects.nonNull;

public enum FormErrors {
    NO_PACKAGE_PREFIX_DEFINED("FRME01 No package prefixes defined, call initialise('package')", "no.package.names.defined"),
    NO_INSTANCE_CREATOR_DEFINED("FRME02 No instance creator defined, call initialise('package')", "no.instance.creator.defined"),
    // ADMIN USER - GOOGLE
    ERR_GOOGLE_TOKEN_IS_NULL("FRME02 Google SSO token cannot be null", "google.sso.token.isnull"),
    ERR_GOOGLE_TOKEN_VALIDATION("FRME02 Failed to validate Google SSO token", "google.sso.error.in.validation"),
    ERR_GOOGLE_TOKEN_VERIFY("FRME02 Failed to verify Google SSO token", "google.sso.error.in.verify"),
    ERR_GOOGLE_TOKEN_INVALID_HOSTNAME("FRME02 Invalid hostname [{0}] is Google SSO token", "google.sso.invalid.hostname"),
    ERR_GOOGLE_TOKEN_EXPIRED("TFRME02 oken expired", "google.sso.token.expired"),
    ERR_GOOGLE_TOKEN_UNKNOWN_EMAIL("FRME02 Email [{0}] not registered - please ask administrator to add", "email.not.registered"),
    ERR_GOOGLE_TOKEN_ACCESS_DENIED("FRME02 Access is denied for [{0}]- please speak to the system administrator", "email.access.denied"),
    ERR_GOOGLE_TOKEN_INVALID_ID("FRME02 Invalid id [{0}] does not match [{1}] in Google SSO token", "google.sso.invalid.id"),
    ERR_GOOGLE_TOKEN_INVALID_EMAIL("FRME02 Invalid email [{0}] does not match [{1}] in Google SSO token", "google.sso.invalid.email"),
    ERR_GOOGLE_TOKEN_INVALID_EMAIL_VALIDATION("FRME02 Email [{0}] in Google SSO token failed validation", "google.sso.invalid.email.validation"),
    ERR_GOOGLE_TOKEN_INVALID_NAME("FRME02 Invalid name [{0}] does not match [{1}] in Google SSO token", "google.sso.invalid.name");

    private String field;
    private final String message;
    private final String messageKey;

    FormErrors(String field, String message, String messageKey) {
        this.field = field;
        this.message = message;
        this.messageKey = messageKey;
    }

    FormErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        if (field != null) {
            return new ErrorDO(field, message, messageKey);
        } else {
            return new ErrorDO(message, messageKey);
        }

    }

    public ErrorDO getError(Long id) {
        if (nonNull(field)) {
            if (nonNull(id)) {
                return new ErrorDO(field, message, messageKey, String.valueOf(id));
            } else {
                return new ErrorDO(field, message, messageKey);
            }
        } else {
            if (nonNull(id)) {
                return new ErrorDO(null, message, messageKey, String.valueOf(id));
            } else {
                return new ErrorDO(message, messageKey);
            }
        }
    }

    public ErrorDO getError(String... params) {
        if (nonNull(field)) {
            if (nonNull(params)) {
                return new ErrorDO(field, message, messageKey, params);
            } else {
                return new ErrorDO(field, message, messageKey);
            }
        } else {
            if (nonNull(params)) {
                return new ErrorDO(null, message, messageKey, params);
            } else {
                return new ErrorDO(message, messageKey);
            }
        }
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }
}
