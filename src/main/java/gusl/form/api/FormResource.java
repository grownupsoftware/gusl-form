package gusl.form.api;

import gusl.auth.model.loggedin.LoggedInUser;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.form.model.GuslFormConfigDO;
import gusl.form.model.MaintainConfigDO;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Singleton
@Path(value = "form")
@PermitAll // remove me
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FormResource {

    @Inject
    private FormService theFormService;

    @GET
    @Path("{key}")
    @RolesAllowed({FormRoles.FORMS})
    public MaintainConfigDO getMaintain(
            @Context HttpServletResponse httpResponse,
            @LoggedInUser LoggedInUserDO loggedInUser,
            @PathParam("key") String key
    ) throws GUSLErrorException {
        return theFormService.getMaintainForm(loggedInUser, key);
    }

    @GET
    @Path("/system")
    @RolesAllowed({FormRoles.FORMS})
    public GuslFormConfigDO getSystem(
            @Context HttpServletResponse httpResponse,
            @LoggedInUser LoggedInUserDO loggedInUser
    ) throws GUSLErrorException {
        return theFormService.getSystem(loggedInUser);
    }

}
