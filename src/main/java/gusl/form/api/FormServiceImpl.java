package gusl.form.api;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.form.model.GuslFormConfigDO;
import gusl.form.model.MaintainConfigDO;
import gusl.form.registra.FormRegistra;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

@Service
@CustomLog
public class FormServiceImpl implements FormService {

    @Inject
    private FormRegistra theFormRegistra;

    @Override
    public MaintainConfigDO getMaintainForm(LoggedInUserDO loggedInUser, String key) throws GUSLErrorException {
        return theFormRegistra.get(loggedInUser, key);
    }

    @Override
    public GuslFormConfigDO getSystem(LoggedInUserDO loggedInUser) throws GUSLErrorException {
        return theFormRegistra.getSystem(loggedInUser);
    }
}
