package gusl.form.api;

import gusl.model.role.RoleDefinitions;
import gusl.model.role.RoleGrouping;

public class FormRoles implements RoleDefinitions {

    public static final String NAME = "SYSTEM";

    public static final String BASE = NAME + "_";

    public static final String FORMS = BASE + "FORMS"; //<-- if you change this also change RoleCacheImpl

    @Override
    public RoleGrouping getRoles() {
        return RoleGrouping.builder()
                .name(NAME)
                .roles(new String[]{
                        FORMS
                }).build();
    }
}
