package gusl.form.api;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.form.model.GuslFormConfigDO;
import gusl.form.model.MaintainConfigDO;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface FormService {
    MaintainConfigDO getMaintainForm(LoggedInUserDO loggedInUser, String key) throws GUSLErrorException;

    GuslFormConfigDO getSystem(LoggedInUserDO loggedInUser) throws GUSLErrorException;

}
