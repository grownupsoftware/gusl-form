package gusl.form.interfaces;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.form.builder.toolbar.ToolbarActionBuilder;

public interface IToolbarActionBuilder {
    ToolbarActionBuilder build(LoggedInUserDO loggedInUser);

}
