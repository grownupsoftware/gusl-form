package gusl.form.interfaces;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.form.builder.lookup.LookupBuilder;

public interface ILookupBuilder {
    LookupBuilder build(LoggedInUserDO loggedInUser);
}
