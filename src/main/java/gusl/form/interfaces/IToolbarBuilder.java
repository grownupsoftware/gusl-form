package gusl.form.interfaces;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.form.builder.toolbar.ToolbarBuilder;

public interface IToolbarBuilder {
    ToolbarBuilder build(LoggedInUserDO loggedInUser);
}
