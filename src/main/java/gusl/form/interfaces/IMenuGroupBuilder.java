package gusl.form.interfaces;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.form.model.MenuGroupDO;

public interface IMenuGroupBuilder {
    MenuGroupDO build(LoggedInUserDO loggedInUser);
}
