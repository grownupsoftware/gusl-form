package gusl.form.utils;

import gusl.auth.model.loggedin.PasswordChangePeriod;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PasswordConfigDO {
    private int minimumLength;
    private int minimumNumerics;
    private int minimumUpper;
    private int minimumAlpha;
    private int minimumSpecial;
    private PasswordChangePeriod changePeriod;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
