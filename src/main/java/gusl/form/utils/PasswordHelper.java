package gusl.form.utils;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PasswordHelper {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(PasswordHelper.class);

    private static final String NUMERICS = "0123456789";
    private static final String SPECIAL = "!@#$%&";
    private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final static Random theRandom = new Random();

    public static String getRequirementsMessage(PasswordConfigDO config) {
        StringBuilder builder = new StringBuilder();
        builder.append("Minimum ").append(config.getMinimumLength()).append(" characters.");
        if ((config.getMinimumNumerics() + config.getMinimumSpecial() + config.getMinimumUpper()) > 0) {
            builder.append(" (with at least ");
        }
        if (config.getMinimumNumerics() > 0) {
            builder.append(config.getMinimumNumerics()).append(" numeric ");
        }
        if (config.getMinimumUpper() > 0) {
            builder.append(config.getMinimumUpper()).append(" uppercase ");
        }
        if (config.getMinimumSpecial() > 0) {
            builder.append(config.getMinimumSpecial()).append(" special ");
        }
        if (config.getMinimumAlpha() > 0) {
            builder.append(config.getMinimumAlpha()).append(" alpha ");
        }
        if ((config.getMinimumNumerics() + config.getMinimumSpecial() + config.getMinimumUpper()) > 0) {
            builder.append(")");
        }
        return builder.toString();
    }

    public static String generateRandomPassword(PasswordConfigDO config) {
        String password = generateRandomPassword(NUMERICS, config.getMinimumNumerics());
        password += generateRandomPassword(SPECIAL, config.getMinimumSpecial());
        password += generateRandomPassword(UPPER, config.getMinimumUpper());
        int remainingReqd = config.getMinimumLength() - (config.getMinimumNumerics() + config.getMinimumSpecial() + config.getMinimumUpper());
        if (remainingReqd < config.getMinimumAlpha()) {
            remainingReqd = config.getMinimumAlpha();
        }
        password += generateRandomPassword(ALPHA, remainingReqd);
        return shuffle(password);
    }

    public static boolean validatePassword(PasswordConfigDO config, String password) {
        return validatePassword(config, password, false);
    }

    public static boolean validatePassword(PasswordConfigDO config, String password, boolean debug) {
        if (StringUtils.isBlank(password)) {
            return false;
        }
        if (password.length() < config.getMinimumLength()) {
            return false;
        }
        int whiteSpace = 0;
        int numerics = 0;
        int alpha = 0;
        int special = 0;
        int upper = 0;
        for (int x = 0; x < password.length(); x++) {
            int c = password.codePointAt(x);
            if (Character.isWhitespace(c)) {
                whiteSpace++;
            } else if (Character.isDigit(c)) {
                numerics++;
            } else if (SPECIAL.contains(String.valueOf(password.charAt(x)))) {
                special++;
            } else if (Character.isLetter(c) && String.valueOf(password.charAt(x)).equals(String.valueOf(password.charAt(x)).toUpperCase())) {
                upper++;
            } else if (Character.isLetter(c)) {
                alpha++;
            }
        }

        if (debug || logger.isDebugEnabled()) {
            log(config, password, numerics, alpha, special, upper);
        }

        return special >= config.getMinimumSpecial()
                && numerics >= config.getMinimumNumerics()
                && upper >= config.getMinimumUpper()
                && alpha >= config.getMinimumAlpha();
    }

    private static void log(PasswordConfigDO config, String password, int numerics, int alpha, int special, int upper) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n").append("password ").append(password).append("\n");
        builder.append("\t Req ").append(config.getMinimumLength()).append(" minimum characters found ").append(numerics + alpha + special + upper).append("\n");
        builder.append("\t Req ").append(config.getMinimumNumerics()).append(" Numerics found ").append(numerics).append("\n");
        builder.append("\t Req ").append(config.getMinimumUpper()).append(" Uppercase found ").append(upper).append("\n");
        builder.append("\t Req ").append(config.getMinimumSpecial()).append(" Special found ").append(special).append("\n");
        builder.append("\t Req ").append(config.getMinimumAlpha()).append(" Alpha found ").append(alpha).append("\n");
        builder.append("\t valid? ").append(special >= config.getMinimumSpecial()
                && numerics >= config.getMinimumNumerics()
                && upper >= config.getMinimumUpper()
                && alpha >= config.getMinimumAlpha()).append("\n");
        logger.info("Validation: {}", builder.toString());

    }

//    public String generateRandomPassword(int len) {
//        String password = generateRandomPassword("0123456789", 1);
//        password += generateRandomPassword("!@#$%&", 1);
//        password += generateRandomPassword("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi"
//                + "jklmnopqrstuvwxyz!@#$%&", len - 2);
//        return shuffle(password);
//    }

    public static String generateRandomPassword(String baseString, int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(baseString.charAt(theRandom.nextInt(baseString.length())));
        }
        return sb.toString();
    }

    public static String shuffle(String input) {
        List<Character> characters = new ArrayList<Character>();
        for (char c : input.toCharArray()) {
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while (characters.size() != 0) {
            int randPicker = (int) (Math.random() * characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }

}
