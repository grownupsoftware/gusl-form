package gusl.form.registra;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.form.model.GuslFormConfigDO;
import gusl.form.model.MaintainConfigDO;
import gusl.form.registra.CallbackHelper.CreateInstanceCallbackFunction;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface FormRegistra {

    void initialise(CreateInstanceCallbackFunction callbackInstanceCreator,
                    CreateInstanceCallbackFunction toolbarCallbackInstanceCreator,
                    CreateInstanceCallbackFunction lookupCallbackInstanceCreator,
                    CreateInstanceCallbackFunction actionCallbackInstanceCreator,
                    String... packagePrefixes) throws GUSLErrorException;

    MaintainConfigDO get(LoggedInUserDO loggedInUser, String key) throws GUSLErrorException;

    GuslFormConfigDO getSystem(LoggedInUserDO loggedInUser) throws GUSLErrorException;

}
