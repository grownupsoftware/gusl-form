package gusl.form.registra;

import gusl.annotations.form.LookupConfig;
import gusl.annotations.form.pagetypes.UiForm;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.exceptions.GUSLException;
import gusl.core.lambda.MutableBoolean;
import gusl.core.utils.StringUtils;
import gusl.form.builder.FormBuilder;
import gusl.form.builder.lookup.LookupBuilder;
import gusl.form.builder.toolbar.TickerToolbarItem;
import gusl.form.builder.toolbar.ToolbarActionBuilder;
import gusl.form.builder.toolbar.ToolbarBuilder;
import gusl.form.builder.toolbar.ToolbarItem;
import gusl.form.errors.FormErrors;
import gusl.form.interfaces.ILookupBuilder;
import gusl.form.interfaces.IMenuGroupBuilder;
import gusl.form.interfaces.IToolbarActionBuilder;
import gusl.form.interfaces.IToolbarBuilder;
import gusl.form.model.*;
import gusl.form.registra.CallbackHelper.CreateInstanceCallbackFunction;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;
import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.Annotation;
import java.util.*;

import static gusl.auth.model.roles.RoleHelper.hasPermissionOrIsSuperUser;
import static gusl.core.utils.Utils.safeStream;
import static gusl.form.builder.FormBuildHelper.getFieldsForClass;
import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toUnmodifiableList;

@Service
@CustomLog
public class FormRegistraImpl implements FormRegistra {

    private Reflections theReflections;

    private final Map<String, MaintainConfigDO> maintainConfigMap = new HashMap<>();

    private List<FormDO> theUiForms;

    private List<IMenuGroupBuilder> theMenuBuilders;

    private List<IToolbarBuilder> theToolbarBuilders;

    private List<ILookupBuilder> theLookupBuilders;

    private List<IToolbarActionBuilder> theToolbarActionBuilders;

    private String[] thePackagePrefixes;

    CreateInstanceCallbackFunction<Class<? extends IMenuGroupBuilder>, IMenuGroupBuilder> theMenuCallbackInstanceCreator;
    CreateInstanceCallbackFunction<Class<? extends IToolbarBuilder>, IToolbarBuilder> theToolbarCallbackInstanceCreator;
    CreateInstanceCallbackFunction<Class<? extends ILookupBuilder>, ILookupBuilder> theLookupCallbackInstanceCreator;
    CreateInstanceCallbackFunction<Class<? extends IToolbarActionBuilder>, IToolbarActionBuilder> theToolbarActionCallbackInstanceCreator;

    @Override
    public void initialise(
            CreateInstanceCallbackFunction menuCallbackInstanceCreator,
            CreateInstanceCallbackFunction toolbarCallbackInstanceCreator,
            CreateInstanceCallbackFunction lookupCallbackInstanceCreator,
            CreateInstanceCallbackFunction actionCallbackInstanceCreator,
            String... packagePrefixes)
            throws GUSLErrorException {
        thePackagePrefixes = packagePrefixes;
        theMenuCallbackInstanceCreator = menuCallbackInstanceCreator;
        theToolbarCallbackInstanceCreator = toolbarCallbackInstanceCreator;
        theLookupCallbackInstanceCreator = lookupCallbackInstanceCreator;
        theToolbarActionCallbackInstanceCreator = actionCallbackInstanceCreator;
    }

    @Override
    public MaintainConfigDO get(LoggedInUserDO loggedInUser, String key) throws GUSLErrorException {
        return maintainConfigMap.get(key);
    }

    @Override
    public GuslFormConfigDO getSystem(LoggedInUserDO loggedInUser) throws GUSLErrorException {
        GuslFormConfigDO menuSystem = buildSystem(loggedInUser);
        // logger.debug("menuSystem: {}", menuSystem);

        if (isNull(theUiForms)) {
            theUiForms = getUiForms();
        }

        menuSystem.setForms(theUiForms);
        // logger.debug("menuSystem: {}", menuSystem);
        return menuSystem;
    }

    private GuslFormConfigDO buildSystem(LoggedInUserDO loggedInUser) throws GUSLErrorException {
        if (isNull(theMenuBuilders)) {
            getMenuGroupBuilders();
        }
        if (isNull(theToolbarBuilders)) {
            getToolbarBuilders();
        }
        if (isNull(theLookupBuilders)) {
            getLookupBuilders();
        }

        if (isNull(theToolbarActionBuilders)) {
            getToolbarActionBuilders();
        }
        final ToolbarBuilder toolbarBuilder = buildToolbar(loggedInUser);
        final LookupBuilder lookupBuilder = buildLookups(loggedInUser);
        final ToolbarActionBuilder actionBuilder = buildActions(loggedInUser);

        return GuslFormConfigDO.builder()
                .menuGroups(buildMenuGroups(loggedInUser))
                .toolbarItems(toolbarBuilder.getItems())
                .toolbarTickers(toolbarBuilder.getTickers())
                .toolbarMenus(toolbarBuilder.getMenus())
                .lookups(convertLookupToDTO(lookupBuilder.getLookups()))
                .actions(actionBuilder.getActions())
                .build();
    }

    private List<LookupConfigDTO> convertLookupToDTO(List<LookupConfig> lookups) {
        return safeStream(lookups).map(lookup -> LookupConfigDTO.builder()
                .name(lookup.getName())
                .extractLabel(lookup.getExtractLabel())
                .tableFields(lookup.getTableFields())
                .fieldTypes(lookup.getFieldTypes())
                .sortBy(lookup.getSortBy())
                .pageable(lookup.isPageable())
                .externalUrl(lookup.getExternalUrl())
                .uniqueId(lookup.getUniqueId())
                .retValue(lookup.getRetValue())
                .inline(lookup.isInline())
                .lookupClass(lookup.getLookupClass())
                .fields(getFieldsForClass(lookup.getLookupClass()))
                .build()).collect(toList());
    }

    private ToolbarBuilder buildToolbar(final LoggedInUserDO loggedInUser) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\t %-10s\n", "Toolbar"));
        builder.append(String.format("\t %-10s\n", "-------"));

        List<TickerToolbarItem> tickers = new ArrayList<>();
        List<ToolbarItem> items = new ArrayList<>();
        List<MenuGroupDO> menuGroups = new ArrayList<>();

        safeStream(theToolbarBuilders)
                .forEach(toolbarBuilder -> {
                    builder.append(String.format("\t %-10s", toolbarBuilder.getClass().getCanonicalName())).append("\n");
                    final ToolbarBuilder build = toolbarBuilder.build(loggedInUser);
                    if (nonNull(build.getTickers()) && !build.getTickers().isEmpty()) {
                        tickers.addAll(build.getTickers());
                    }
                    if (nonNull(build.getItems()) && !build.getItems().isEmpty()) {
                        items.addAll(build.getItems());
                    }
                    if (nonNull(build.getMenus()) && !build.getMenus().isEmpty()) {
                        menuGroups.addAll(build.getMenus());
                    }
                });

        logger.debug("Form menus for User {}: \n{}\n", loggedInUser.getUsername(), builder.toString());
        tickers.sort(comparing(TickerToolbarItem::getDisplayOrder,
                Comparator.nullsLast(naturalOrder())));

        items.sort(comparing(ToolbarItem::getDisplayOrder,
                Comparator.nullsLast(naturalOrder())));

        menuGroups.sort(comparing(MenuGroupDO::getDisplayOrder,
                Comparator.nullsLast(naturalOrder())));

        return ToolbarBuilder.builder().tickers(tickers).items(items).menus(menuGroups).build();

    }

    private ToolbarActionBuilder buildActions(final LoggedInUserDO loggedInUser) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\t %-10s\n", "Action"));
        builder.append(String.format("\t %-10s\n", "------"));

        List<ActionConfigDO> actions = new ArrayList<>();

        safeStream(theToolbarActionBuilders)
                .forEach(actionBuilder -> {
                    builder.append(String.format("\t %-10s", actionBuilder.getClass().getCanonicalName())).append("\n");
                    final ToolbarActionBuilder build = actionBuilder.build(loggedInUser);
                    if (nonNull(build.getActions()) && !build.getActions().isEmpty()) {
                        actions.addAll(build.getActions());
                    }
                });

        logger.debug("Actions for Roles {}: \n{}\n", loggedInUser.getUsername(), builder.toString());

        return ToolbarActionBuilder.builder().actions(actions).build();
    }

    private LookupBuilder buildLookups(final LoggedInUserDO loggedInUser) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\t %-10s\n", "Lookup"));
        builder.append(String.format("\t %-10s\n", "----"));

        List<LookupConfig> lookups = new ArrayList<>();

        safeStream(theLookupBuilders)
                .forEach(lookupBuilder -> {
                    builder.append(String.format("\t %-10s", lookupBuilder.getClass().getCanonicalName())).append("\n");
                    final LookupBuilder build = lookupBuilder.build(loggedInUser);
                    if (nonNull(build.getLookups()) && !build.getLookups().isEmpty()) {
                        lookups.addAll(build.getLookups());
                    }
                });

        logger.debug("Lookup for Roles {}: \n{}\n", loggedInUser.getUsername(), builder.toString());

        return LookupBuilder.builder().lookups(lookups).build();
    }

    private List<MenuGroupDO> buildMenuGroups(final LoggedInUserDO loggedInUser) {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\t %-10s %-2s\n", "Menu", " #"));
        builder.append(String.format("\t %-10s %-2s\n", "----", "--"));

        final List<MenuGroupDO> menuGroups = safeStream(theMenuBuilders)
                .map(menuBuilder -> {
                    final MenuGroupDO menuGroup = menuBuilder.build(loggedInUser);
                    if (isNull(menuGroup)) {
                        return null;
                    }
                    builder.append(String.format("\t %-10s %2d", menuBuilder.getClass().getCanonicalName(), isNull(menuGroup.getMenuItems()) ? 0 : menuGroup.getMenuItems().size()))
                            .append("\n");
                    return stripOutChildElementsWithNoPermission(loggedInUser, menuGroup);
                })
                .filter(Objects::nonNull)
                .collect(toList());

        logger.debug("Form menus for Roles User:{} SuperUser:{}: \n{} \n", loggedInUser.getUsername(), loggedInUser.isSuperUser(), builder.toString());

        applyPermissions(loggedInUser, menuGroups);

        final List<MenuGroupDO> allowedMenuGroups = safeStream(menuGroups)
                .filter(menuGroup -> nonNull(menuGroup.getMenuItems()) && !menuGroup.getMenuItems().isEmpty())
                .sorted(comparing(MenuGroupDO::getDisplayOrder, Comparator.nullsLast(naturalOrder())))
                .collect(toUnmodifiableList());

        StringBuilder builder2 = new StringBuilder();
        builder2.append("After permissions applied: \n");
        builder2.append(String.format("\t %-30s %-2s\n", "Menu", " #"));
        builder2.append(String.format("\t %-30s %-2s\n", "----", "--"));
        safeStream(allowedMenuGroups).forEach(group -> {
            builder2.append(String.format("\t %-30s %2d", group.getLabel(), isNull(group.getMenuItems()) ? 0 : group.getMenuItems().size()))
                    .append("\n");

        });

        logger.debug("allowedMenuGroups for {}\n {}", loggedInUser.getUsername(), builder2.toString());
        return allowedMenuGroups;

    }

    private MenuGroupDO stripOutChildElementsWithNoPermission(LoggedInUserDO loggedInUser, MenuGroupDO menuGroup) {

        // Logged in user has correct cohortId for menu group
        if (nonNull(loggedInUser.getCurrentCohortId())
                && nonNull(menuGroup.getAllowedCohorts())
                && nonNull(menuGroup.getAllowedCohorts().getIds())
                && !menuGroup.getAllowedCohorts().getIds().isEmpty()) {
            if (!menuGroup.getAllowedCohorts().getIds().contains(loggedInUser.getCurrentCohortId())) {
                logger.temp("ignoring {} as in wrong cohort", menuGroup.getLabel());
                return null;
            }
        }

//        if (loggedInUser.isSuperUser()) {
//            return menuGroup;
//        }
        menuGroup.setMenuItems(safeStream(menuGroup.getMenuItems())
                .filter(menuItem -> {
                    if (isNull(menuItem.getAllowedCohorts()) || isNull(menuItem.getAllowedCohorts().getIds()) || isNull(loggedInUser.getCurrentCohortId())) {
                        // default - if no cohort specified they get access
                        return true;
                    }
                    return menuItem.getAllowedCohorts().getIds().contains(loggedInUser.getCurrentCohortId());
                })
                .map(menuItem -> {

                    menuItem.setSingleForm(filterForm(loggedInUser, menuItem.getSingleForm()));
                    menuItem.setSinglePage(filterPage(loggedInUser, menuItem.getSinglePage()));

                    if (nonNull(menuItem.getEntityGroup())) {
                        menuItem.getEntityGroup().setMenuItems(safeStream(menuItem.getEntityGroup().getMenuItems())
                                .map(tab -> {
                                    if (passesPermission(loggedInUser, tab.getPermission())) {
                                        tab.setSingleForm(filterForm(loggedInUser, tab.getSingleForm()));
                                        tab.setSinglePage(filterPage(loggedInUser, tab.getSinglePage()));
                                        recursiveEntityGroup(loggedInUser, tab.getEntityGroup());
                                        return tab;
                                    }
                                    return null;
                                })
                                .filter(Objects::nonNull)
                                .collect(toList()));
                    }
                    return menuItem;
                })
                .filter(Objects::nonNull)
                .collect(toList()));
        return menuGroup;
    }

    private void applyPermissions(LoggedInUserDO loggedInUser, List<MenuGroupDO> menuGroups) {
//        if (RoleHelper.isSuperUser(loggedInUser)) {
//            return;
//        }
        safeStream(menuGroups)
                .forEach(menuGroup -> safeStream(menuGroup.getMenuItems())
                        // .peek(menu -> logger.info("Menu: {} {} {} ", loggedInUser.getUsername(), loggedInUser.isSuperUser(), menu.getCode()))
                        .forEach(menu -> recursiveMenu(loggedInUser, menu)));
    }

    private void recursiveMenu(LoggedInUserDO loggedInUser, MenuDO menu) {
        if (nonNull(menu)) {
            recursiveEntityGroup(loggedInUser, menu.getEntityGroup());
            menu.setSingleForm(filterForm(loggedInUser, menu.getSingleForm()));
            menu.setSinglePage(filterPage(loggedInUser, menu.getSinglePage()));
        }

    }

    private void recursiveEntityGroup(LoggedInUserDO loggedInUser, EntityGroupConfigDO entityGroup) {
        if (nonNull(entityGroup)) {
            recursiveMenu(loggedInUser, entityGroup.getEntryForm());
            recursiveMDatasources(loggedInUser, entityGroup.getDataSources());
            entityGroup.setCreateConfig(filterCreateConfig(loggedInUser, entityGroup.getCreateConfig()));
            safeStream(entityGroup.getMenuItems())
                    .forEach(menu -> recursiveMenu(loggedInUser, menu));
        }

    }

    private void recursiveMDatasources(LoggedInUserDO loggedInUser, List<DataSource> dataSources) {
        safeStream(dataSources)
                .filter(datasource -> nonNull(datasource.getDefinition()))
                .forEach(datasource -> {
                    datasource.getDefinition().setRows(filterColumns(loggedInUser, datasource.getDefinition().getRows()));
                    datasource.getDefinition().setRestActions(filterActions(loggedInUser, datasource.getDefinition().getRestActions()));
                    datasource.getDefinition().setOverallRestActions(filterActions(loggedInUser, datasource.getDefinition().getOverallRestActions()));
                    datasource.getDefinition().setTabs(filterTabs(loggedInUser, datasource.getDefinition().getTabs()));
                });
    }

    private List<MaintainTableTab> filterTabs(LoggedInUserDO loggedInUser, List<MaintainTableTab> tabs) {
        if (nonNull(tabs)) {
            return safeStream(tabs)
                    .filter(tab -> passesPermission(loggedInUser, tab.getPermission()))
                    .map(tab -> {
                        if (nonNull(tab.getTableView()) && nonNull(tab.getTableView().getFilters())) {
                            tab.getTableView().setFilters(filterFields(loggedInUser, tab.getTableView().getFilters()));
                        }
                        if (nonNull(tab.getTableView()) && nonNull(tab.getTableView().getActions())) {
                            tab.getTableView().setActions(filterTableActions(loggedInUser, tab.getTableView().getActions()));
                        }
                        return tab;
                    })
                    .collect(toUnmodifiableList());
        }
        return null;
    }

    private List<TableActionsConfigDO> filterTableActions(LoggedInUserDO loggedInUser, List<TableActionsConfigDO> actions) {
        if (nonNull(actions)) {
            return safeStream(actions)
                    .filter(action -> passesPermission(loggedInUser, action.getPermission()))
                    .collect(toUnmodifiableList());
        }
        return null;
    }

    private List<FieldConfigDO> filterFields(LoggedInUserDO loggedInUser, List<FieldConfigDO> fields) {
        if (nonNull(fields)) {
            return safeStream(fields)
                    .filter(field -> passesPermission(loggedInUser, field.getPermission()))
                    .filter(field -> passesAllowedCohorts(loggedInUser, field))
                    .collect(toUnmodifiableList());
        }
        return null;
    }

    private List<ActionConfigDO> filterActions(LoggedInUserDO loggedInUser, List<ActionConfigDO> actions) {
        if (nonNull(actions)) {
            return safeStream(actions)
                    .filter(action -> passesPermission(loggedInUser, action.getPermission()))
                    .map(action -> {
                        action.setChildren(filterActions(loggedInUser, action.getChildren()));
                        action.setRows(filterColumns(loggedInUser, action.getRows()));
                        action.setConfirmationFormRows(filterColumns(loggedInUser, action.getConfirmationFormRows()));
                        return action;
                    })
                    .collect(toUnmodifiableList());
        }
        return null;
    }

    private boolean passesPermission(LoggedInUserDO loggedInUser, String permission) {
        return StringUtils.isBlank(permission) || hasPermissionOrIsSuperUser(loggedInUser, permission);
    }

    private boolean passesAllowedCohorts(LoggedInUserDO loggedInUser, FieldConfigDO field) {
        if (isNull(field.getMediaRestrictions()) || field.getMediaRestrictions().length < 1) {
            if (isNull(field.getAllowedCohorts()) || field.getAllowedCohorts().length == 0 || isNull(loggedInUser.getCurrentCohortId())) {
                return true;
            }

            final HashSet<String> allowedIds = new HashSet<>(Arrays.asList(field.getAllowedCohorts()));
            return allowedIds.contains(loggedInUser.getCurrentCohortId());

        }

        MutableBoolean found = new MutableBoolean(false);
        MutableBoolean haveNull = new MutableBoolean(false);

        safeStream(field.getMediaRestrictions()).forEach(restriction -> {
            if (isNull(restriction.getCohortId())) {
                haveNull.set(true);
            } else if (restriction.getCohortId().equals(loggedInUser.getCurrentCohortId())) {
                found.set(true);
            }
        });

        return found.get() || haveNull.get();
    }

    /*
        private boolean passesAllowedCohorts(LoggedInUserDO loggedInUser, String[] allowedCohorts) {
            if (isNull(allowedCohorts) || allowedCohorts.length == 0 || isNull(loggedInUser.getCurrentCohortId())) {
                return true;
            }

            final HashSet<String> allowedIds = new HashSet<>(Arrays.asList(allowedCohorts));
            return allowedIds.contains(loggedInUser.getCurrentCohortId());
        }

     */
    private MenuDO filterCreateConfig(LoggedInUserDO loggedInUser, MenuDO menu) {
        if (nonNull(menu) && StringUtils.isNotBlank(menu.getPermission())) {
            if (!hasPermissionOrIsSuperUser(loggedInUser, menu.getPermission())) {
                return null;
            }
        }

        return menu;
    }

    private SingleFormConfigDO filterForm(LoggedInUserDO loggedInUser, SingleFormConfigDO singleForm) {
        if (nonNull(singleForm) && StringUtils.isNotBlank(singleForm.getPermission())) {
            if (!hasPermissionOrIsSuperUser(loggedInUser, singleForm.getPermission())) {
                return null;
            }
        }
        if (nonNull(singleForm) && nonNull(singleForm.getRows())) {
            singleForm.setRows(filterColumns(loggedInUser, singleForm.getRows()));
        }
        return singleForm;
    }

    private SinglePageConfigDO filterPage(LoggedInUserDO loggedInUser, SinglePageConfigDO singlePage) {
        if (nonNull(singlePage) && StringUtils.isNotBlank(singlePage.getPermission())) {
            if (!hasPermissionOrIsSuperUser(loggedInUser, singlePage.getPermission())) {
                return null;
            }
        }
        if (nonNull(singlePage) && nonNull(singlePage.getRows())) {
            singlePage.setRows(filterColumns(loggedInUser, singlePage.getRows()));
        }
        return singlePage;
    }

    private List<FormRowConfigDO> filterColumns(LoggedInUserDO loggedInUser, List<FormRowConfigDO> rows) {
        return safeStream(rows)
                .map(row -> {
                    row.setColumns(safeStream(row.getColumns())
                            .map(column -> {
                                // filter out fields
                                column.setFields(safeStream(column.getFields())
                                        // .peek(field -> logger.info("Field: {} {} {} {} {} {}", loggedInUser.getUsername(), loggedInUser.isSuperUser(), field.getName(), field.getPermission(), StringUtils.isBlank(field.getPermission()), hasPermission(loggedInUser, field.getPermission())))
                                        // .peek(field -> logger.info("{} current: {} allowed: {} passes: {}", field.getName(), loggedInUser.getCurrentCohortId(), field.getAllowedCohorts(), passesAllowedCohorts(loggedInUser, field.getAllowedCohorts())))
                                        .filter(field -> passesAllowedCohorts(loggedInUser, field))
                                        .filter(field -> {

                                            field.setInnerFields(safeStream(field.getInnerFields())
                                                    // .peek(fld -> logger.info("inner fld: {} {} {} {} {} {}", loggedInUser.getUsername(), loggedInUser.isSuperUser(), fld.getName(), fld.getPermission(), StringUtils.isBlank(fld.getPermission()), hasPermission(loggedInUser, fld.getPermission())))
                                                    .filter(fld -> passesAllowedCohorts(loggedInUser, fld))
                                                    .filter(fld -> StringUtils.isBlank(fld.getPermission())
                                                            || hasPermissionOrIsSuperUser(loggedInUser, fld.getPermission()))
                                                    .collect(toList()));

                                            field.setActions(safeStream(field.getActions())
                                                    // .peek(action -> logger.info("Field Action: {} {} {} {} {} {}", loggedInUser.getUsername(), loggedInUser.isSuperUser(), action.getButtonLabel(), action.getPermission(), StringUtils.isBlank(action.getPermission()), hasPermission(loggedInUser, action.getPermission())))
                                                    .filter(action -> StringUtils.isBlank(action.getPermission())
                                                            || hasPermissionOrIsSuperUser(loggedInUser, action.getPermission()))
                                                    .collect(toUnmodifiableList()));

                                            return StringUtils.isBlank(field.getPermission())
                                                    || hasPermissionOrIsSuperUser(loggedInUser, field.getPermission());
                                        })
                                        .collect(toUnmodifiableList()));
                                // filter out actions
                                column.setActions(safeStream(column.getActions())
                                        //.peek(action -> logger.info("Action: {} {} {} {} {} {}", loggedInUser.getUsername(), loggedInUser.isSuperUser(), action.getButtonLabel(), action.getPermission(), StringUtils.isBlank(action.getPermission()), hasPermission(loggedInUser, action.getPermission())))
                                        .filter(action -> StringUtils.isBlank(action.getPermission())
                                                || hasPermissionOrIsSuperUser(loggedInUser, action.getPermission()))
                                        .collect(toUnmodifiableList()));
                                return column;
                            })
                            .collect(toUnmodifiableList()));
                    return row;
                }).collect(toUnmodifiableList());
    }

    private List<FormDO> getUiForms() throws GUSLErrorException {

        List<FormDO> forms = new ArrayList<>();
        Set<Class<?>> uiFormClasses = getAnnotatedClasses(UiForm.class);

        for (Class<?> clazz : uiFormClasses) {
            UiForm uiForm = clazz.getAnnotation(UiForm.class);
            FormRowConfigDO rowConfig = FormBuilder.getRowConfig(clazz);
            forms.add(new FormDO(clazz.getSimpleName(), uiForm.title(), uiForm.updateUrl(), rowConfig));
        }
        return forms;
    }

    public void getToolbarBuilders() throws GUSLErrorException {
        if (isNull(theToolbarCallbackInstanceCreator)) {
            throw new GUSLErrorException(FormErrors.NO_INSTANCE_CREATOR_DEFINED.getError());
        }
        if (isNull(theReflections)) {
            bootstrap();
        }
        final Set<Class<? extends IToolbarBuilder>> toolbarBuilders = theReflections.getSubTypesOf(IToolbarBuilder.class);

        theToolbarBuilders = safeStream(toolbarBuilders).map(builder -> {
            try {
                return theToolbarCallbackInstanceCreator.apply(builder);
            } catch (GUSLException e) {
                logger.error("Failed to initialise: {}", builder.getCanonicalName());
                return null;
            }

        }).filter(Objects::nonNull).collect(toList());

    }

    public void getToolbarActionBuilders() throws GUSLErrorException {

        if (isNull(theToolbarActionCallbackInstanceCreator)) {
            throw new GUSLErrorException(FormErrors.NO_INSTANCE_CREATOR_DEFINED.getError());
        }
        if (isNull(theReflections)) {
            bootstrap();
        }
        List<TickerToolbarItem> tickers = new ArrayList<>();
        List<ToolbarItem> items = new ArrayList<>();

        final Set<Class<? extends IToolbarActionBuilder>> actionBuilders = theReflections.getSubTypesOf(IToolbarActionBuilder.class);

        theToolbarActionBuilders = safeStream(actionBuilders).map(builder -> {
            try {
                return theToolbarActionCallbackInstanceCreator.apply(builder);
            } catch (GUSLException e) {
                logger.error("Failed to initialise: {}", builder.getCanonicalName());
                return null;
            }

        }).filter(Objects::nonNull).collect(toList());

    }

    public void getLookupBuilders() throws GUSLErrorException {
        final Set<Class<? extends ILookupBuilder>> lookupBuilders = theReflections.getSubTypesOf(ILookupBuilder.class);

        theLookupBuilders = safeStream(lookupBuilders).map(builder -> {
            try {
                return theLookupCallbackInstanceCreator.apply(builder);
            } catch (GUSLException e) {
                logger.error("Failed to initialise: {}", builder.getCanonicalName());
                return null;
            }

        }).filter(Objects::nonNull).collect(toList());

    }

    public void getMenuGroupBuilders() throws GUSLErrorException {
        if (isNull(theMenuCallbackInstanceCreator)) {
            throw new GUSLErrorException(FormErrors.NO_INSTANCE_CREATOR_DEFINED.getError());
        }
        if (isNull(theReflections)) {
            bootstrap();
        }
        final Set<Class<? extends IMenuGroupBuilder>> menuGroupBuilders = theReflections.getSubTypesOf(IMenuGroupBuilder.class);

        theMenuBuilders = safeStream(menuGroupBuilders).map(builder -> {
            try {
                return theMenuCallbackInstanceCreator.apply(builder);
            } catch (GUSLException e) {
                logger.error("Failed to initialise: {}", builder.getCanonicalName());
                return null;
            }

        }).filter(Objects::nonNull).collect(toList());

    }

    public synchronized Set<Class<?>> getAnnotatedClasses(Class<? extends Annotation> annotatedClass) throws GUSLErrorException {
        if (isNull(theReflections)) {
            bootstrap();
        }
        return theReflections.getTypesAnnotatedWith((Class<? extends Annotation>) annotatedClass);
    }

    private synchronized void bootstrap() throws GUSLErrorException {
        if (isNull(theReflections)) {
            if (isNull(thePackagePrefixes)) {
                throw new GUSLErrorException(FormErrors.NO_PACKAGE_PREFIX_DEFINED.getError());
            }

            ConfigurationBuilder builder = new ConfigurationBuilder();
            safeStream(thePackagePrefixes).forEach(pkg -> builder.addUrls(ClasspathHelper.forPackage(pkg)));
            builder.addScanners(new TypeAnnotationsScanner());
            try {
                theReflections = new Reflections(builder);
            } catch (IllegalStateException ex) {
                // Try again ...
                theReflections = new Reflections(builder);
            }
        }
    }

}
