package gusl.form.registra;

import gusl.core.exceptions.GUSLException;

public class CallbackHelper {

    @FunctionalInterface
    public interface CreateInstanceCallbackFunction<T extends Class, R> {

        R apply(T t) throws GUSLException;
    }

}
