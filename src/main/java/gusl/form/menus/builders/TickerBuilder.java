package gusl.form.menus.builders;

import com.google.common.base.Strings;
import gusl.annotations.form.TableRowDefinition;
import gusl.annotations.form.TickerCommandConfig;
import gusl.form.builder.FormBuilder;
import gusl.form.model.*;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static gusl.core.utils.StringUtils.isNotBlank;
import static gusl.form.menus.builders.EntityGroupMenuBuilder.createConditionals;
import static java.util.Objects.nonNull;
import static org.apache.logging.log4j.util.Strings.trimToNull;

public class TickerBuilder {

    private Field field;
    private Class<?> clazz;
    private List<Class<?>> commandsClasses = new LinkedList<>();
    private String tickerCode;

    public TickerBuilder(Class<?> clazz) {
        this.clazz = clazz;
    }

    public TickerBuilder(Field field) {
        this.field = field;
    }

    public TickerBuilder registerCommand(Class<?> eventClazz) {
        commandsClasses.add(eventClazz);
        return this;
    }

    public TickerConfigDO build() {

        Class<?> theClass = nonNull(clazz) ? clazz : field.getType();
        TableRowDefinition annotation = nonNull(clazz) ? clazz.getAnnotation(TableRowDefinition.class) : field.getAnnotation(TableRowDefinition.class);

        TickerConfigDO tickerConfigDO = new TickerConfigDO();
        FormRowConfigDO rowConfig = FormBuilder.getRowConfig(theClass);
        tickerConfigDO.setRows(Collections.singletonList(rowConfig));
        tickerConfigDO.setCode(tickerCode);

        if (nonNull(annotation.ticker()) && annotation.ticker().length > 0) {
            tickerConfigDO.setHideCohort(annotation.ticker()[0].hideCohort());
            tickerConfigDO.setHideFilter(annotation.ticker()[0].hideFilter());
            tickerConfigDO.setHideLock(annotation.ticker()[0].hideLock());
            tickerConfigDO.setShowBorder(annotation.ticker()[0].showBorder());
            tickerConfigDO.setShowAdd(annotation.ticker()[0].showAdd());
            tickerConfigDO.setHideHeader(annotation.ticker()[0].hideHeader());
            tickerConfigDO.setHidePagination(annotation.ticker()[0].hidePagination());
            tickerConfigDO.setHideRefresh(annotation.ticker()[0].hideRefresh());
            tickerConfigDO.setShowPopout(annotation.ticker()[0].showPopout());
            tickerConfigDO.setPopoutWidth(annotation.ticker()[0].popoutWidth());
            tickerConfigDO.setPopoutHeight(annotation.ticker()[0].popoutHeight());
        }

        List<MaintainTableTab> tabs = Arrays.stream(annotation.tabs()).map(t -> {
            TableViewConfigDO tableView = GuslEntityGroupMenuBuilder.createTableView(t, annotation, theClass, rowConfig, field);
            tableView.setExpandable(t.ticker().expandable());
            tableView.setActionable(t.ticker().actionable());
            List<RowConditionDTO> conditions = Arrays.stream(t.ticker().conditions())
                    .map(queryCondition -> new RowConditionDTO(queryCondition.field(), queryCondition.value()))
                    .collect(Collectors.toList());
            tableView.setConditionals(createConditionals(field));
            return new MaintainTableTab(tableView, conditions);
        }).collect(Collectors.toList());
        tickerConfigDO.setTabs(tabs);
        tickerConfigDO.setTabsWithCount(true);

        commandsClasses.addAll(Arrays.asList(annotation.tickerCommandEvents()));

        List<TickerCommandDTO> commands = commandsClasses
                .stream()
                .map(this::createCommand)
                .collect(Collectors.toList());
        tickerConfigDO.setCommands(commands);
        return tickerConfigDO;
    }

    private TickerCommandDTO createCommand(Class<?> clazz) {
        TickerCommandConfig annotation = clazz.getAnnotation(TickerCommandConfig.class);
        TickerCommandDTO tickerCommandDTO = new TickerCommandDTO();
        tickerCommandDTO.setButtonLabel(annotation.base().buttonLabel());
        tickerCommandDTO.setCommand(annotation.command());
        tickerCommandDTO.setJsEditCondition(isNotBlank(annotation.base().jsEditCondition()) ? annotation.base().jsEditCondition() : null);
        tickerCommandDTO.setPopUpTitle(annotation.base().popUpTitle());
        tickerCommandDTO.setPopUpDescription(annotation.base().popUpDescription());
        tickerCommandDTO.setSuccessMessage(annotation.base().successMessage());
        tickerCommandDTO.setRows(FormBuilder.getRowsFormActionConfig(annotation.base()));
        tickerCommandDTO.setMinModalWidth(isNotBlank(annotation.base().minModalWidth()) ? annotation.base().minModalWidth() : null);
        tickerCommandDTO.setConfirmationRequired(annotation.requireConfirmation());
        tickerCommandDTO.setIcon(Strings.emptyToNull(annotation.fontAwesomeIcon()));
        tickerCommandDTO.setQuickAccess(annotation.quickAccess());
        tickerCommandDTO.setQuickAccess(annotation.quickAccess());
        tickerCommandDTO.setActionsBar(annotation.actionsBar());
        tickerCommandDTO.setTemplateUrl(annotation.base().templateUrl());

        tickerCommandDTO.setConfirmationMessage(trimToNull(annotation.base().confirmationMessage()));
        tickerCommandDTO.setConfirmationFields(StringUtils.trimToNull(annotation.base().confirmationFields()));

        return tickerCommandDTO;
    }

    public TickerBuilder tickerCode(String code) {
        this.tickerCode = code;
        return this;
    }
}
