package gusl.form.menus.builders;

import gusl.annotations.form.CohortMediaType;
import gusl.annotations.form.MediaType;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Utils;
import gusl.form.model.*;
import gusl.model.cohort.CohortIdsDO;
import org.glassfish.jersey.server.model.AnnotatedMethod;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Path;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static gusl.form.builder.Builder.menuItem;
import static gusl.form.builder.FormBuildHelper.getRowsForSimpleFieldsDefClass;
import static java.util.Objects.nonNull;

public abstract class AbstractMenuGroupBuilder {

    public final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    public abstract MenuGroupDO build(LoggedInUserDO loggedInUser);

    protected MenuDO addMenu(LoggedInUserDO loggedInUser, Class menuItemBOClass, String... permission) {
        return addMenu(loggedInUser, menuItemBOClass, null, MediaType.Laptop, permission);
    }

    protected MenuDO addMenu(LoggedInUserDO loggedInUser, Class menuItemBOClass, MediaType mediaType, String... permission) {
        return addMenu(loggedInUser, menuItemBOClass, null, mediaType, permission);
    }

    protected MenuDO addMenu(LoggedInUserDO loggedInUser, Class menuItemBOClass, CohortIdsDO allowedCohorts, String... permission) {
        return addMenu(loggedInUser, menuItemBOClass, allowedCohorts, MediaType.Laptop, permission);
    }

    protected MenuDO addMenu(LoggedInUserDO loggedInUser, Class menuItemBOClass, CohortMediaType[] cohortMediaTypeRestrictions, String... permission) {
        Map<String, CohortMediaType> mapRestrictionsByCohort = Utils.safeStream(cohortMediaTypeRestrictions)
                .collect(Collectors.toMap(CohortMediaType::getCohortId, Function.identity()));

        if (nonNull(loggedInUser.getCurrentCohortId()) && !mapRestrictionsByCohort.isEmpty()) {
            if (mapRestrictionsByCohort.containsKey(loggedInUser.getCurrentCohortId())) {
                // cohort match - so restrict by media type (media restricted client side)
                return addMenu(loggedInUser, menuItemBOClass, null, mapRestrictionsByCohort.get(loggedInUser.getCurrentCohortId()).getMediaType(), permission);
            }
        }
        // no restriction
        return addMenu(loggedInUser, menuItemBOClass, null, MediaType.Laptop, permission);
    }

    protected MenuDO addMenu(LoggedInUserDO loggedInUser, Class menuItemBOClass, CohortIdsDO allowedCohorts, MediaType mediaType, String... permission) {
        if (nonNull(loggedInUser.getCurrentCohortId())
                && nonNull(allowedCohorts)
                && nonNull(allowedCohorts.getIds())
                && !allowedCohorts.getIds().isEmpty()) {
            if (!allowedCohorts.getIds().contains(loggedInUser.getCurrentCohortId())) {
                return null;
            }
        }

        if (canAccess(loggedInUser, permission)) {
            return new GuslEntityGroupMenuBuilder(loggedInUser, menuItemBOClass)
                    .mediaType(mediaType)
                    .showInSideBar(true)
                    .build();
        }

        return null;
    }

    protected boolean canAccess(LoggedInUserDO loggedInUser, String... permission) {
        if (loggedInUser.isSuperUser()) {
            return true;
        }
        return !Collections.disjoint(loggedInUser.getPermissions(), new HashSet<>(Arrays.asList(permission)));
    }

    protected MenuDO hideInMenu(MenuDO menu) {
        menu.setShowInSideBar(false);
        return menu;
    }

    protected MenuDO buildLink(String name, String route, String permission) {
        return menuItem()
                .label(name)
                .code(convertToCode(name))
                .permission(permission)
                //.icon()//
                .route(route) //<---- add route option to menu
                .build();
    }

    protected MenuDO buildSinglePage(LoggedInUserDO loggedInUser, String name, Class uiClass, String endpoint, Class<?> resourceClass) {
        if (!canAccess(endpoint, resourceClass, loggedInUser)) {
            return null;
        }
        return menuItem()
                .label(name)
                .code(convertToCode(name))
                .singlePage(SinglePageConfigDO.builder()
                        .title(name)
                        .rows(getRowsForSimpleFieldsDefClass(uiClass))
                        .selectUrl(EndPointDO.builder()
                                .method(EndPointType.GET)
                                .url(endpoint)
                                .build())
                        .build())
                .build();
    }

    private String convertToCode(String name) {
        return name.toLowerCase().replace(" ", "_");
    }

    private boolean canAccess(String path, Class<?> resource, LoggedInUserDO loggedInUser) {

        if (loggedInUser.getRoleIds() == null || loggedInUser.getRoleIds().getIds() == null || loggedInUser.getRoleIds().getIds().isEmpty()) {
            logger.warn("Unable to determine roles for {}", loggedInUser);
            return false;
        }

        if (loggedInUser.getRoleIds().getIds().contains("SUPER_USER")) {
            return true;
        }

        final Set<String> permissions = loggedInUser.getPermissions();

        Path base = resource.getDeclaredAnnotation(Path.class);
        String baseUrl = "";
        if (base != null) {
            baseUrl = base.value();
        }

        for (Method method : resource.getDeclaredMethods()) {
            for (Annotation annotation : method.getAnnotations()) {
                if (annotation.annotationType() == Path.class) {
                    String url = baseUrl + ((Path) annotation).value();
                    if (url.equals(path)) {
//                        logger.info("I got here with {}:{}", url, method);
                        final AnnotatedMethod am = new AnnotatedMethod(method);
                        if (am.isAnnotationPresent(DenyAll.class)) {
                            return false;
                        }
                        if (am.isAnnotationPresent(PermitAll.class)) {
                            return true;
                        }

                        if (!am.isAnnotationPresent(RolesAllowed.class)) {
                            logger.warn("No Permissions set for {}:{}, access denied", path, method.getName());
                            return false;
                        }

                        for (String permission : am.getAnnotation(RolesAllowed.class).value()) {
                            logger.debug("role {} in {} hasPermission: {}", permission, permissions, permissions.contains(permission));
                            if (permissions.contains(permission)) {
                                return true;
                            }
                        }

                        logger.info("User {}, doesn't have permission for {}", loggedInUser.getUsername(), path);
                        return false;
                    }
                }
            }
        }

        logger.warn("Can't find method for {}", path);
        return false;
    }
}
