package gusl.form.menus.builders;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.form.model.MenuDO;
import gusl.form.builder.builders.EntityMaintain;

import static gusl.form.builder.Builder.menuItem;

public abstract class AbstractMenuBuilder {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    protected final LoggedInUserDO loggedInUser;

    public AbstractMenuBuilder(LoggedInUserDO loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public abstract MenuDO build();

    public MenuDO build(String label, Class klass) {
        EntityMaintain.Builder builder = new EntityMaintain.Builder(klass);
        return menuItem().label(label).maintain(builder.build(loggedInUser)).build();
    }

}
