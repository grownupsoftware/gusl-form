package gusl.form.menus.builders;

import com.google.common.base.Strings;
import gusl.annotations.form.*;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.form.builder.FormBuilder;
import gusl.form.builder.builders.CassandraQuery;
import gusl.form.builder.builders.DataTableQuery;
import gusl.form.builder.builders.ElasticQuery;
import gusl.form.builder.builders.EntityGroup;
import gusl.form.model.*;
import lombok.CustomLog;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static gusl.core.utils.Utils.safeStream;
import static gusl.form.builder.Builder.maintain;
import static gusl.form.builder.FormBuildHelper.getColsForSimpleFieldsDefClass;
import static gusl.form.builder.FormBuildHelper.getRowsForCardDefClass;
import static gusl.form.builder.FormBuilder.createOrderBy;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@CustomLog
public class GuslEntityGroupMenuBuilder extends EntityGroupMenuBuilder {

    public GuslEntityGroupMenuBuilder(LoggedInUserDO loggedInUser, Class<?> clazz) {
        super(loggedInUser, clazz, MediaType.Laptop);
    }

    public GuslEntityGroupMenuBuilder(LoggedInUserDO loggedInUser, Class<?> clazz, MediaType mediaType) {
        super(loggedInUser, clazz, mediaType);
    }

    /**
     * Override in order to add tickers (specific to admin)
     *
     * @param clazz
     * @param entityGroupConfigBuilder
     */
    @Override
    protected void createRowDefinition(Class<?> clazz, EntityGroup.Builder entityGroupConfigBuilder) {
        Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(TableRowDefinition.class))
                .forEach(f -> {
                    TableRowDefinition annotation = f.getAnnotation(TableRowDefinition.class);

                    switch (annotation.type()) {
                        case TableRowDefinition.ENTITY_GROUP:
                            entityGroupConfigBuilder.clearSubMenus();
                            entityGroupConfigBuilder.subMenus(convertToMenuItems(annotation));
                            break;
                        case TableRowDefinition.MAINTAIN_TYPE:
                            List<FormRowConfigDO> rows = getRowsForCardDefClass(f.getType());

                            List<MaintainTableTab> tabs = Arrays.stream(annotation.tabs())
                                    .map(t -> new MaintainTableTab(
                                            createEndpoints(t),
                                            createTableView(t, annotation, f.getType(), rows.get(0), f))
                                    ).collect(Collectors.toList());

                            MaintainConfigDO maintain = maintain()
                                    .tabs(tabs)
                                    .rows(rows)
                                    .build();

                            RestActionConfig[] restActionConfigs = Stream.of(annotation.tabs())
                                    .flatMap(t -> Stream.of(t.actions()))
                                    .toArray(RestActionConfig[]::new);

                            maintain.setRestActions(createActionsDTO(restActionConfigs));
                            maintain.setTitle(annotation.title());
                            maintain.setId(Strings.emptyToNull(annotation.id()));
                            maintain.setOverallRestActions(createActionsDTO(annotation.overallRestActions()));
                            entityGroupConfigBuilder.selectConfigMaintain(maintain);
                            break;
                        case TableRowDefinition.ELASTIC_TYPE:
                            UiTabDefinition tab = annotation.tabs()[0];

                            ElasticConfigDO elastic = new ElasticQuery.Builder(tab.elastic().esoClass())
                                    .index(tab.elastic().indexName())
                                    .aggsUrl(Strings.nullToEmpty(tab.elastic().aggsUrl()))
                                    .convertTo(tab.elastic().convertEsoTo())
                                    .title(annotation.title())
                                    .expandable(false)
                                    .withTimestamp()
                                    .sortTieBreaker(tab.sortTieBreaker())
                                    .defaultSort(sortStringToObj(tab.defaultSortString()))
                                    .restActions(createActionsDTO(tab.actions()))
                                    .initialConditions(FormBuilder.constructInitialConditionsForElasticTab(Arrays.asList(tab.elastic().conditions())))
                                    .elasticFilterGroup(Strings.emptyToNull(tab.elastic().elasticQueryGroup()))
                                    .refreshRate(annotation.refreshRate())
                                    .tableControl(createTableControl(tab.tableControls()))
                                    .groupActions(createActionsDTO(tab.groupActions()))
                                    .orderByActions(createOrderBy(tab.orderByActions()))
                                    .build();

                            elastic.setTitle(annotation.title());
                            elastic.setNoLabel(tab.noLabel());
                            elastic.setId(Strings.emptyToNull(annotation.id()));
                            elastic.setOverallRestActions(createActionsDTO(annotation.overallRestActions()));
                            entityGroupConfigBuilder.selectConfigElastic(elastic);
                            break;
                        case TableRowDefinition.DATA_TABLE:
                        case TableRowDefinition.ALPHA_LIST:
                        case TableRowDefinition.ACCORDION_LIST:
                        case TableRowDefinition.FLEX_LIST:
                            UiTabDefinition dataTableTab = annotation.tabs()[0];

                            // start
                            List<ConditionalRowDetailsDTO> dtConditionals = new ArrayList<>();
                            ComplexTableRowsDefinition dtRowsDefAnnotation = f.getAnnotation(ComplexTableRowsDefinition.class);
                            if (nonNull(dtRowsDefAnnotation)) {

                                if (dtRowsDefAnnotation.value().length > 0) {
                                    ConditionalRowDetailsDTO conditional = new ConditionalRowDetailsDTO();
                                    conditional.setExpandable(true);
                                    conditional.setDetailsConfig(new FormRowConfigDO(getColsForSimpleFieldsDefClass(dtRowsDefAnnotation.value()[0].configClass())));
                                    conditional.setTable(dtRowsDefAnnotation.value()[0].table());
                                    conditional.setUrl(dtRowsDefAnnotation.value()[0].getDetailsUrl());
                                    dtConditionals.add(conditional);
                                }
                            }

                            DataTableConfigDO dataTable = new DataTableQuery.Builder(dataTableTab.dataTable().dtoClass())
                                    .selectUrl(dataTableTab.dataTable().selectUrl())
                                    .blastDeltaCommand(dataTableTab.dataTable().blastDeltaCommand())
                                    .title(annotation.title())
                                    .expandable(annotation.expandable())
                                    .defaultSort(sortStringToObj(dataTableTab.defaultSortString()))
                                    .restActions(createActionsDTO(dataTableTab.actions()))
                                    .initialConditions(FormBuilder.constructInitialConditionsForElasticTab(Arrays.asList(dataTableTab.cassandra().conditions())))
                                    .conditionals(dtConditionals)
                                    .refreshRate(dataTableTab.refreshRate())
                                    .tableControl(createTableControl(dataTableTab.tableControls()))
                                    .groupActions(createActionsDTO(dataTableTab.groupActions()))
                                    .orderByActions(createOrderBy(dataTableTab.orderByActions()))
                                    .alphaList(annotation.type().equals(TableRowDefinition.ALPHA_LIST))
                                    .flexList(annotation.type().equals(TableRowDefinition.FLEX_LIST))
                                    .accordionList(annotation.type().equals(TableRowDefinition.ACCORDION_LIST))
                                    .build();

                            dataTable.setTitle(annotation.title());
                            dataTable.setNoLabel(dataTableTab.noLabel());
                            dataTable.setId(Strings.emptyToNull(annotation.id()));
                            dataTable.setOverallRestActions(createActionsDTO(annotation.overallRestActions()));

                            entityGroupConfigBuilder.selectConfigDataTable(dataTable);
                            entityGroupConfigBuilder.headerUrl(annotation.headerUrl());

                            break;
                        case TableRowDefinition.CASSANDRA_TYPE:
                            UiTabDefinition cassTab = annotation.tabs()[0];

                            // start
                            List<ConditionalRowDetailsDTO> conditionals = new ArrayList<>();
                            ComplexTableRowsDefinition rowsDefAnnotation = f.getAnnotation(ComplexTableRowsDefinition.class);
                            if (nonNull(rowsDefAnnotation)) {

                                if (rowsDefAnnotation.value().length > 0) {
                                    ConditionalRowDetailsDTO conditional = new ConditionalRowDetailsDTO();
                                    conditional.setExpandable(true);
                                    conditional.setDetailsConfig(new FormRowConfigDO(getColsForSimpleFieldsDefClass(rowsDefAnnotation.value()[0].configClass())));
                                    conditional.setTable(rowsDefAnnotation.value()[0].table());
                                    conditional.setUrl(rowsDefAnnotation.value()[0].getDetailsUrl());
                                    conditionals.add(conditional);
                                }
                            }

                            CassandraConfigDO cassandra = new CassandraQuery.Builder(cassTab.cassandra().dtoClass())
                                    .selectUrl(cassTab.cassandra().selectUrl())
                                    .blastDeltaCommand(cassTab.cassandra().blastDeltaCommand())
                                    .highlightDelta(cassTab.cassandra().highlightDelta())
                                    .disableTableControls(cassTab.cassandra().disableTableControls())
                                    .disableColumnResize(cassTab.cassandra().disableColumnResize())
                                    .aggsUrl(Strings.nullToEmpty(cassTab.elastic().aggsUrl()))
                                    .convertTo(cassTab.cassandra().convertTo())
                                    .title(annotation.title())
                                    .expandable(annotation.expandable())
                                    .sortTieBreaker(cassTab.sortTieBreaker())
                                    .defaultSort(sortStringToObj(cassTab.defaultSortString()))
                                    .restActions(createActionsDTO(cassTab.actions()))
                                    .initialConditions(FormBuilder.constructInitialConditionsForElasticTab(Arrays.asList(cassTab.cassandra().conditions())))
                                    .elasticFilterGroup(null)
                                    .conditionals(conditionals)
                                    .refreshRate(cassTab.cassandra().refreshRate())
                                    .tableControl(createTableControl(cassTab.tableControls()))
                                    .groupActions(createActionsDTO(cassTab.groupActions()))
                                    .orderByActions(createOrderBy(cassTab.orderByActions()))
                                    .build();

                            cassandra.setTitle(annotation.title());
                            cassandra.setNoLabel(cassTab.noLabel());
                            cassandra.setId(Strings.emptyToNull(annotation.id()));
                            cassandra.setOverallRestActions(createActionsDTO(annotation.overallRestActions()));

                            entityGroupConfigBuilder.selectConfigCassandra(cassandra);
                            entityGroupConfigBuilder.headerUrl(annotation.headerUrl());

                            break;
                        case TableRowDefinition.TICKER:
                            TickerConfigDO tickerConfig = new TickerBuilder(f).tickerCode(annotation.tickerCode()).build();
                            tickerConfig.setTitle(annotation.title());
                            tickerConfig.setId(Strings.emptyToNull(annotation.id()));
                            entityGroupConfigBuilder.selectConfigTicker(tickerConfig);
                            break;
                        case TableRowDefinition.BLAST_COLLECTION:
                            UiTabDefinition blastTab = annotation.tabs()[0];

                            BlastCollectionConfigDO blast = BlastCollectionConfigDO.builder()
                                    .collectionName(blastTab.blastTable().collectionName())
                                    .rows(getRowsForCardDefClass(blastTab.blastTable().rowClass()))
                                    .expandable(blastTab.blastTable().expandable())
                                    .title(annotation.title())
                                    .rowActions(createActionsDTO(blastTab.actions()))
                                    .groupActions(createActionsDTO(blastTab.groupActions()))
                                    .tableActions(createActionsDTO(annotation.overallRestActions()))
                                    .expandedRows(isNull(blastTab.blastTable().expandedClass()) ? null : getRowsForCardDefClass(blastTab.blastTable().expandedClass()))
                                    .getUrl(blastTab.blastTable().getUrl())
                                    .tableControl(createTableControl(blastTab.tableControls()))
                                    .orderByActions(createOrderBy(blastTab.orderByActions()))
                                    .build();

                            entityGroupConfigBuilder.selectConfigBlastCollectionTable(blast);
                            break;

                    }

                });
    }

    private Class<?> extractDtoClass(UiTabDefinition t) {
        if (nonNull(t.cassandra())) {
            return t.cassandra().dtoClass();
        }
        if (nonNull(t.blastTable())) {
            return t.blastTable().rowClass();
        }
        if (nonNull(t.blastForm())) {
            return t.blastForm().formClass();
        }
        if (nonNull(t.dataTable())) {
            return t.dataTable().dtoClass();
        }
        if (nonNull(t.elastic())) {
            return t.elastic().esoClass();
        }
        return null;
    }

    private String extractSelectUrl(UiTabDefinition t) {
        if (nonNull(t.cassandra())) {
            return t.cassandra().selectUrl();
        }
        if (nonNull(t.dataTable())) {
            return t.dataTable().selectUrl();
        }
        return null;
    }

    private List<MenuDO> convertToMenuItems(TableRowDefinition annotation) {
        return safeStream(annotation.tabs()).map(tab -> {
                    final Class<?> dtoClass = extractDtoClass(tab);
                    if (isNull(dtoClass)) {
                        return null;
                    }
                    final String selectUrl = extractSelectUrl(tab);
                    if (isNull(selectUrl)) {
                        return null;
                    }
                    DataTableConfigDO dataTable = new DataTableQuery.Builder(dtoClass)
                            .selectUrl(selectUrl)
                            .blastDeltaCommand(tab.dataTable().blastDeltaCommand())
                            .title(annotation.title())
                            .expandable(annotation.expandable())
                            .defaultSort(sortStringToObj(tab.defaultSortString()))
                            .restActions(createActionsDTO(tab.actions()))
                            .initialConditions(FormBuilder.constructInitialConditionsForElasticTab(Arrays.asList(tab.cassandra().conditions())))
                            .refreshRate(tab.refreshRate())
                            .tableControl(createTableControl(tab.tableControls()))
                            .groupActions(createActionsDTO(tab.groupActions()))
                            .orderByActions(createOrderBy(tab.orderByActions()))
                            .alphaList(annotation.type().equals(TableRowDefinition.ALPHA_LIST))
                            .flexList(annotation.type().equals(TableRowDefinition.FLEX_LIST))
                            .accordionList(annotation.type().equals(TableRowDefinition.ACCORDION_LIST))
                            .build();

                    dataTable.setTitle(annotation.title());
                    dataTable.setNoLabel(tab.noLabel());
                    dataTable.setId(Strings.emptyToNull(annotation.id()));
                    dataTable.setOverallRestActions(createActionsDTO(annotation.overallRestActions()));

                    DataSource<?> dataSource = new DataSource<>(MenuType.MAINTAIN, dataTable);
                    return MenuDO.builder()
                            .code(tab.code())
                            .label(tab.label())
                            .entityGroup(EntityGroupConfigDO.builder()
                                    .dataSources(Collections.singletonList(dataSource))
                                    .build())
                            .menuType(MenuType.ENTITY_GROUP)
                            .permission(tab.permission())
                            .build();

                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

}
