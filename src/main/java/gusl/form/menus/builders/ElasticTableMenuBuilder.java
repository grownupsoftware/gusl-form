package gusl.form.menus.builders;

import gusl.annotations.form.UiMenuItem;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.form.model.MenuDO;

import static gusl.form.builder.Builder.menuItem;

public class ElasticTableMenuBuilder extends AbstractMenuBuilder {

    private final Class<?> clazz;

    public ElasticTableMenuBuilder(
            LoggedInUserDO loggedInUser,
            Class<?> clazz) {
        super(loggedInUser);
        this.clazz = clazz;
    }

    @Override
    public MenuDO build() {

        UiMenuItem uiMenuItem = clazz.getAnnotation(UiMenuItem.class);
        return menuItem()
                .label(uiMenuItem.label())
                .help(uiMenuItem.help())
                .code(uiMenuItem.code())
                .permission(uiMenuItem.permission())
//                .grouped(clazz.isAnnotationPresent(Grouped.class))
//                .entityGroup(
//                        new EntityGroup.Builder(clazz)
//                                .selectUrl(uiMenuItem.selectOne())
//                                .subMenus(buildSubMenuForms(clazz))
//                                .selectConfig(createRowDefinition(clazz))
//                                .createConfig(createCreateConfig(clazz))
//                                .build()
//                )
                .build();
    }
}
