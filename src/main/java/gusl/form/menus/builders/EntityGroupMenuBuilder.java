package gusl.form.menus.builders;

import gusl.annotations.form.*;
import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.LambdaExceptionHelper;
import gusl.form.builder.FormBuilder;
import gusl.form.builder.MaintainBuilder;
import gusl.form.builder.builders.*;
import gusl.form.model.*;
import gusl.model.errors.SystemErrors;
import lombok.CustomLog;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.base.Strings.emptyToNull;
import static gusl.core.utils.Utils.safeStream;
import static gusl.form.builder.Builder.menuItem;
import static gusl.form.builder.Builder.tableView;
import static gusl.form.builder.FormBuildHelper.*;
import static gusl.form.builder.FormBuilder.createOrderBy;
import static gusl.form.builder.builders.EntityGroup.Builder.subMenuItem;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@CustomLog
public abstract class EntityGroupMenuBuilder extends AbstractMenuBuilder {

    private final Class<?> clazz;
    private boolean singleton = false;
    private boolean showInSideBar = false;

    private MediaType mediaType;

    public EntityGroupMenuBuilder(
            LoggedInUserDO loggedInUser,
            Class<?> clazz) {
        super(loggedInUser);
        this.clazz = clazz;
        this.mediaType = MediaType.Laptop;
    }

    public EntityGroupMenuBuilder(
            LoggedInUserDO loggedInUser,
            Class<?> clazz,
            MediaType mediaType
    ) {
        super(loggedInUser);
        this.clazz = clazz;
        this.mediaType = mediaType;
    }

    abstract protected void createRowDefinition(Class<?> clazz, EntityGroup.Builder entityGroupConfigBuilder);

    @Override
    public MenuDO build() {

        UiMenuItem uiMenuItem = clazz.getAnnotation(UiMenuItem.class);

        if (isNull(uiMenuItem)) {
            return null;
        }
        EntityGroup.Builder entityGroupConfigBuilder = new EntityGroup.Builder(clazz)
                .selectUrl(uiMenuItem.selectOne())
                .subMenus(buildSubMenuForms(clazz))
                .createConfig(createCreateConfig(clazz))
                .entryFormConfig(createQueryConfig(clazz))
                .header(createHeader(clazz))
                .title(uiMenuItem.formTitle())
                .titleTag(emptyToNull(uiMenuItem.idTitleTagJs()))
                .singleton(singleton);

        createRowDefinition(clazz, entityGroupConfigBuilder);
        return menuItem()
                .label(uiMenuItem.label())
                .code(uiMenuItem.code())
                .help(uiMenuItem.help())
                .permission(uiMenuItem.permission())
                .entityGroup(entityGroupConfigBuilder.build())
                .showInSideBar(showInSideBar)
                .externalUrl(uiMenuItem.externalUrl())
                .footer(uiMenuItem.footer())
                .mediaType(mediaType)
                .build();
    }

    protected EntityGroupHeaderDO createHeader(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(UiEntityGroupHeader.class))
                .findFirst()
                .map(f -> {
                    UiEntityGroupHeader annotation = f.getAnnotation(UiEntityGroupHeader.class);

                    Map<String, FieldConfigDO> fieldConfigsForName = FormBuilder.getRowConfig(f.getType()).getColumns()
                            .stream()
                            .flatMap(c -> c.getFields().stream())
                            .collect(Collectors.toMap(FieldConfigDO::getName, Function.identity()));

                    Map<Integer, List<FieldConfigDO>> fieldsByColumn = Arrays.stream(f.getType().getDeclaredFields())
                            .filter(nestedField -> nestedField.isAnnotationPresent(UiField.class))
                            .collect(Collectors.groupingBy(
                                    nestedField -> Optional.ofNullable(nestedField.getAnnotation(UiHeaderField.class)).map(UiHeaderField::column).orElse(0),
                                    Collectors.mapping(nestedField -> fieldConfigsForName.get(nestedField.getName()), toList())
                            ));

                    List<FormColumnConfigDO> columns = fieldsByColumn
                            .entrySet()
                            .stream()
                            .sorted(Map.Entry.comparingByKey())
                            .map(entry -> new FormColumnConfigDO(entry.getValue()))
                            .collect(toList());

                    List<ActionConfigDO> actionsDTOs = createActionsDTO(annotation.actions());

                    EntityGroupHeaderDO entityGroupHeaderDO = new EntityGroupHeaderDO();

                    entityGroupHeaderDO.setBackgroundColor(annotation.jsBackground());
                    entityGroupHeaderDO.setFontColor(annotation.jsFontColor());
                    entityGroupHeaderDO.setTitle(annotation.jsTitle());
                    entityGroupHeaderDO.setImage(annotation.jsImage());

                    entityGroupHeaderDO.setRows(Collections.singletonList(new FormRowConfigDO(columns)));
                    entityGroupHeaderDO.setRefreshRateInSeconds(annotation.refreshRate());
                    entityGroupHeaderDO.setUrl(annotation.url());
                    entityGroupHeaderDO.setActions(actionsDTOs);

                    entityGroupHeaderDO.setWithRefreshButton(annotation.withRefreshButton());

                    if (nonNull(annotation.panels()) && annotation.panels().length > 0) {
                        entityGroupHeaderDO.setPanels(safeStream(annotation.panels()).map(panel -> PanelConfigDO.builder()
                                .panelCss(emptyToNull(panel.panelCss()))
                                .noTitle(panel.noTitle())
                                .title(emptyToNull(panel.title()))
                                .titleCss(emptyToNull(panel.titleCss()))
                                .selectUrl(emptyToNull(panel.selectUrl()))
                                .blastCollection(emptyToNull(panel.blastCollection()))
                                .blastTopic(emptyToNull(panel.blastTopic()))
                                .fxFlex(panel.fxFlex())
                                .jsEditCondition(emptyToNull(panel.jsEditCondition()))
                                .rows(safeStream(panel.dtoClass()).map(dtoClass -> new FormRowConfigDO(getColsForSimpleFieldsDefClass(dtoClass)))
                                        .collect(toList()))
                                .actions(createActionsDTO(panel.actions()))
                                .refreshRate(panel.refreshRate())
                                .build()).collect(toList()));
                    }

                    return entityGroupHeaderDO;
                })
                .orElse(null);
    }

    public List<ActionConfigDO> createActionsDTO(RestActionConfig[] actions) {
        return Arrays.stream(actions).map(FormBuilder::createAdminActionConfigFromAnnotation).collect(toList());
    }

    protected MenuDO createCreateConfig(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(UiCreateEntity.class))
                .findFirst()
                .map(f -> {
                    UiCreateEntity annotation = f.getAnnotation(UiCreateEntity.class);

                    return subMenuItem()
                            .label(annotation.title())
                            .code("add")
                            .permission(annotation.permission())
                            .entityGroupSingleForm(createCreateConfigSingleForm(f, annotation))
                            .getUrl(emptyToNull(annotation.templateUrl()))
                            .build();
                }).orElse(null);
    }

    protected MenuDO createQueryConfig(Class<?> clazz) {
//        if (clazz.getName().toLowerCase().contains("report")) {
//            System.out.println("dupa");
//        }

        return Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(UiSingleFormQuery.class))
                .findFirst()
                .map(f -> {
                    UiSingleFormQuery annotation = f.getAnnotation(UiSingleFormQuery.class);

                    return subMenuItem()
                            .label("Entry form")
                            .code("entry-form")
                            .entityGroupSingleForm(createQueryForm(f, annotation))
                            .getUrl(emptyToNull(annotation.submitUrl()))
                            .build();
                }).orElse(null);
    }

    protected SingleFormConfigDO createCreateConfigSingleForm(Field f, UiCreateEntity annotation) {
        return new EntityGroupSingleForm.Builder()
                .title(annotation.title())
                .updateUrl(createEndpointDTO(annotation.submitUrl(), EndPointType.POST))
                .rows(getRowsForCardDefClass(f.getType()))
                .multipartForm(annotation.multipartForm())
                .build();
    }

    protected SingleFormConfigDO createQueryForm(Field f, UiSingleFormQuery annotation) {
        return new EntityGroupSingleForm.Builder()
                .title("Entry form")
                .updateUrl(createEndpointDTO(annotation.submitUrl(), EndPointType.POST))
                .rows(getRowsForCardDefClass(f.getType()))
                .multipartForm(false)
                .build();
    }

    protected String createUrl(MaintainTabDefinition maintainTabDefinition) {
        if (maintainTabDefinition.params().length == 0) {
            return maintainTabDefinition.path();
        }

        String reduce = Arrays.stream(maintainTabDefinition.params())
                .flatMap(p -> Arrays.stream(p.value()).map(v -> p.key() + '=' + v))
                .reduce(maintainTabDefinition.path() + "?", (a, b) -> a + b + '&');

        return reduce.substring(0, reduce.lastIndexOf("&"));
    }

    protected EndpointsConfigDO createEndpoints(UiTabDefinition tab) {
        EndpointsConfigDO endpointsConfigDO = new EndpointsConfigDO();
        endpointsConfigDO.setSelectAllUrl(createUrl(tab.maintain()));
        return endpointsConfigDO;
    }

    protected List<MenuDO> buildSubMenuForms(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(UiSubMenu.class))
                .map(LambdaExceptionHelper.rethrowFunction(this::createSubMenu))
                .collect(toList());
    }

    protected MenuDO createSubMenu(Field f) throws GUSLErrorException {
        UiSubMenu subMenu = f.getAnnotation(UiSubMenu.class);

        if (SubMenuType.FORM == subMenu.type()) {
            return createSubMenuForm(f, subMenu);
        } else if (SubMenuType.TABLE == subMenu.type()) {
            return createSubMenuTable(f, subMenu);
        } else if (SubMenuType.ELASTIC_TABLE == subMenu.type()) {
            return createSubMenuElasticTable(f, subMenu);
        } else if (SubMenuType.CASSANDRA_TABLE == subMenu.type()) {
            return createSubMenuCassandraTable(f, subMenu);
        } else if (SubMenuType.BLAST_COLLECTION_TABLE == subMenu.type()) {
            return createSubMenuBlastCollectionTable(f, subMenu);
        } else if (SubMenuType.BLAST_COLLECTION_FORM == subMenu.type()) {
            return createSubMenuBlastCollectionForm(f, subMenu);
        } else if (SubMenuType.DATA_TABLE == subMenu.type()) {
            return createSubMenuDataTable(f, subMenu);
        } else if (SubMenuType.ALPHA_LIST == subMenu.type()) {
            return createSubMenuDataTable(f, subMenu);
        } else if (SubMenuType.FLEX_LIST == subMenu.type()) {
            return createSubMenuDataTable(f, subMenu);
        } else if (SubMenuType.ACCORDION_LIST == subMenu.type()) {
            return createSubMenuDataTable(f, subMenu);
        } else {
            throw new GUSLErrorException(SystemErrors.ERR_SERVER_ERROR.getError("Sub menu type for field " + f.getName() + " has unrecognized value: " + subMenu.type()));
        }
    }

    protected MenuDO createSubMenuElasticTable(Field f, UiSubMenu subMenu) {
        SortDTO sortDTO = sortStringToObj(subMenu.defaultSortString());

        return menuItem()
                .code(subMenu.code())
                .label(subMenu.label())
                .elastic(
                        new ElasticQuery.Builder(subMenu.esoClass())
                                .title(subMenu.label())
                                .index(subMenu.indexName())
                                .convertTo(subMenu.convertEsoTo().equals(Object.class) ? null : subMenu.convertEsoTo())
                                .conditionals(createConditionals(f))
                                .sortTieBreaker(subMenu.sortTieBreaker())
                                .defaultSort(sortDTO)
                                .getOneUrl(subMenu.getUrl())
                                .aggsUrl(emptyToNull(subMenu.aggsUrl()))
                                .initialConditions(FormBuilder.constructInitialConditionsForElasticTab(Arrays.asList(subMenu.conditions())))
                                .expandable(subMenu.expandable())
                                .restActions(createActionsDTO(subMenu.actions()))
                                // .rowActions(createActionsDTO(subMenu.rowActions()))
                                .groupActions(createActionsDTO(subMenu.groupActions()))
                                .orderByActions(createOrderBy(subMenu.orderByActions()))
                                .elasticFilterGroup(emptyToNull(subMenu.elasticQueryGroup()))
                                .getUrl(emptyToNull(subMenu.elasticCustomGetUrl()))
                                .mediaType(subMenu.mediaType())
                                .build()
                )
                .build();
    }

    protected MenuDO createSubMenuDataTable(Field f, UiSubMenu subMenu) {
        return menuItem()
                .code(subMenu.code())
                .label(subMenu.label())
                .permission(subMenu.permission())
                .dataTable(new DataTableQuery.Builder(subMenu.esoClass())
                        .selectUrl(subMenu.getUrl())
//                        .convertTo(subMenu.convertEsoTo().equals(Object.class) ? null : subMenu.convertEsoTo())
                        .title(subMenu.label())
                        .expandable(subMenu.expandable())
                        .initialConditions(FormBuilder.constructInitialConditionsForElasticTab(Arrays.asList(subMenu.conditions())))
                        .conditionals(createConditionals(f))
                        .defaultSort(sortStringToObj(subMenu.defaultSortString()))
                        .restActions(createActionsDTO(subMenu.actions()))
                        .rowActions(createActionsDTO(subMenu.rowActions()))
                        .groupActions(createActionsDTO(subMenu.groupActions()))
                        .orderByActions(createOrderBy(subMenu.orderByActions()))
                        .refreshRate(subMenu.refreshRate())
                        .mediaType(subMenu.mediaType())
                        .alphaList(subMenu.type() == SubMenuType.ALPHA_LIST)
                        .flexList(subMenu.type() == SubMenuType.FLEX_LIST)
                        .accordionList(subMenu.type() == SubMenuType.ACCORDION_LIST)
                        .build())
                .build();
    }

    protected MenuDO createSubMenuCassandraTable(Field f, UiSubMenu subMenu) {
        return menuItem()
                .code(subMenu.code())
                .label(subMenu.label())
                .permission(subMenu.permission())
                .cassandra(new CassandraQuery.Builder(subMenu.esoClass())
                        .selectUrl(subMenu.getUrl())
                        .convertTo(subMenu.convertEsoTo().equals(Object.class) ? null : subMenu.convertEsoTo())
                        .title(subMenu.label())
                        .expandable(subMenu.expandable())
                        .initialConditions(FormBuilder.constructInitialConditionsForElasticTab(Arrays.asList(subMenu.conditions())))
                        .conditionals(createConditionals(f))
                        .sortTieBreaker(subMenu.sortTieBreaker())
                        .defaultSort(sortStringToObj(subMenu.defaultSortString()))
                        .restActions(createActionsDTO(subMenu.actions()))
                        .rowActions(createActionsDTO(subMenu.rowActions()))
                        .groupActions(createActionsDTO(subMenu.groupActions()))
                        .orderByActions(createOrderBy(subMenu.orderByActions()))
                        .elasticFilterGroup(emptyToNull(subMenu.elasticQueryGroup()))
                        .refreshRate(subMenu.refreshRate())
                        .mediaType(subMenu.mediaType())
                        .build())
                .build();
    }

    protected MenuDO createSubMenuBlastCollectionTable(Field f, UiSubMenu subMenu) {
        return menuItem()
                .code(subMenu.code())
                .label(subMenu.label())
                .permission(subMenu.permission())
                .blastCollectionTable(new BlastCollectionQuery.Builder(subMenu.esoClass())
                        .collectionName(subMenu.collectionName())
                        .getUrl(subMenu.getUrl())
                        .title(subMenu.label())
                        .expandable(subMenu.expandable())
                        .rowActions(createActionsDTO(subMenu.rowActions()))
                        .groupActions(createActionsDTO(subMenu.groupActions()))
                        .tableActions(createActionsDTO(subMenu.actions()))
                        .orderByActions(createOrderBy(subMenu.orderByActions()))
                        .mediaType(subMenu.mediaType())
                        .build())
                .build();
    }

    protected MenuDO createSubMenuBlastCollectionForm(Field f, UiSubMenu subMenu) {
        return menuItem()
                .code(subMenu.code())
                .label(subMenu.label())
                .permission(subMenu.permission())
                .blastCollectionForm(new BlastCollectionQuery.Builder(subMenu.esoClass())
                        .collectionName(subMenu.collectionName())
                        // .getUrl(subMenu.getUrl())
                        .title(subMenu.label())
                        // .expandable(subMenu.expandable())
                        .rowActions(createActionsDTO(subMenu.rowActions()))
                        .groupActions(createActionsDTO(subMenu.groupActions()))
                        .orderByActions(createOrderBy(subMenu.orderByActions()))
                        .tableActions(createActionsDTO(subMenu.actions()))
                        .mediaType(subMenu.mediaType())
                        .build())
                .build();
    }

    protected static SortDTO sortStringToObj(String str) {
        return Optional.ofNullable(StringUtils.stripToNull(str))
                .map(ds -> {
                    String[] split = ds.split(",");
                    if (split.length == 2 && nonNull(split[0]) && nonNull(split[1])) {
                        return new SortDTO(split[0].trim(), split[1].trim());
                    }
                    return null;
                })
                .orElse(null);
    }

    protected MenuDO createSubMenuTable(Field f, UiSubMenu subMenu) {
        Class<?> clazz = (Class) ((ParameterizedType) f.getGenericType()).getActualTypeArguments()[0];
        // logger.info(clazz.getName());
        final MaintainTableTab maintainTableTab = new MaintainTableTab(
                createEndpoints(subMenu),
                createTableView(subMenu, subMenu.label(), clazz, f));

        List<MaintainTableTab> tabs = Collections.singletonList(maintainTableTab);

        return subMenuItem()
                .label(subMenu.label())
                .code(subMenu.code())
                .permission(subMenu.permission())
                .maintain(new MaintainBuilder.Builder()
                        .tabs(tabs)
                        .row(FormBuilder.getRowConfig(clazz))
                        .actions(createActionsDTO(subMenu.actions()))
                        .rowActions(createActionsDTO(subMenu.rowActions()))
                        .groupActions(createActionsDTO(subMenu.groupActions()))
                        .orderByActions(createOrderBy(subMenu.orderByActions()))
                        .mediaType(subMenu.mediaType())
                        .build())
                .build();
    }

    public static TableViewConfigDO createTableView(
            UiTabDefinition tab,
            TableRowDefinition definition,
            Class<?> clazz,
            FormRowConfigDO row,
            Field field
    ) {
        TableViewConfigDO tableView = tableView()
                .title(tab.label())
                .code(tab.code())
                .actions(Arrays.stream(definition.actions()).map(action -> new TableActionsConfigDO()).collect(toList()))
                .paged(tab.maintain().paged())
                .disableTransactionControls(definition.disableTableControls())
                .disableColumnResize(definition.disableColumnResize())
                .headerUrl(definition.headerUrl())
                .refreshRate(definition.refreshRate())
                .build();

        List<FieldConfigDO> filters = !definition.filterClass().equals(Object.class)
                ? getFilters(definition.filterClass())
                : getFilters(clazz);

        tableView.setDefaultSort(sortStringToObj(tab.defaultSortString()));
        tableView.setFilters(filters);

        ComplexTableRowsDefinition annotation = field.getAnnotation(ComplexTableRowsDefinition.class);

        if (definition.expandable() || nonNull(annotation)) {
            ConditionalRowDetailsDTO conditional = new ConditionalRowDetailsDTO();
            conditional.setExpandable(true);
            conditional.setDetailsConfig(row);

            if (annotation.value().length > 0) {
                conditional.setTable(annotation.value()[0].table());
                conditional.setUrl(annotation.value()[0].getDetailsUrl());
            }

            tableView.setConditionals(Collections.singletonList(conditional));
        }
        return tableView;
    }

    public static List<FieldConfigDO> getFilters(Class<?> type) {
        return Arrays.stream(type.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(UiFilter.class))
                .map(FormBuilder::createFilterField)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
    }

    protected TableViewConfigDO createTableView(UiSubMenu subMenu, String title, Class<?> clazz, Field f) {
        // TableViewConfigDO tableView = FormBuilder.getTableView(title, false, false, false, false, true);
        TableViewConfigDO tableView = FormBuilder.getTableView(title, null);

        List<FieldConfigDO> filters = getFilters(clazz);
        tableView.setFilters(filters);

        tableView.setConditionals(createConditionals(f));
        tableView.setDefaultSort(sortStringToObj(subMenu.defaultSortString()));
        return tableView;
    }

    public static List<ConditionalRowDetailsDTO> createConditionals(Field field) {
        if (!field.isAnnotationPresent(ComplexTableRowsDefinition.class)) {
            return Collections.emptyList();
        }
        ComplexTableRowsDefinition annotation = field.getAnnotation(ComplexTableRowsDefinition.class);
        return Arrays.stream(annotation.value())
                .map(EntityGroupMenuBuilder::toDTO)
                .collect(toList());
    }

    public static ConditionalRowDetailsDTO toDTO(ComplexTableRowDetails annotation) {
        ConditionalRowDetailsDTO rowDetails = new ConditionalRowDetailsDTO();
        rowDetails.setUrl(annotation.getDetailsUrl());
        if (nonNull(annotation.configClass())) {
            rowDetails.setDetailsConfig(getRowsForCardDefClass(annotation.configClass()).get(0));
        }
        rowDetails.setGrouped(annotation.grouped());
        rowDetails.setCode(annotation.code());
        rowDetails.setDetailsButton(annotation.detailsButton());
        rowDetails.setTable(annotation.table());

        List<RowConditionDTO> conditions = Arrays.stream(annotation.conditions())
                .map(c -> new RowConditionDTO(c.field(), c.value()))
                .collect(toList());
        rowDetails.setConditions(conditions);
        rowDetails.setExpandable(annotation.expandable());
        return rowDetails;
    }

    protected EndpointsConfigDO createEndpoints(UiSubMenu subMenu) {
        EndpointsConfigDO endpointsConfigDO = new EndpointsConfigDO();
        endpointsConfigDO.setSelectAllUrl(subMenu.getUrl());
        return endpointsConfigDO;
    }

    protected MenuDO createSubMenuForm(Field f, UiSubMenu subMenu) {
        return subMenuItem()
                .label(subMenu.label())
                .code(subMenu.code())
                .entityGroupSingleForm(createSingleForm(f, subMenu))
                .getUrl(subMenu.getUrl())
                .build();
    }

    protected SingleFormConfigDO createSingleForm(Field f, UiSubMenu subMenu) {
        try {
            return new EntityGroupSingleForm.Builder()
                    .title(subMenu.label())
                    .updateUrl(getUpdateUrlForMenu(subMenu))
                    .rows(getRowsForCardDefClass(f.getType()))
                    .build();
        } catch (Throwable t) {
            logger.error("Error creating single form field: {} subMenu: {}", f, subMenu, t);
            return new SingleFormConfigDO();
        }
    }

    public EntityGroupMenuBuilder singleton() {
        this.singleton = true;
        return this;
    }

    public EntityGroupMenuBuilder showInSideBar(boolean showInSideBar) {
        this.showInSideBar = showInSideBar;
        return this;
    }

    public EntityGroupMenuBuilder mediaType(MediaType mediaType) {
        this.mediaType = mediaType;
        return this;
    }

    protected TableControlDO createTableControl(TableControlsDefinition tableControls) {
        return TableControlDO.builder()
                .pagination(tableControls.pagination())
                .resize(tableControls.resize())
                .filters(tableControls.filters())
                .search(tableControls.search())
                .refresh(tableControls.refresh())
                .columnSettings(tableControls.columnSettings())
                .build();
    }

}
