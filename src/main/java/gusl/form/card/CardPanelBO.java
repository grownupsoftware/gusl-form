package gusl.form.card;

import gusl.core.tostring.ToString;
import gusl.form.model.ActionConfigDO;
import gusl.report.model.ReportDO;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CardPanelBO {

    private String title;

    private String subTitle;

    private ActionConfigDO form;

    private ReportDO report;

    private String reportUrl;

    private String bespokeRoute;

    private CardPagedQueryBO pagedQuery;

    @Singular
    private List<ActionConfigDO> actions;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
