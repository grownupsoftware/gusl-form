package gusl.form.card;

import gusl.core.tostring.ToString;
import gusl.form.model.ActionConfigDO;
import gusl.report.model.ReportDO;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CardBO {

    private String swipeLeftIcon;

    private String swipeRightIcon;

    private String moreIcon;

    @Singular
    private List<ActionConfigDO> swipeDowns;
    @Singular
    private List<ActionConfigDO> swipeRights;
    @Singular
    private List<ActionConfigDO> swipeUps;
    @Singular
    private List<ActionConfigDO> swipeLefts;
    @Singular
    private List<ActionConfigDO> doubleTaps;

    private String label;
    private String subLabel;
    private ReportDO content;

    private CardPanelBO cardPanel;

    private Object formValue;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
