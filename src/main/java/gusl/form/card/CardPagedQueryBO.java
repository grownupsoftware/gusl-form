package gusl.form.card;

import gusl.core.tostring.ToString;
import gusl.model.query.QueryParamsDTO;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CardPagedQueryBO {

    private String url;

    private QueryParamsDTO queryParams;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
