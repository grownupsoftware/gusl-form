package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TickerConfigDO extends MaintainConfigDO {
    private String code;
    private List<TickerCommandDTO> commands;
    private boolean hideCohort;
    private boolean hideLock;
    private boolean hideFilter;
    private boolean showBorder;
    private boolean showAdd;
    private boolean hideRefresh;
    private boolean showPopout;
    private int popoutWidth;
    private int popoutHeight;
    private boolean hideHeader;
    private boolean hidePagination;
}
