package gusl.form.model;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.report.model.ReportDO;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HeaderDO {

    // Fields are rendered in the UI in the same order as defined here (changing the order will not change the UI)

    @DocField(description = "Icon to be displayed")
    private String icon;

    @DocField(description = "Images to be displayed")
    private String image;

    @DocField(description = "Id field - can be text not related to a unique Id")
    private String id;

    @DocField(description = "Name - or any descriptive field")
    private String name;

    @DocField(description = "Extra info defined as a report for more complex headers")
    private ReportDO info;

    @DocField(description = "Wrap header in a link - will open a new tab")
    private String linkUrl;

    @DocField(description = "Css to be applied to Id")
    private String idCss;

    @DocField(description = "Css to be applied to Name")
    private String nameCss;

    @DocField(description = "Css to be applied to icon")
    private String iconCss;

    @DocField(description = "Css to be applied to image")
    private String imageCss;

    @DocField(description = "left / center / right")
    private String align;

    @DocField(description = "rate at which header API is called, -1 no refresh")
    private Integer refreshRate;

    public static HeaderDO ofId(String id) {
        return HeaderDO.builder().id(id).build();
    }

    public static HeaderDO ofName(String name) {
        return HeaderDO.builder().id(name).build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
