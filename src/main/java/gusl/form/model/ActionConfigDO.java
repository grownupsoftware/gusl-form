package gusl.form.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@ToString
public class ActionConfigDO extends BaseActionConfigDO {

    private String url;
    private String templateUrl;
    private String method;
    private List<ActionConfigDO> children;
    private String routerLink;
    private String modalCode;
    //    private String route;
    private String altSaveUrl;
}
