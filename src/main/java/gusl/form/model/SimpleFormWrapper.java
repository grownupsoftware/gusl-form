package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class SimpleFormWrapper<T> {
    private T content;

    public static <T> SimpleFormWrapper wrap(T content) {
        return new SimpleFormWrapper<>(content);
    }

}
