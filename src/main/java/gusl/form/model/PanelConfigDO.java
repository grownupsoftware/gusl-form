package gusl.form.model;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PanelConfigDO {
    private String panelCss;

    private boolean noTitle;
    private String title;
    private String titleCss;

    private List<FormRowConfigDO> rows;

    private String selectUrl;
    private String blastCollection;
    private String blastTopic;

    private Integer fxFlex;

    // not implemented
    private String jsEditCondition;
    private List<ActionConfigDO> actions;

    private Integer refreshRate;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
