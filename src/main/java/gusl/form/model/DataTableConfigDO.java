package gusl.form.model;

import gusl.annotations.form.MediaType;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class DataTableConfigDO extends MaintainConfigDO {
    private String selectUrl;
    private String dtoClass;
    private String blastDeltaCommand;

    private Integer refreshRate;

    private MediaType mediaType;

    private boolean alphaList;
    private boolean flexList;
    private boolean accordionList;

    private String defaultSortString;



}
