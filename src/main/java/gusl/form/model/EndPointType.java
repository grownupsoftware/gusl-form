package gusl.form.model;

public enum EndPointType {
    POST("POST"),
    GET("GET"),
    PUT("PUT"),
    DELETE("ENABLED");

    private final String name;

    EndPointType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
