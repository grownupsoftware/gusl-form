package gusl.form.model;

import gusl.core.tostring.ToString;
import gusl.form.builder.FormBuilder;
import gusl.report.model.ReportDO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SummaryDO<T> {

    private List<FormRowConfigDO> form;
    private List<T> tableData;
    private T formData;
    private ReportDO report;
    private SummaryType type;
    private boolean noTableHeader;
    private String label;
    private List<ActionConfigDO> tableActions;
    private List<ActionConfigDO> rowActions;
    private String blastDeltaCommand;
    private boolean highlightDelta;
    private boolean disableTableControls;
    private boolean disableColumnResize;

    public static <T> SummaryDO.SummaryDOBuilder<T> builder() {
        return new SummaryDO.SummaryDOBuilder<T>();
    }

    public static class SummaryDOBuilder<T> {
        private List<FormRowConfigDO> form;
        private List<T> tableData;
        private T formData;
        private ReportDO report;
        private SummaryType type;
        private boolean noTableHeader;
        private String label;
        private List<ActionConfigDO> tableActions;
        private List<ActionConfigDO> rowActions;

        private String blastDeltaCommand;

        private boolean highlightDelta;
        private boolean disableTableControls;
        private boolean disableColumnResize;

        private Integer refreshRate;

        public SummaryDOBuilder<T> tableData(List<T> tableData) {
            this.tableData = tableData;
            this.type = SummaryType.TABLE;
            return this;
        }

        public SummaryDOBuilder<T> formData(T formData) {
            this.formData = formData;
            this.type = SummaryType.FORM;
            return this;
        }

        public SummaryDOBuilder<T> report(ReportDO report) {
            this.report = report;
            this.type = SummaryType.REPORT;
            return this;
        }

        public SummaryDOBuilder<T> form(Class<T> klass) {
            this.form = FormBuilder.getRowsConfig(klass);
            return this;
        }

        public SummaryDOBuilder<T> blastDeltaCommand(String blastDeltaCommand) {
            this.blastDeltaCommand = blastDeltaCommand;
            return this;
        }

        public SummaryDOBuilder<T> highlightDelta(boolean highlightDelta) {
            this.highlightDelta = highlightDelta;
            return this;
        }

        public SummaryDOBuilder<T> disableTableControls(boolean disableTableControls) {
            this.disableTableControls = disableTableControls;
            return this;
        }

        public SummaryDOBuilder<T> disableColumnResize(boolean disableColumnResize) {
            this.disableColumnResize = disableColumnResize;
            return this;
        }

        public SummaryDOBuilder<T> noTableHeader() {
            this.noTableHeader = true;
            return this;
        }

        public SummaryDOBuilder<T> label(String label) {
            this.label = label;
            return this;
        }

        public SummaryDOBuilder<T> tableActions(List<ActionConfigDO> tableActions) {
            this.tableActions = tableActions;
            return this;
        }

        public SummaryDOBuilder<T> rowActions(List<ActionConfigDO> rowActions) {
            this.rowActions = rowActions;
            return this;
        }

        public SummaryDO<T> build() {
            SummaryDO<T> summary = new SummaryDO<>();
            summary.setTableData(tableData);
            summary.setFormData(formData);
            summary.setReport(report);
            summary.setForm(form);
            summary.setType(type);
            summary.setNoTableHeader(noTableHeader);
            summary.setLabel(label);
            summary.setTableActions(tableActions);
            summary.setRowActions(rowActions);
            summary.setBlastDeltaCommand(blastDeltaCommand);
            summary.setHighlightDelta(highlightDelta);
            summary.setDisableTableControls(disableTableControls);
            summary.setDisableColumnResize(disableColumnResize);
            return summary;
        }
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
