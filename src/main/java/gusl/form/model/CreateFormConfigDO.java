package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class CreateFormConfigDO {

    private List<FormRowConfigDO> rows;
    private String title;
    private EndPointDO insertUrl;
    private EndPointDO templateUrl;
    private String jsEditCondition;
    private boolean multipartForm;
    private String permission;

}
