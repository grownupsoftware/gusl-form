package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class AggFieldConfigDO {
    private AggFieldType type;

}
