package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class EndpointsConfigDO {

    private String selectAllUrl;
    private String selectOneUrl;
    private String insertUrl;
    private String updateUrl;
    private String deleteUrl;

    public EndpointsConfigDO(String selectOneUrl) {
        this.selectOneUrl = selectOneUrl;
    }
}
