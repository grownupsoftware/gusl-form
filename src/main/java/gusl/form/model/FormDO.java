package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class FormDO {

    private String code;

    private String title;

    private String updateUrl;

    private FormRowConfigDO rowConfig;

}
