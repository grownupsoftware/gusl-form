package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class FormValidator {
    private FormValidatorType type;
    private Long longValue;

    public FormValidator(FormValidatorType type) {
        this.type = type;
    }
}
