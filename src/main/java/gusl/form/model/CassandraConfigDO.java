package gusl.form.model;

import gusl.annotations.form.MediaType;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CassandraConfigDO extends MaintainConfigDO {
    private String selectUrl;
    private String dtoClass;
    private String convertTo;
    private String blastDeltaCommand;
    private boolean highlightDelta;
    private boolean disableTableControls;
    private boolean disableColumnResize;

    private Integer refreshRate;

    private MediaType mediaType;

}
