package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class EntityGroupHeaderDO {

    private List<FormRowConfigDO> rows;
    private String backgroundColor;
    private Integer refreshRateInSeconds;
    private String url;
    private String fontColor;
    private String title;
    private String image;
    private List<ActionConfigDO> actions;
    private List<PanelConfigDO> panels;
    private Boolean withRefreshButton;

}
