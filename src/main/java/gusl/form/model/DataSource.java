package gusl.form.model;

public class DataSource<T extends MaintainConfigDO> {
    private MenuType type;
    private T definition;

    public DataSource() {
    }

    public DataSource(MenuType type, T definition) {
        this.type = type;
        this.definition = definition;
    }

    public MenuType getType() {
        return type;
    }

    public void setType(MenuType type) {
        this.type = type;
    }

    public T getDefinition() {
        return definition;
    }

    public void setDefinition(T definition) {
        this.definition = definition;
    }
}
