package gusl.form.model;

public enum MenuType {
    ELASTIC("ELASTIC"),
    SINGLE_FORM("SINGLE_FORM"),
    ENTITY_GROUP_SINGLE_FORM("ENTITY_GROUP_SINGLE_FORM"),
    ENTITY_GROUP("ENTITY_GROUP"),
    MAINTAIN("MAINTAIN"),
    DASHBOARD("DASHBOARD"),
    TICKER("TICKER"),
    QUERY("QUERY"),
    SINGLE_PAGE("SINGLE_PAGE"),
    CASSANDRA("CASSANDRA"),

    //multiple from subset (ENTITY_GROUP, TICKER, ELASTIC)
    MULTIPLE("MULTIPLE"),
    BLAST_COLLECTION_TABLE("BLAST_COLLECTION_TABLE"),

    DATA_TABLE("DATA_TABLE"),

    BLAST_COLLECTION_FORM("BLAST_COLLECTION_FORM");

    private final String name;

    MenuType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
