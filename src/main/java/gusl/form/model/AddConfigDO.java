package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class AddConfigDO {

    private boolean allowAdd;
    private String title;

}
