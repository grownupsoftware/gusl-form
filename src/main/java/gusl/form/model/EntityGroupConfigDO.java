package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class EntityGroupConfigDO {

    private EntityGroupHeaderDO header;
    private List<MenuDO> menuItems;
    private String title;
    private String selectUrl;
    //supports elastic and maintain
    private MenuType selectTableType;
    private MenuDO createConfig;
    private List<DataSource> dataSources;
    private String titleTag;
    private MenuDO entryForm;
    private String help;
    private String headerUrl;

}
