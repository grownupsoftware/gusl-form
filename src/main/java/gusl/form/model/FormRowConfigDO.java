package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class FormRowConfigDO {

    private List<FormColumnConfigDO> columns;
    private String css;
    private Integer cols;

    public FormRowConfigDO(List<FormColumnConfigDO> columns) {
        this.columns = columns;
    }
}
