package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class RowConditionDTO {
    private String field;
    private String value;
    private String operand;

    public RowConditionDTO(String field, String value) {
        this.field = field;
        this.value = value;
    }
}
