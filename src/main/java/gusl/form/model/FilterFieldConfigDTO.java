package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class FilterFieldConfigDTO {

    private List<FormValidator> validators;
    private String name;

}
