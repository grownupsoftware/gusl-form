package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class MaintainTableTab {
    private EndpointsConfigDO endpoints;
    private TableViewConfigDO tableView;
    private List<RowConditionDTO> initialConditions;
    private String permission;

    private Class<?> dtoClass;

    public MaintainTableTab(EndpointsConfigDO endpoints, TableViewConfigDO tableView) {
        this.endpoints = endpoints;
        this.tableView = tableView;
    }

    public MaintainTableTab(TableViewConfigDO tableView, List<RowConditionDTO> initialConditions) {
        this.tableView = tableView;
        this.initialConditions = initialConditions;
    }

    public MaintainTableTab(TableViewConfigDO tableView, List<RowConditionDTO> initialConditions, Class<?> dtoClass) {
        this.tableView = tableView;
        this.initialConditions = initialConditions;
        this.dtoClass = dtoClass;
    }
}
