package gusl.form.model;

import lombok.*;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class UrlDO {
    private String path;
    private Map<String, List<String>> params;

}
