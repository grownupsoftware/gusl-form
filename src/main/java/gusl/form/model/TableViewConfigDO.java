package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class TableViewConfigDO {

    private String title;
    private AddConfigDO add;
    private List<TableActionsConfigDO> actions;
    private int[] rowsPerPageOptions;
    private List<FieldConfigDO> filters;
    private String code;
    private List<ConditionalRowDetailsDTO> conditionals;
    private SortDTO defaultSort;
    private String aggsUrl;
    private String sortTieBreaker;
    private Boolean paged;
    private boolean expandable;
    private boolean actionable;
    private String headerUrl;
    private boolean disableTableControls;
    private boolean disableColumnResize;
    private String blastDeltaCommand;

    private Integer refreshRate;

}
