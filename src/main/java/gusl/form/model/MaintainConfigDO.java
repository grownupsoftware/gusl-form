package gusl.form.model;

import gusl.annotations.form.MediaType;
import gusl.annotations.form.page.OrderByActionDO;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class MaintainConfigDO {

    protected List<MaintainTableTab> tabs;
    protected List<FormRowConfigDO> rows;
    protected Boolean tabsWithCount;
    protected String countUrl;
    protected List<ActionConfigDO> restActions;
    protected List<ActionConfigDO> overallRestActions;
    protected List<ActionConfigDO> rowActions;
    protected String title;
    protected String id;
    protected boolean noLabel;

    protected TableControlDO tableControl;
    protected List<ActionConfigDO> groupActions;
    protected List<OrderByActionDO> orderByActions;

    private MediaType mediaType;

}
