package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class TableActionsConfigDO {

    private String tag;
    private String pageTitle;
    private boolean overrideHandle;
    private String cssIcon;
    private String tooltip;
    private String conditional;
    private String permission;

}
