package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class TreeTypeDefDTO {
    private String type;
    private List<FieldConfigDO> fields;
    private String updateUrl;
    private String addUrl;
    private String reorderUrl;
    private String deleteUrl;
    private List<String> childrenTypes;

}
