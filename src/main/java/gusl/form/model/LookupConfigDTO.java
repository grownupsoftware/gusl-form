package gusl.form.model;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LookupConfigDTO {
    private String name;
    private String extractLabel;
    private List<String> tableFields;
    private List<String> fieldTypes;
    private String sortBy;
    private boolean pageable;
    private String externalUrl;
    private String uniqueId;
    private String retValue;
    private boolean inline;
    private Class<?> lookupClass;
    private List<FieldConfigDO> fields;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
