package gusl.form.model;

import gusl.annotations.form.breadcrumb.BreadCrumbsDO;
import gusl.annotations.form.page.FilterActionControlDO;
import gusl.annotations.form.page.OrderByActionDO;
import gusl.annotations.form.page.PagedDAOResponse;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import gusl.query.QueryParams;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PagedSummaryResponse<T, H, F> {

    @ToStringCount
    private List<T> content;
    private QueryParams queryParams;
    private int total;
    private SummaryDO<H> header;
    private SummaryDO<F> footer;
    private BreadCrumbsDO breadCrumb;
    List<OrderByActionDO> orderByActions;
    FilterActionControlDO filterAction;
    private String noRowsMessage;
    private String noRowsAutoRoute;
    private List<Integer> rowsPerPage;

    public static <T, H, F> PagedSummaryResponse.PagedSummaryResponseBuilder<T, H, F> builder() {
        return new PagedSummaryResponse.PagedSummaryResponseBuilder<T, H, F>();
    }

    public static class PagedSummaryResponseBuilder<T, H, F> {
        private List<T> content;
        private QueryParams queryParams;
        private int total;

        private SummaryDO<H> header;
        private SummaryDO<F> footer;

        private BreadCrumbsDO breadCrumb;
        List<OrderByActionDO> orderByActions;
        FilterActionControlDO filterAction;
        private String noRowsMessage;
        private String noRowsAutoRoute;
        private List<Integer> rowsPerPage;

        PagedSummaryResponseBuilder() {

        }

        public PagedSummaryResponseBuilder<T, H, F> pagedResponse(PagedDAOResponse<T> response) {
            this.content = response.getContent();
            this.queryParams = response.getQueryParams();
            this.total = response.getTotal();
            this.breadCrumb = response.getBreadCrumb();
            this.orderByActions = response.getOrderByActions();
            this.filterAction = response.getFilterAction();
            this.noRowsMessage = response.getNoRowsMessage();
            this.noRowsAutoRoute = response.getNoRowsAutoRoute();
            this.rowsPerPage = response.getRowsPerPage();
            return this;
        }

        public PagedSummaryResponseBuilder<T, H, F> header(SummaryDO<H> header) {
            this.header = header;
            return this;
        }

        public PagedSummaryResponseBuilder<T, H, F> footer(SummaryDO<F> footer) {
            this.footer = footer;
            return this;
        }

        public PagedSummaryResponse<T, H, F> build() {
            PagedSummaryResponse<T, H, F> response = new PagedSummaryResponse<>();
            response.setContent(content);
            response.setTotal(total);
            response.setQueryParams(queryParams);
            response.setHeader(header);
            response.setFooter(footer);
            response.setBreadCrumb(breadCrumb);
            response.setOrderByActions(orderByActions);
            response.setFilterAction(filterAction);
            response.setNoRowsMessage(noRowsMessage);
            response.setNoRowsAutoRoute(noRowsAutoRoute);
            response.setRowsPerPage(rowsPerPage);
            return response;
        }
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
