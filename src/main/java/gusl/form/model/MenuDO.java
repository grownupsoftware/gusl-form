package gusl.form.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import gusl.annotations.form.MediaType;
import gusl.model.cohort.CohortIdsDO;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class MenuDO {

    private String label;
    private String code;
    private String help;
    private String icon;
    private String route;
    private boolean iconOnly;
    private String permission;
    private String command;
    private Integer displayOrder;
    private ActionConfigDO action;

    private CohortIdsDO allowedCohorts;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Boolean grouped;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Boolean showInSideBar;

    private String selectUrl;
    private SingleFormConfigDO singleForm;

    private SinglePageConfigDO singlePage;

    private MenuType menuType;
    private EntityGroupConfigDO entityGroup;

    private CreateFormConfigDO createForm;
    private String headerUrl;
    private String externalUrl;
    private String component;

    private MediaType mediaType;

    private boolean footer;

    private boolean overlay;

    private boolean noLabel;

    private Boolean mobileTopSticky;

}
