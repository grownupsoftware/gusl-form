package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class UiRefDTO {

    private String nestedId;
    private String entity;
    private String code;
    private String tab;

}
