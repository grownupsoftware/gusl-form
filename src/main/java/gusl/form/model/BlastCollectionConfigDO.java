package gusl.form.model;

import gusl.annotations.form.MediaType;
import gusl.core.tostring.ToString;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BlastCollectionConfigDO extends MaintainConfigDO {
    protected String collectionName;
    protected String title;
    protected List<ActionConfigDO> rowActions;
    protected List<ActionConfigDO> tableActions;
    protected List<FormRowConfigDO> rows;

    protected boolean expandable;
    protected String getUrl;
    protected List<FormRowConfigDO> expandedRows;
    private TableControlDO tableControl;
    protected List<ActionConfigDO> groupActions;

    private MediaType mediaType;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
