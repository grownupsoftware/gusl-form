package gusl.form.model;

import gusl.annotations.form.MediaType;
import lombok.*;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ElasticConfigDO extends MaintainConfigDO {

    private boolean withTimestamp;
    private String convertTo;
    private String indexClass;
    private String indexName;
    private String elasticFilterGroup;

    private Integer refreshRate;

    private MediaType mediaType;

}
