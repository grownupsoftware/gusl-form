package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class SingleFormConfigDO {

    private List<FormRowConfigDO> rows;
    private String title;
    private EndPointDO updateUrl;
    private String jsEditCondition;
    private boolean multipartForm;
    private String permission;

    private boolean noBanner;

}
