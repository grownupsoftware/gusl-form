package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class SimpleQueryConfigDO {

    private Class inboundClass;

    private Class outboundClass;

    private String endpoint;
}
