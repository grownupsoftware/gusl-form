package gusl.form.model;

import lombok.*;

@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ElasticTableConfig {
}
