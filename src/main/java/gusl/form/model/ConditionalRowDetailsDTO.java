package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ConditionalRowDetailsDTO {
    private List<RowConditionDTO> conditions;
    private String url;
    private FormRowConfigDO detailsConfig;
    private Boolean expandable;
    private boolean grouped;
    private boolean table;
    private String code;
    private boolean detailsButton;

}
