package gusl.form.model;

public enum SummaryType {
    TABLE,
    FORM,
    REPORT
}
