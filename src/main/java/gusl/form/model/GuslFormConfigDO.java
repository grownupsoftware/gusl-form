package gusl.form.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import gusl.form.builder.toolbar.TickerToolbarItem;
import gusl.form.builder.toolbar.ToolbarItem;
import lombok.*;
import nl.crashdata.chartjs.data.simple.SimpleChartJsConfig;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GuslFormConfigDO {

    private List<MenuGroupDO> menuGroups;

    private List<FormDO> forms;

    private Map<String, SimpleChartJsConfig<?, ?>> graphs;

    private List<TickerToolbarItem> toolbarTickers;

    private List<ToolbarItem> toolbarItems;

    private List<MenuGroupDO> toolbarMenus;

    private List<LookupConfigDTO> lookups;

    private List<ActionConfigDO> actions;

}
