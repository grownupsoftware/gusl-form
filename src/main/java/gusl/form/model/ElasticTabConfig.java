package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ElasticTabConfig {

    private String indexName;

    private Class esoClass;

    private Class convertToClass;

    private String queryGroup;

//    public ElasticTabConfig(String indexName, Class esoClass, Class convertToClass, String queryGroup) {
//        this.indexName = indexName;
//        this.esoClass = esoClass;
//        this.convertToClass = convertToClass;
//        this.queryGroup = queryGroup;
//    }

//    public static class Builder {
//        private String indexName;
//        private Class esoClass;
//        private Class convertToClass;
//        private String queryGroup;
//
//        public Builder indexName(String indexName) {
//            this.indexName = indexName;
//            return this;
//        }
//
//        public Builder esoClass(Class esoClass) {
//            this.esoClass = esoClass;
//            return this;
//        }
//
//        public Builder convertTo(Class convertToClass) {
//            this.convertToClass = convertToClass;
//            return this;
//        }
//
//        public Builder queryGroup(String queryGroup) {
//            this.queryGroup = queryGroup;
//            return this;
//        }
//
//        public ElasticTabConfig build() {
//            return new ElasticTabConfig(indexName, esoClass, convertToClass, queryGroup);
//        }
//    }
}
