package gusl.form.model;

import lombok.*;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class TreeOptionsDTO {
    private Map<String, TreeTypeDefDTO> types;
    private Integer depth;
}
