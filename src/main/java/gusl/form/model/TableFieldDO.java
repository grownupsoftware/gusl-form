package gusl.form.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class TableFieldDO {

    private String name;
    private String label;
    private String type;
    private boolean filterInTable;
    private String moneyFormat;
    private String dateFormat;
    private String lookup;
    private String lookupArg;

}
