package gusl.form.model;

import com.google.common.base.Strings;
import gusl.annotations.form.DurationOptions;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DurationConfig {
    boolean showLetters;
    boolean showYears;
    boolean showMonths;
    boolean showWeeks;
    boolean showDays;
    boolean showHours;
    boolean showMinutes;
    boolean showSeconds;
    boolean showButtons;
    boolean showPreview;
    String previewFormat;

    public static DurationConfig of(DurationOptions ann) {
        return DurationConfig
                .builder()
                .showLetters(ann.showLetters())
                .showYears(ann.showYears())
                .showMonths(ann.showMonths())
                .showWeeks(ann.showWeeks())
                .showDays(ann.showDays())
                .showHours(ann.showHours())
                .showMinutes(ann.showMinutes())
                .showSeconds(ann.showSeconds())
                .showButtons(ann.showButtons())
                .showPreview(ann.showPreview())
                .previewFormat(Strings.emptyToNull(ann.previewFormat()))
                .build();
    }
}
