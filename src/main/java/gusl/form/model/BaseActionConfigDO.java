package gusl.form.model;

import gusl.annotations.form.ActionType;
import gusl.annotations.form.MediaType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@SuperBuilder
public class BaseActionConfigDO {
    private String buttonLabel;
    private String popUpTitle;
    private String popUpDescription;
    private String successMessage;
    private String jsEditCondition;
    private List<FormRowConfigDO> rows;
    private List<FormRowConfigDO> confirmationFormRows;
    private String minModalWidth;
    private String minModalHeight;
    private String parentMenuLabel;
    private String jsWarningCondition;
    private String warningMessage;
    private String confirmationMessage;
    private String confirmationFields;
    private boolean downloadAction;
    private boolean multipartForm;
    private boolean dynamicResponse;
    private String confirmationUrl;
    private boolean displaySummary;
    private String permission;
    private String templateUrl;
    private String modalCode;
    private String icon;
    private boolean iconOnly;
    private boolean inLine;
    private String saveButton;
    private String cancelButton;
    private String saveIcon;
    private String cancelIcon;
    private boolean showCancel;
    private String selectUrl;
    private String selectMethod;
    private ActionType actionType;
    private int displayOrder;
    private String saveColorTheme;
    private String cancelColorTheme;
    private String tooltip;
    private String route;
    private String command;

    private Integer fxFlex;

    private MediaType mediaType;

    private String buttonCss;

    private String properties;

    private String altSaveButton;
    private String altSaveIcon;
    private String altSaveColorTheme;

}
