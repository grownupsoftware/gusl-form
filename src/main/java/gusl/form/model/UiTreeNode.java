package gusl.form.model;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class UiTreeNode {

    @UiField(type = FieldType.text)
    protected String title;

    protected List<? extends UiTreeNode> children;

    protected String type;

}
