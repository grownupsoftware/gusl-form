package gusl.form.model;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CascadingTableBO<T> {

    private List<T> content;
    private List<FormRowConfigDO> fields;

    @Singular
    private List<ActionConfigDO> actions;

    private String tableWidth;
    private boolean noHeader;
    private boolean expand;
    private boolean report;
    private String expandUrl;
    private boolean disableTableControls;
    private boolean disableColumnResize;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
