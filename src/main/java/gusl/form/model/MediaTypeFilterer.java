package gusl.form.model;

import gusl.annotations.form.MediaType;

public class MediaTypeFilterer {

    public static boolean canShow(MediaType currentDisplayMediaType, MediaType definedMediaType) {
        switch (currentDisplayMediaType) {
            case Mobile:
                switch (definedMediaType) {
                    case Mobile:
                    case MobileOnly:
                    case MobileAndTabletOnly:
                        return true;
                    default:
                        return false;
                }
            case Tablet:
                switch (definedMediaType) {
                    case Mobile:
                    case Tablet:
                    case TabletOnly:
                    case MobileAndTabletOnly:
                        return true;
                    default:
                        return false;
                }
            case Laptop:
                switch (definedMediaType) {
                    case Mobile:
                    case Tablet:
                    case Laptop:
                    case TabletOnly:
                        return true;
                    default:
                        return false;
                }
            case Desktop:
                switch (definedMediaType) {
                    case Mobile:
                    case Tablet:
                    case Laptop:
                    case Desktop:
                    case DesktopOnly:
                        return true;
                    default:
                        return false;
                }
            case ExtraLarge:
                switch (definedMediaType) {
                    case Mobile:
                    case Tablet:
                    case Laptop:
                    case Desktop:
                    case ExtraLarge:
                    case ExtraLargeOnly:
                        return true;
                    default:
                        return false;
                }
        }
        return false;
    }
}
