package gusl.form.model;

import gusl.core.tostring.ToString;
import gusl.report.model.ReportDO;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ActionCompletedBO {
    private boolean hideNotification;

    private boolean reloadTemplate;

    private String notificationMessage;

    private ReportDO report;

    public static ActionCompletedBO hideNotification() {
        return ActionCompletedBO.builder().hideNotification(true).build();
    }

    public static ActionCompletedBO notification(String notificationMessage) {
        return ActionCompletedBO.builder().notificationMessage(notificationMessage).build();
    }

    public static ActionCompletedBO notifyAndReload(String notificationMessage) {
        return ActionCompletedBO.builder().notificationMessage(notificationMessage).reloadTemplate(true).build();
    }

    public static ActionCompletedBO hideNotificationAndReload() {
        return ActionCompletedBO.builder().hideNotification(true).reloadTemplate(true).build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
