package gusl.form.model;

import gusl.annotations.form.MediaType;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TableControlDO {

    private MediaType pagination;
    private MediaType resize;
    private MediaType filters;
    private MediaType search;
    private MediaType refresh;
    private MediaType columnSettings;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
