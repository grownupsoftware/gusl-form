package gusl.form.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@SuperBuilder
public class TickerCommandDTO extends BaseActionConfigDO {
    private String command;
    private boolean confirmationRequired;
    private String icon;
    private boolean quickAccess;
    private boolean actionsBar;

}
