package gusl.form.model;

import lombok.*;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class SinglePageConfigDO {
    private List<FormRowConfigDO> rows;
    private List<FormColumnConfigDO> panels;
    private String title;
    private EndPointDO selectUrl;
    private Map<String, Object> properties;
    private String permission;

}
