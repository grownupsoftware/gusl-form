package gusl.form.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class FormColumnConfigDO {

    private List<FieldConfigDO> fields;
    private String updateUrl;
    private String deleteUrl;
    private String title;
    private String css;

    private Integer fxFlex;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean ignorePanel;

    private String code;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean fullWidth;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean nested;

    private String jsEditCondition;
    private List<ActionConfigDO> actions;
    private boolean multipartForm;
    private boolean noLabel;

    private String width;

    private String height;


    public FormColumnConfigDO(String title, List<FieldConfigDO> fields) {
        this.fields = fields;
        this.title = title;
    }

    public FormColumnConfigDO(List<FieldConfigDO> fields) {
        this.fields = fields;
    }
}
