package gusl.form.model;

import gusl.annotations.form.MediaType;
import gusl.core.tostring.ToString;
import gusl.model.cohort.CohortIdsDO;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class MenuGroupDO {

    private String label;
    private List<MenuDO> menuItems;
    private Integer displayOrder;
    private String icon;
    private boolean alwaysOpen;
    private boolean iconOnly;
    private String route;
    private String permission;
    private String code;
    private String command;
    private String align;
    private Boolean showInSideBar;
    private ActionConfigDO action;

    private boolean footer;

    private boolean overlay;

    private CohortIdsDO allowedCohorts;

    private MediaType mediaType;

    private Boolean mobileTopSticky;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
