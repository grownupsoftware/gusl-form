package gusl.form.model;

public enum AggFieldType {
    SUM, AVG
}
