package gusl.form.model;

public enum Mode {
    view("view"),
    edit("edit"),
    delete("delete"),
    add("add");

    private final String name;

    Mode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
