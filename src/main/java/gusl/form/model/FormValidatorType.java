package gusl.form.model;

public enum FormValidatorType {
    NOT_NULL, MIN, MAX, MIN_LENGTH, GUSL_MIN_LENGTH, GUSL_MAX_LENGTH
}
