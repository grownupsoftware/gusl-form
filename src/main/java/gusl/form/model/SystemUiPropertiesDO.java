package gusl.form.model;

import gusl.auth.AuthType;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemUiPropertiesDO {

    private String wsEndpoint;
    private String googleSSO;
    private String titlePrefix;
    private String homePage;
    private String logoUrl; // <== top bar
    private AuthType authType;
    private String loginTitle;
    private String loginText;
    private String loginLogo; // <== left hand side
    private String loginBackgroundImage;
    private String loginBackgroundColor;
    private String loginTextColor;
    private String loginSmallLogo; //<== above username
    private String footer;
    private ActionConfigDO changePassword;
    private String layout;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
