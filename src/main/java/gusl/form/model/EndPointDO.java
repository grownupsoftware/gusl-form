package gusl.form.model;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class EndPointDO {

    private String url;
    private EndPointType method;
    private List<String> fields;

    public static EndPointDO GET(String url) {
        return EndPointDO.builder()
                .method(EndPointType.GET)
                .url(url)
                .build();
    }

    public static EndPointDO POST(String url) {
        return EndPointDO.builder()
                .method(EndPointType.POST)
                .url(url)
                .build();
    }

    public static EndPointDO POST(String url, List<String> fields) {
        return EndPointDO.builder()
                .method(EndPointType.POST)
                .url(url)
                .fields(fields)
                .build();
    }

    public static EndPointDO PUT(String url) {
        return EndPointDO.builder()
                .method(EndPointType.PUT)
                .url(url)
                .build();
    }

    public static EndPointDO PUT(String url, List<String> fields) {
        return EndPointDO.builder()
                .method(EndPointType.PUT)
                .url(url)
                .fields(fields)
                .build();
    }

}
