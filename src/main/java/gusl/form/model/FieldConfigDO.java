package gusl.form.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import gusl.annotations.form.CohortMediaType;
import gusl.annotations.form.MediaType;
import gusl.core.annotations.DocField;
import lombok.Data;

import java.util.List;

@Data
public class FieldConfigDO {

    private String suffix;
    private String sortableField;
    private String filterElasticName;
    private String filterFromComplexName;
    private String jsValue;
    private String permission;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean jsRecalculate;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean viewHide;
    private String expandedRowColor;
    private String lookupLayout;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean canReorder;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean canDelete;

    private String reorderWeightField;
    private String codeMode;
    private String theme;
    private String externalLink;
    private List<String> filterApplyFor;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean justFilter;

    private List<AggFieldConfigDO> aggs;
    private String lookupInsertUrl;
    private String lookupSelectUrl;
    private String lookupSearchUrl;
    private String lookupCollection;

    private boolean lookupOverwritesForm;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MetricOptionsDTO metricOptions;
    private TreeOptionsDTO treeOptions;
    private String newTableElementUrl;
    private String expandablePanel;
    private boolean strippedRows;
    private boolean hideIfEmpty;

    private String help;

    private String properties;
    private String panelOptionsUrl;
    private boolean editIfTicked;
    private boolean clipboard;
    private List<ActionConfigDO> actions;
    private DurationConfig durationOptions;
    private String lookupWatcherFieldName;
    private String lookupWatcherFieldObjectId;
    private String popupUrl;

    private int displayOrder;

    private MediaType mediaType;
    private boolean hasTime;
    private boolean hasFuture;
    private boolean hasPast;
    private String distinctUrl;

    private Integer fxFlex;

    public FieldConfigDO(String name, String label, String type) {
        this.name = name;
        this.label = label;
        this.type = type;
    }

    public FieldConfigDO() {
    }

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean formFullWidth;

    @DocField(description = "Name of the field")
    private String name;

    @DocField(description = "The display label")
    private String label;

    @DocField(description = "The type of field")
    private String type;

    @DocField(description = "??")
    private String child;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "For maintain, display in table")
    private boolean displayInTable;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "Option_list show in field when selected")
    private boolean displayWhenSelected;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "For maintain, allow filtering")
    private boolean filterInTable;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "For maintain, allow advance filtering")
    private boolean advancedFilter;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "Ignore field when adding a record")
    private boolean addIgnore;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "Hide this field during edit mode")
    private boolean editHide;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "Hide this field during delete mode")
    private boolean deleteHide;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "In edit mode, field is only read only")
    private String editReadOnly;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "In add mode, field is read only")
    private boolean addReadOnly;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private boolean sortable;

    @DocField(description = "Form entry place holders")
    private String placeholder;

    @DocField(description = "Form validators")
    private List<FormValidator> validators;

    @DocField(description = "css prefix")
    private String cssClassPrefix;

    @DocField(description = "for 'option' the list of options")
    private String[] options;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    @DocField(description = "display label")
    private boolean noLabel;

    @DocField(description = "type of lookup")
    private String lookup;

    @DocField(description = "the money format")
    private String moneyFormat;

    @DocField(description = "category for lookups")
    private String category;

    @DocField(description = "css for label field")
    private String labelCss;

    @DocField(description = "css for column in table view")
    private String colCss;

    @DocField(description = "css for entry field")
    private String entryCss;

    @DocField(description = "css for label field")
    private String tableCss;

    @DocField(description = "lookup argument")
    private String lookupArg;

    @DocField(description = "property name")
    private String propertyName;

    @DocField(description = "format for date")
    private String dateFormat;

    @DocField(description = "number of decimal places for formatting")
    private String decimalPlaces;

    @DocField(description = "Form config for a panel")
    private FormRowConfigDO rowConfig;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Boolean fixedCurrency;

    private String timezone;

    private String selectUrl;

    private List<FieldConfigDO> innerFields;

    private MenuDO standaloneTableConfig;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private String canAdd;

    private String align;

    private String idFieldName;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Boolean ignoreIfNotPresent;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Boolean localeFormatted;

    private UiRefDTO ref;

    private String elasticSearchOperand;

    private String onBefore;

    private String onAfter;

    private Boolean searchable;

    private Boolean searchableSelected;

    private Boolean noAutoSearch;

    private Boolean searchbarOpen;

    private Boolean filterable;

    @DocField(description = "Extra info for input field")
    private String hint;

    @DocField(description = "used by text_comment")
    private String updateFieldUrl;

    @DocField(description = "used by text_comment")
    private String deleteFieldUrl;

    @DocField(description = "url to update order in nested table")
    private String reorderUrl;

    @DocField(description = "Only visible if empty or users current cohort matches")
    private String[] allowedCohorts;

    private CohortMediaType[] mediaRestrictions;

    private String blastDeltaCommand;

    private String keyField;

    private String jsEditCondition;

}
