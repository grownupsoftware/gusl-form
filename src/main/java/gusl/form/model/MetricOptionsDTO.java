package gusl.form.model;

public class MetricOptionsDTO {

    private String name;

    public MetricOptionsDTO() {
    }

    public MetricOptionsDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
