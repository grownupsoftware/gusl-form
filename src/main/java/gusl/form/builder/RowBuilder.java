package gusl.form.builder;

import gusl.form.model.FormColumnConfigDO;
import gusl.form.model.FormRowConfigDO;

import java.util.LinkedList;
import java.util.List;

public class RowBuilder {

    public static class Builder {

        private List<FormColumnConfigDO> columns = new LinkedList<>();
        private String css;

        public Builder columns(List<FormColumnConfigDO> columns) {
            this.columns = columns;
            return this;
        }

        public Builder css(String css) {
            this.css = css;
            return this;
        }

        public Builder column(FormColumnConfigDO column) {
            columns.add(column);
            return this;
        }

        public FormRowConfigDO build() {
            FormRowConfigDO config = new FormRowConfigDO();
            config.setColumns(columns);
            config.setCss(css);
            return config;
        }

    }

}
