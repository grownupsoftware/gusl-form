package gusl.form.builder;

import gusl.annotations.form.MediaType;
import gusl.annotations.form.QueryCondition;
import gusl.annotations.form.RestActionConfig;
import gusl.annotations.form.SubMenuType;

public class UiSubMenuBuilder {

    private SubMenuType type;

    private String label;

    private String code;

    private String editUrl;

    private String getUrl;

    private Class<?> esoClass;

    private Class<?> convertEsoTo;

    //index name without 'lm-'prefix
    private String indexName;

    private QueryCondition[] conditions;

    //structure = '[field-name],[direction]' where direction === ASC or DESC
    private String defaultSortString;

    private String sortTieBreaker;

    private boolean expandable;

    private String aggsUrl;

    private String elasticQueryGroup;

    private RestActionConfig[] actions;

    private RestActionConfig[] rowActions;

    private RestActionConfig[] groupActions;

    private Integer refreshRate;

    private boolean noBanner;

    private MediaType mediaType;

    private boolean noLabel;

    public UiSubMenuBuilder(
            SubMenuType type,
            String label,
            String code,
            String editUrl,
            String getUrl,
            Class<?> esoClass,
            Class<?> convertEsoTo,
            String indexName,
            QueryCondition[] conditions,
            String defaultSortString,
            String sortTieBreaker,
            boolean expandable,
            String aggsUrl,
            String elasticQueryGroup,
            RestActionConfig[] actions,
            RestActionConfig[] rowActions,
            RestActionConfig[] groupActions,
            Integer refreshRate,
            boolean noBanner,
            MediaType mediaType,
            boolean noLabel

    ) {
        this.type = type;
        this.label = label;
        this.code = code;
        this.editUrl = editUrl;
        this.getUrl = getUrl;
        this.esoClass = esoClass;
        this.convertEsoTo = convertEsoTo;
        this.indexName = indexName;
        this.conditions = conditions;
        this.defaultSortString = defaultSortString;
        this.sortTieBreaker = sortTieBreaker;
        this.expandable = expandable;
        this.aggsUrl = aggsUrl;
        this.elasticQueryGroup = elasticQueryGroup;
        this.actions = actions;
        this.rowActions = rowActions;
        this.groupActions = groupActions;
        this.refreshRate = refreshRate;
        this.noBanner = noBanner;
        this.mediaType = mediaType;
        this.noLabel = noLabel;
    }

    public SubMenuType getType() {
        return type;
    }

    public void setType(SubMenuType type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEditUrl() {
        return editUrl;
    }

    public void setEditUrl(String editUrl) {
        this.editUrl = editUrl;
    }

    public String getGetUrl() {
        return getUrl;
    }

    public void setGetUrl(String getUrl) {
        this.getUrl = getUrl;
    }

    public Class<?> getEsoClass() {
        return esoClass;
    }

    public void setEsoClass(Class<?> esoClass) {
        this.esoClass = esoClass;
    }

    public Class<?> getConvertEsoTo() {
        return convertEsoTo;
    }

    public void setConvertEsoTo(Class<?> convertEsoTo) {
        this.convertEsoTo = convertEsoTo;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public QueryCondition[] getConditions() {
        return conditions;
    }

    public void setConditions(QueryCondition[] conditions) {
        this.conditions = conditions;
    }

    public String getDefaultSortString() {
        return defaultSortString;
    }

    public void setDefaultSortString(String defaultSortString) {
        this.defaultSortString = defaultSortString;
    }

    public String getSortTieBreaker() {
        return sortTieBreaker;
    }

    public void setSortTieBreaker(String sortTieBreaker) {
        this.sortTieBreaker = sortTieBreaker;
    }

    public boolean isExpandable() {
        return expandable;
    }

    public void setExpandable(boolean expandable) {
        this.expandable = expandable;
    }

    public String getAggsUrl() {
        return aggsUrl;
    }

    public void setAggsUrl(String aggsUrl) {
        this.aggsUrl = aggsUrl;
    }

    public String getElasticQueryGroup() {
        return elasticQueryGroup;
    }

    public void setElasticQueryGroup(String elasticQueryGroup) {
        this.elasticQueryGroup = elasticQueryGroup;
    }

    public RestActionConfig[] getActions() {
        return actions;
    }

    public void setActions(RestActionConfig[] actions) {
        this.actions = actions;
    }

    public RestActionConfig[] getRowActions() {
        return rowActions;
    }

    public void setRowActions(RestActionConfig[] rowActions) {
        this.rowActions = rowActions;
    }

    public RestActionConfig[] getGroupActions() {
        return groupActions;
    }

    public void setGroupActions(RestActionConfig[] groupActions) {
        this.groupActions = groupActions;
    }

    public Integer getRefreshRate() {
        return refreshRate;
    }

    public void setRefreshRate(Integer refreshRate) {
        this.refreshRate = refreshRate;
    }

    public boolean isNoBanner() {
        return noBanner;
    }

    public void setNoBanner(boolean noBanner) {
        this.noBanner = noBanner;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public static class Builder {
        private SubMenuType type;
        private String label;
        private String code;
        private String editUrl;
        private String getUrl;
        private Class<?> esoClass;
        private Class<?> convertEsoTo;
        private String indexName;
        private QueryCondition[] conditions;
        private String defaultSortString;
        private String sortTieBreaker;
        private boolean expandable;
        private String aggsUrl;
        private String elasticQueryGroup;
        private RestActionConfig[] actions;

        private RestActionConfig[] rowActions;

        private RestActionConfig[] groupActions;

        private Integer refreshRate;

        private boolean noBanner;

        private MediaType mediaType;

        private boolean noLabel;

        public Builder(SubMenuType type) {
            this.type = type;
        }

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder type(SubMenuType type) {
            this.type = type;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder noLabel(boolean noLabel) {
            this.noLabel = noLabel;
            return this;
        }

        public Builder noBanner(boolean noBanner) {
            this.noBanner = noBanner;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder refreshRate(Integer refreshRate) {
            this.refreshRate = refreshRate;
            return this;
        }

        public Builder editUrl(String editUrl) {
            this.editUrl = editUrl;
            return this;
        }

        public Builder getUrl(String getUrl) {
            this.getUrl = getUrl;
            return this;
        }

        public Builder esoClass(Class<?> esoClass) {
            this.esoClass = esoClass;
            return this;
        }

        public Builder convertEsoTo(Class<?> convertEsoTo) {
            this.convertEsoTo = convertEsoTo;
            return this;
        }

        public Builder indexName(String indexName) {
            this.indexName = indexName;
            return this;
        }

        public Builder conditions(QueryCondition[] conditions) {
            this.conditions = conditions;
            return this;
        }

        public Builder defaultSortString(String defaultSortString) {
            this.defaultSortString = defaultSortString;
            return this;
        }

        public Builder sortTieBreaker(String sortTieBreaker) {
            this.sortTieBreaker = sortTieBreaker;
            return this;
        }

        public Builder expandable(boolean expandable) {
            this.expandable = expandable;
            return this;
        }

        public Builder aggsUrl(String aggsUrl) {
            this.aggsUrl = aggsUrl;
            return this;
        }

        public Builder elasticQueryGroup(String elasticQueryGroup) {
            this.elasticQueryGroup = elasticQueryGroup;
            return this;
        }

        public Builder actions(RestActionConfig[] actions) {
            this.actions = actions;
            return this;
        }

        public Builder rowActions(RestActionConfig[] rowActions) {
            this.rowActions = rowActions;
            return this;
        }

        public Builder groupActions(RestActionConfig[] groupActions) {
            this.groupActions = groupActions;
            return this;
        }

        public UiSubMenuBuilder build() {
            return new UiSubMenuBuilder(type, label, code, editUrl, getUrl, esoClass, convertEsoTo,
                    indexName, conditions, defaultSortString, sortTieBreaker, expandable,
                    aggsUrl, elasticQueryGroup, actions, rowActions, groupActions, refreshRate, noBanner, mediaType, noLabel);
        }

    }
}
