package gusl.form.builder;

import gusl.form.model.AddConfigDO;

public class AddBuilder {

    public static class Builder {

        private String title;

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public AddConfigDO build() {
            AddConfigDO configDo = new AddConfigDO();
            configDo.setAllowAdd(true);
            configDo.setTitle(title);

            return configDo;
        }
    }

}
