package gusl.form.builder;

import gusl.form.model.DashboardConfigDO;
import gusl.form.model.FormRowConfigDO;

import java.util.LinkedList;
import java.util.List;

public class DashboardBuilder {

    public static class Builder<T> {

        private List<FormRowConfigDO> rows = new LinkedList<>();
        private String formWidth;

        public Builder row(FormRowConfigDO row) {
            rows.add(row);
            return this;
        }

        public Builder rows(List<FormRowConfigDO> rows) {
            this.rows.addAll(rows);
            return this;
        }

        public Builder formWidth(String formWidth) {
            this.formWidth = formWidth;
            return this;
        }

        public DashboardConfigDO build() {
            DashboardConfigDO configDo = new DashboardConfigDO();
            configDo.setFormWidth(formWidth);
            configDo.setRows(rows);
            return configDo;
        }
    }
}
