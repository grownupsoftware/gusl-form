package gusl.form.builder;

import gusl.form.model.FieldConfigDO;
import gusl.form.model.FormColumnConfigDO;

import java.util.LinkedList;
import java.util.List;

public class ColumnBuilder {

    public static class Builder {

        private List<FieldConfigDO> fields;
        private String title;
        private String css;

        private Integer fsFlex;
        private boolean ignorePanel;

        private String width;
        private String height;

        public Builder() {
            fields = new LinkedList<>();
        }

        public Builder fields(List<FieldConfigDO> fields) {
            this.fields = fields;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder width(String width) {
            this.width = width;
            return this;
        }

        public Builder height(String height) {
            this.height = height;
            return this;
        }

        public Builder css(String css) {
            this.css = css;
            return this;
        }

        public Builder fsFlex(int fsFlex) {
            this.fsFlex = fsFlex;
            return this;
        }

        public Builder ignorePanel() {
            this.ignorePanel = true;
            return this;
        }

        public Builder field(FieldConfigDO field) {
            fields.add(field);
            return this;
        }

        public FormColumnConfigDO build() {
            FormColumnConfigDO config = new FormColumnConfigDO();
            config.setCss(css);
            config.setFields(fields);
            config.setIgnorePanel(ignorePanel);
            config.setTitle(title);
            config.setFxFlex(fsFlex);
            config.setWidth(width);
            config.setHeight(height);
            return config;
        }

    }
}
