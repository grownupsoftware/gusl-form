package gusl.form.builder;

public class UiMenuItemBuilder {

    private String label;
    private String code;
    private String selectOne;
    private String formTitle;

    private String idTitleTagJs;

    public UiMenuItemBuilder(String label, String code, String selectOne, String formTitle, String idTitleTagJs) {
        this.label = label;
        this.code = code;
        this.selectOne = selectOne;
        this.formTitle = formTitle;
        this.idTitleTagJs = idTitleTagJs;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSelectOne() {
        return selectOne;
    }

    public void setSelectOne(String selectOne) {
        this.selectOne = selectOne;
    }

    public String getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }

    public String getIdTitleTagJs() {
        return idTitleTagJs;
    }

    public void setIdTitleTagJs(String idTitleTagJs) {
        this.idTitleTagJs = idTitleTagJs;
    }

    public static class Builder {
        private String label;
        private String code;
        private String selectOne;
        private String formTitle;
        private String idTitleTagJs;

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder selectOne(String selectOne) {
            this.selectOne = selectOne;
            return this;
        }

        public Builder formTitle(String formTitle) {
            this.formTitle = formTitle;
            return this;
        }

        public Builder idTitleTagJs(String idTitleTagJs) {
            this.idTitleTagJs = idTitleTagJs;
            return this;
        }

        public UiMenuItemBuilder build() {
            return new UiMenuItemBuilder(label, code, selectOne, formTitle, idTitleTagJs);
        }
    }

}
