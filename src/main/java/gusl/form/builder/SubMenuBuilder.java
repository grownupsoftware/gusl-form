package gusl.form.builder;

import gusl.form.model.*;

import java.util.ArrayList;

public class SubMenuBuilder {

    public static final String ELASTIC_INDEX_PREFIX = "gusl-";

    public static class Builder {

        private String label;
        private String code;
        private String permission;
        private MaintainConfigDO maintainConfig;
        private MenuType menuType;
        private SingleFormConfigDO singleFormConfigDO;
        private SinglePageConfigDO singlePageConfigDO;
        private String getUrl;

        private boolean noLabel;

        public Builder noLabel(boolean noLabel) {
            this.noLabel = noLabel;
            return this;
        }

        public Builder noLabel() {
            this.noLabel = true;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder permission(String permission) {
            this.permission = permission;
            return this;
        }

        public Builder index(String index) {
            this.code = ELASTIC_INDEX_PREFIX + index;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder maintain(MaintainConfigDO maintainConfig) {
            this.maintainConfig = maintainConfig;
            menuType = MenuType.MAINTAIN;
            return this;
        }

        public Builder elastic(MaintainConfigDO maintainConfig) {
            this.maintainConfig = maintainConfig;
            menuType = MenuType.ELASTIC;
            return this;
        }

        public Builder entityGroup(MaintainConfigDO maintainConfig) {
            this.maintainConfig = maintainConfig;
            menuType = MenuType.ENTITY_GROUP;
            return this;
        }

        public Builder entityGroupSingleForm(SingleFormConfigDO singleFormConfigDO) {
            this.singleFormConfigDO = singleFormConfigDO;
            menuType = MenuType.ENTITY_GROUP_SINGLE_FORM;
            return this;
        }

        public Builder singlePage(SinglePageConfigDO singlePageConfigDO) {
            this.singlePageConfigDO = singlePageConfigDO;
            menuType = MenuType.SINGLE_PAGE;
            return this;
        }

        public Builder getUrl(String url) {
            this.getUrl = url;
            return this;
        }

        public MenuDO build() {

            if (this.code == null) {
                this.code = label.replace(" ", "").toLowerCase();
            }

            MenuDO menuDO = new MenuDO();
            menuDO.setLabel(label);
            menuDO.setCode(code);
            menuDO.setPermission(permission);

            EntityGroupConfigDO entityGroup = new EntityGroupConfigDO();
            entityGroup.setDataSources(new ArrayList<>());
            entityGroup.getDataSources().add(new DataSource<>(MenuType.MAINTAIN, maintainConfig));

            menuDO.setEntityGroup(entityGroup);
            menuDO.setSingleForm(singleFormConfigDO);
            menuDO.setSinglePage(singlePageConfigDO);
            menuDO.setMenuType(menuType);
            menuDO.setSelectUrl(getUrl);
            menuDO.setNoLabel(noLabel);
            return menuDO;
        }

    }
}
