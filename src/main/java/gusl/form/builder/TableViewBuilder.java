package gusl.form.builder;

import gusl.form.model.AddConfigDO;
import gusl.form.model.FieldConfigDO;
import gusl.form.model.TableActionsConfigDO;
import gusl.form.model.TableViewConfigDO;

import java.util.LinkedList;
import java.util.List;

public class TableViewBuilder {

    public static class Builder {

        private String code;
        private String title;
        private AddConfigDO add;
        private List<TableActionsConfigDO> actions = new LinkedList<>();
        private int[] rowsPerPageOptions;
        private List<FieldConfigDO> filters;
        private Boolean paged;

        private boolean disableTableControls;
        private boolean disableColumnResize;

        private String headerUrl;

        private Integer refreshRate;

        public Builder refreshRate(Integer refreshRate) {
            this.refreshRate = refreshRate;
            return this;
        }

        public Builder headerUrl(String url) {
            headerUrl = url;
            return this;
        }

        public Builder disableTransactionControls(boolean value) {
            disableTableControls = value;
            return this;
        }

        public Builder disableColumnResize(boolean value) {
            disableColumnResize = value;
            return this;
        }

        public Builder action(TableActionsConfigDO action) {
            actions.add(action);
            return this;
        }

        public Builder paged(Boolean paged) {
            this.paged = paged;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder addRecord(AddConfigDO add) {
            this.add = add;
            return this;
        }

        public Builder actions(List<TableActionsConfigDO> actions) {
            this.actions = actions;
            return this;
        }

        public Builder rowsPerPageOptions(int[] rowsPerPageOptions) {
            this.rowsPerPageOptions = rowsPerPageOptions;
            return this;
        }

        public Builder filters(List<FieldConfigDO> filters) {
            this.filters = filters;
            return this;
        }

        public TableViewConfigDO build() {
            TableViewConfigDO config = new TableViewConfigDO();
            config.setFilters(filters);
            config.setActions(actions);
            config.setAdd(add);
            config.setRowsPerPageOptions(rowsPerPageOptions);
            config.setTitle(title);
            config.setCode(code);
            config.setPaged(paged);
            config.setDisableTableControls(disableTableControls);
            config.setDisableColumnResize(disableColumnResize);
            config.setHeaderUrl(headerUrl);
            config.setRefreshRate(refreshRate);
            return config;
        }
    }

}
