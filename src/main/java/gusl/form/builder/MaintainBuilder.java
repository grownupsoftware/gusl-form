package gusl.form.builder;

import gusl.annotations.form.MediaType;
import gusl.annotations.form.page.OrderByActionDO;
import gusl.form.model.*;

import java.util.LinkedList;
import java.util.List;

public class MaintainBuilder {

    public static class Builder {

        private List<MaintainTableTab> tabs = new LinkedList<>();
        private List<FormRowConfigDO> rows = new LinkedList<>();

        private List<ActionConfigDO> actionsDTOs;

        private List<ActionConfigDO> rowActions;

        private List<ActionConfigDO> groupActions;

        protected List<OrderByActionDO> orderByActions;


        private MediaType mediaType;

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder row(FormRowConfigDO row) {
            rows.add(row);
            return this;
        }

        public Builder rows(List<FormRowConfigDO> rows) {
            this.rows.addAll(rows);
            return this;
        }

        public Builder tabs(List<MaintainTableTab> tabs) {
            this.tabs.addAll(tabs);
            return this;
        }

        public Builder actions(List<ActionConfigDO> actions) {
            actionsDTOs = actions;
            return this;
        }

        public Builder rowActions(List<ActionConfigDO> rowActions) {
            this.rowActions = rowActions;
            return this;
        }

        public Builder groupActions(List<ActionConfigDO> groupActions) {
            this.groupActions = groupActions;
            return this;
        }
        public Builder orderByActions(List<OrderByActionDO> orderByActions) {
            this.orderByActions = orderByActions;
            return this;
        }

        public MaintainConfigDO build() {
            MaintainConfigDO configDo = new MaintainConfigDO();
            configDo.setRows(rows);
            configDo.setTabs(tabs);
            configDo.setOverallRestActions(actionsDTOs);
            configDo.setRowActions(rowActions);
            configDo.setGroupActions(groupActions);
            configDo.setOrderByActions(orderByActions);
            configDo.setMediaType(mediaType);
            return configDo;
        }
    }
}
