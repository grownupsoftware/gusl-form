package gusl.form.builder.lookup;

import gusl.annotations.form.LookupConfig;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LookupBuilder {

    @Singular
    @ToStringCount
    private List<LookupConfig> lookups;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
