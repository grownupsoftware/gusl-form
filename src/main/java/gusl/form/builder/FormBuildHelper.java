package gusl.form.builder;

import gusl.annotations.form.UiField;
import gusl.annotations.form.UiPosition;
import gusl.annotations.form.UiSubMenu;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.form.model.*;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static gusl.form.builder.FormBuilder.createField;
import static gusl.form.builder.FormBuilder.getRowConfig;
import static java.util.Collections.singletonList;
import static java.util.Objects.isNull;

public class FormBuildHelper {
    protected final static GUSLLogger logger = GUSLLogManager.getLogger(FormBuildHelper.class);

    public static EndPointDO getUpdateUrl(String url) {
        return Optional.of(url)
                .filter(s -> !StringUtils.isEmpty(s))
                .map(s -> createEndpointDTO(url, EndPointType.PUT))
                .orElse(null);
    }

    public static EndPointDO createEndpointDTO(String url, EndPointType method) {
        EndPointDO endPointDO = new EndPointDO();
        endPointDO.setMethod(method);
        endPointDO.setUrl(url);
        return endPointDO;
    }

    public static List<FormColumnConfigDO> getCols(Class clazz) {
        List<FormColumnConfigDO> columns = getFieldsFor(clazz).stream().map(nestedField -> {
                    FormColumnConfigDO currentColumn = new FormColumnConfigDO(new ArrayList<>());
                    currentColumn.setCode(nestedField.getName());
                    currentColumn.setFxFlex(getFxFlex(nestedField));
                    currentColumn.setIgnorePanel(isIgnored(nestedField));
                    currentColumn.setMultipartForm(isMultipartForm(nestedField));
                    currentColumn.setWidth(getWidth(nestedField));
                    currentColumn.setHeight(getHeight(nestedField));

                    getTitle(nestedField).ifPresent(currentColumn::setTitle);
                    getUpdateUrlEndpoint(nestedField).ifPresent(currentColumn::setUpdateUrl);
                    getDeleteUrlEndpoint(nestedField).ifPresent(currentColumn::setDeleteUrl);
                    getJsEditCondition(nestedField).ifPresent(currentColumn::setJsEditCondition);
                    getActions(nestedField).ifPresent(currentColumn::setActions);

                    boolean nested = isNested(nestedField);
                    currentColumn.setNested(nested);
                    if (nested) {
                        currentColumn.getFields().addAll(getRowConfig(nestedField.getType()).getColumns().get(0).getFields());
                    } else {
                        createField(nestedField).ifPresent(config -> currentColumn.getFields().add(config));
                    }
                    boolean noLabel = isNoLabel(nestedField);
                    currentColumn.setNoLabel(noLabel);
                    return currentColumn;
                }
        ).collect(Collectors.toList());

        return columns;
    }

    public static List<FormColumnConfigDO> getColsForSimpleFieldsDefClass(Class clazz) {
        FormColumnConfigDO currentColumn = new FormColumnConfigDO();
        currentColumn.setActions(new ArrayList<>());
        currentColumn.setFields(new ArrayList<>());

        getFieldsFor(clazz).forEach(nestedField -> {
            boolean nested = isNested(nestedField);
            currentColumn.setNested(nested);
            if (nested) {
                currentColumn.getFields().addAll(getRowConfig(nestedField.getType()).getColumns().get(0).getFields());
            } else {
                createField(nestedField).ifPresent(config -> currentColumn.getFields().add(config));
            }
            currentColumn.setFxFlex(getFxFlex(nestedField));
            currentColumn.setWidth(getWidth(nestedField));
            currentColumn.setHeight(getHeight(nestedField));

            getUpdateUrlEndpoint(nestedField).ifPresent(currentColumn::setUpdateUrl);
            getDeleteUrlEndpoint(nestedField).ifPresent(currentColumn::setDeleteUrl);
            getJsEditCondition(nestedField).ifPresent(currentColumn::setJsEditCondition);
            getActions(nestedField).ifPresent(currentColumn::setActions);

        });

        return singletonList(currentColumn);
    }

    public static List<FormRowConfigDO> getRowsForCardDefClass(Class clazz) {
        return singletonList(new FormRowConfigDO(getCols(clazz)));
    }

    public static List<FieldConfigDO> getFieldsForClass(Class clazz) {
        if (isNull(clazz)) {
            return null;
        }
        return getRowConfig(clazz).getColumns().get(0).getFields();
    }

    public static List<FormRowConfigDO> getRowsForSimpleFieldsDefClass(Class clazz) {
        return singletonList(new FormRowConfigDO(getColsForSimpleFieldsDefClass(clazz)));
    }

    public static boolean isIgnored(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::ignoreIfNotPresent)
                .orElse(false);
    }

    public static boolean isMultipartForm(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::multipartForm)
                .orElse(false);
    }

    public static boolean isNested(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::nested)
                .orElse(false);
    }

    public static boolean isNoLabel(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::noLabel)
                .orElse(false);
    }

    public static Optional<String> getUpdateUrlEndpoint(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::updateUrl)
                .filter(StringUtils::isNotBlank);
    }

    public static Optional<String> getDeleteUrlEndpoint(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::deleteUrl)
                .filter(StringUtils::isNotBlank);
    }

    public static Optional<List<ActionConfigDO>> getActions(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class))
                .map(UiPosition::actions)
                .filter(a -> a.length > 0)
                .map(Arrays::stream)
                .map(list -> list.map(FormBuilder::createAdminActionConfigFromAnnotation).collect(Collectors.toList()));
    }

    public static Optional<String> getTitle(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::header).filter(StringUtils::isNotBlank);
    }

    public static Optional<String> getJsEditCondition(Field nestedField) {
        return Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::jsEditCondition).filter(StringUtils::isNotBlank);
    }

    public static Integer getFxFlex(Field nestedField) {
        Integer fxFlex = Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::fxFlex).orElse(1);
        if (isNull(fxFlex) || fxFlex == 1 || fxFlex == -1) {
            fxFlex = Optional.ofNullable(nestedField.getAnnotation(UiField.class)).map(UiField::fxFlex).orElse(1);
        }
        return fxFlex;
    }

    public static String getWidth(Field nestedField) {
        String width = Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::width).orElse(null);
        return width;
    }

    public static String getHeight(Field nestedField) {
        String height = Optional.ofNullable(nestedField.getAnnotation(UiPosition.class)).map(UiPosition::height).orElse(null);
        return height;
    }

    public static EndPointDO getUpdateUrlForMenu(UiSubMenu subMenu) {
        return Optional.of(subMenu.editUrl())
                .filter(s -> !StringUtils.isEmpty(s))
                .map(s -> createEndpointDTO(subMenu.label(), EndPointType.PUT))
                .orElse(null);
    }

    public static List<Field> getFieldsFor(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        fields.addAll(Arrays.asList(clazz.getDeclaredFields()));

        Class<?> parentClass = clazz.getSuperclass();

        while (parentClass != null) {
            fields.addAll(0, Arrays.asList(parentClass.getDeclaredFields()));
            parentClass = parentClass.getSuperclass();
        }

        return fields;
    }

}
