package gusl.form.builder;

import gusl.annotations.form.MediaType;
import gusl.form.model.ActionConfigDO;
import gusl.form.model.MenuDO;
import gusl.form.model.MenuGroupDO;
import lombok.CustomLog;

import java.util.LinkedList;
import java.util.List;

import static java.util.Objects.isNull;

@CustomLog
public class MenuGroupBuilder {

    public static class Builder {

        private String label;
        private List<MenuDO> menuItems = new LinkedList<>();
        private Integer displayOrder;
        private String icon;
        private boolean alwaysOpen;
        private boolean iconOnly;
        private String permission;
        private String code;
        private String route;
        private String command;
        private String align;

        private boolean footer;

        private boolean overlay;

        private ActionConfigDO action;

        private Boolean showInSideBar;

        private MediaType mediaType;

        private Boolean mobileTopSticky;

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder showInSideBar() {
            this.showInSideBar = true;
            return this;
        }

        public Builder mobileTopSticky() {
            this.mobileTopSticky = true;
            return this;
        }

        public Builder hideInSideBar() {
            this.showInSideBar = false;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder align(String align) {
            this.align = align;
            return this;
        }

        public Builder permission(String permission) {
            this.permission = permission;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder route(String route) {
            this.route = route;
            return this;
        }

        public Builder action(ActionConfigDO action) {
            this.action = action;
            return this;
        }

        public Builder menuItems(List<MenuDO> menuItems) {
            this.menuItems.addAll(menuItems);
            return this;
        }

        public Builder displayOrder(Integer displayOrder) {
            this.displayOrder = displayOrder;
            return this;
        }

        public Builder icon(String icon) {
            this.icon = icon;
            return this;
        }

        public Builder alwaysOpen() {
            this.alwaysOpen = true;
            return this;
        }

        public Builder iconOnly() {
            this.iconOnly = true;
            return this;
        }

        public Builder menu(MenuDO menu) {
            if (menu != null) {
                this.menuItems.add(menu);
            }
            return this;
        }

        public Builder command(String command) {
            this.command = command;
            return this;
        }

        public Builder footer() {
            this.footer = true;
            return this;
        }

        public Builder overlay() {
            this.overlay = true;
            return this;
        }

        public MenuGroupDO build() {
            // if (!menuItems.isEmpty()) {
            return MenuGroupDO.builder()
                    .displayOrder(displayOrder)
                    .label(label)
                    .menuItems(menuItems)
                    .alwaysOpen(alwaysOpen)
                    .iconOnly(iconOnly)
                    .icon(icon)
                    .route(route)
                    .permission(permission)
                    .code(code)
                    .command(command)
                    .align(align)
                    .showInSideBar(isNull(showInSideBar) ? true : showInSideBar)
                    .mediaType(mediaType)
                    .action(action)
                    .footer(footer)
                    .overlay(overlay)
                    .mobileTopSticky(mobileTopSticky)
                    .build();
//            }
//            return null;
        }
    }

}
