package gusl.form.builder;

import gusl.form.model.EndpointsConfigDO;

public class EndpointsBuilder {

    public static class Builder {

        private String selectAllUrl;
        private String selectOneUrl;
        private String insertUrl;
        private String updateUrl;
        private String deleteUrl;

        public Builder baseUrl(String url, String keyName) {
            this.selectOneUrl = "/" + url + "/{" + keyName + "}";
            this.selectAllUrl = "/" + url;
            this.insertUrl = "/" + url;
            this.updateUrl = "/" + url + "/{" + keyName + "}";
            this.deleteUrl = "/" + url + "/{" + keyName + "}";
            return this;

        }

        public Builder selectAllUrl(String selectAllUrl) {
            this.selectAllUrl = selectAllUrl;
            return this;
        }

        public Builder selectOneUrl(String selectOneUrl) {
            this.selectOneUrl = selectOneUrl;
            return this;
        }

        public Builder insertUrl(String insertUrl) {
            this.insertUrl = insertUrl;
            return this;
        }

        public Builder updateUrl(String updateUrl) {
            this.updateUrl = updateUrl;
            return this;
        }

        public Builder deleteUrl(String deleteUrl) {
            this.deleteUrl = deleteUrl;
            return this;
        }

        public EndpointsConfigDO build() {
            EndpointsConfigDO configDo = new EndpointsConfigDO();
            configDo.setDeleteUrl(deleteUrl);
            configDo.setInsertUrl(insertUrl);
            configDo.setSelectAllUrl(selectAllUrl);
            configDo.setSelectOneUrl(selectOneUrl);
            configDo.setUpdateUrl(updateUrl);
            return configDo;
        }
    }

}
