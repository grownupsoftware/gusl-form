package gusl.form.builder;

import gusl.form.model.ElasticConfigDO;
import gusl.form.model.FormRowConfigDO;

import java.util.LinkedList;
import java.util.List;

public class ElasticBuilder {

    public static class Builder<T> {

        private List<FormRowConfigDO> rows = new LinkedList<>();
        private boolean withTimestamp;
        private String convertTo;
        private String indexClass;

        private Integer refreshRate;

        public Builder row(FormRowConfigDO row) {
            rows.add(row);
            return this;
        }

        public Builder rows(List<FormRowConfigDO> rows) {
            this.rows.addAll(rows);
            return this;
        }

        public Builder withTimestamp(boolean addTimestamp) {
            this.withTimestamp = addTimestamp;
            return this;
        }

        public Builder convertTo(String convertTo) {
            this.convertTo = convertTo;
            return this;
        }

        public Builder indexClass(String indexClass) {
            this.indexClass = indexClass;
            return this;
        }

        public Builder refreshRate(Integer refreshRate) {
            this.refreshRate = refreshRate;
            return this;
        }

        public ElasticConfigDO build() {
            ElasticConfigDO configDo = new ElasticConfigDO();
            configDo.setRows(rows);
            configDo.setWithTimestamp(withTimestamp);
            configDo.setConvertTo(convertTo);
            configDo.setIndexClass(indexClass);
            configDo.setRefreshRate(refreshRate);
            return configDo;
        }
    }
}
