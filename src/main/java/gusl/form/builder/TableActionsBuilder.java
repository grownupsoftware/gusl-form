package gusl.form.builder;

import gusl.form.model.TableActionsConfigDO;

public class TableActionsBuilder {

    public static class Builder {

        private String tag;
        private String pageTitle;
        private boolean overrideHandle;
        private String cssIcon;
        private String tooltip;
        private String conditional;

        public Builder tag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder pageTitle(String pageTitle) {
            this.pageTitle = pageTitle;
            return this;
        }

        public Builder overrideHandle() {
            this.overrideHandle = true;
            return this;
        }

        public Builder cssIcon(String cssIcon) {
            this.cssIcon = cssIcon;
            return this;
        }

        public Builder tooltip(String tooltip) {
            this.tooltip = tooltip;
            return this;
        }

        public Builder conditional(String conditional) {
            this.conditional = conditional;
            return this;
        }

        public TableActionsConfigDO build() {
            TableActionsConfigDO configDo = new TableActionsConfigDO();
            configDo.setConditional(conditional);
            configDo.setCssIcon(cssIcon);
            configDo.setOverrideHandle(overrideHandle);
            configDo.setPageTitle(pageTitle);
            configDo.setTag(tag);
            configDo.setTooltip(tooltip);

            return configDo;
        }
    }

}
