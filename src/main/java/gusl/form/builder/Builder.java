package gusl.form.builder;

import gusl.annotations.form.SubMenuType;
import gusl.form.builder.builders.*;

public class Builder {

    public static FieldBuilder.Builder field() {
        return new FieldBuilder.Builder();
    }

    public static ColumnBuilder.Builder column() {
        return new ColumnBuilder.Builder();
    }

    public static RowBuilder.Builder row() {
        return new RowBuilder.Builder();
    }

    public static MaintainBuilder.Builder maintain() {
        return new MaintainBuilder.Builder();
    }

    public static EntityMaintain.Builder entity(Class dtoClass) {
        return new EntityMaintain.Builder(dtoClass);
    }

    public static ElasticBuilder.Builder elastic() {
        return new ElasticBuilder.Builder();
    }

    public static CassandraBuilder.Builder cassandra() {
        return new CassandraBuilder.Builder();
    }

    public static DataTableBuilder.Builder dataTable() {
        return new DataTableBuilder.Builder();
    }

    public static DataTableBuilder.Builder alphaList() {
        final DataTableBuilder.Builder builder = new DataTableBuilder.Builder();
        builder.alphaList();
        return builder;
    }

    public static DataTableBuilder.Builder accordionList() {
        final DataTableBuilder.Builder builder = new DataTableBuilder.Builder();
        builder.accordionList();
        return builder;
    }

    public static DataTableBuilder.Builder flexList() {
        final DataTableBuilder.Builder builder = new DataTableBuilder.Builder();
        builder.flexList();
        return builder;
    }


    public static DashboardBuilder.Builder dashboard() {
        return new DashboardBuilder.Builder();
    }

    public static EndpointsBuilder.Builder endpoints() {
        return new EndpointsBuilder.Builder();
    }

    public static TableViewBuilder.Builder tableView() {
        return new TableViewBuilder.Builder();
    }

    public static TableActionsBuilder.Builder action() {
        return new TableActionsBuilder.Builder();
    }

    public static AddBuilder.Builder addRecord() {
        return new AddBuilder.Builder();
    }

    public static MenuGroupBuilder.Builder menuGroup() {
        return new MenuGroupBuilder.Builder();
    }

    public static MenuBuilder.Builder menuItem() {
        return new MenuBuilder.Builder();
    }

    public static SubMenuBuilder.Builder subMenuItem() {
        return new SubMenuBuilder.Builder();
    }

    public static SubMenuBuilder.Builder tab() {
        return new SubMenuBuilder.Builder();
    }

    public static UiMenuItemBuilder.Builder uiMenuItem() {
        return new UiMenuItemBuilder.Builder();
    }

    public static UiSubMenuBuilder.Builder uiSubMenu(SubMenuType type) {
        return new UiSubMenuBuilder.Builder(type);
    }

    public static EntityGroup.Builder entityGroup(Class dtoClass) {
        return new EntityGroup.Builder(dtoClass);
    }

    public static EntityGroup.Builder entityGroup() {
        return new EntityGroup.Builder();
    }

    public static EntityGroupSingleForm.Builder form() {
        final EntityGroupSingleForm.Builder builder = new EntityGroupSingleForm.Builder();
        builder.simple(true);
        return builder;
    }

//    public static EntityGroupSingleForm.Builder query() {
//        return new EntityGroupSingleForm.Builder();
//    }

}
