package gusl.form.builder;

import gusl.annotations.form.MediaType;
import gusl.form.model.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.nonNull;

public class MenuBuilder {

    public static class Builder {

        private String label;
        private String code;
        private String help;
        private String permission;
        private MaintainConfigDO maintainConfig;
        private MenuType menuType;
        private EntityGroupConfigDO entityGroupConfigDO;
        private TickerConfigDO tickerConfigDO;
        private ElasticConfigDO elasticConfig;
        private CassandraConfigDO cassandraConfig;

        private DataTableConfigDO dataTableConfig;

        private BlastCollectionConfigDO blastCollectionConfig;
        private boolean showInSideBar = false;
        private boolean footer = false;
        private boolean overlay = false;

        private boolean mobileTopSticky;

        private SingleFormConfigDO singleForm;
        private SinglePageConfigDO singlePage;

        private CreateFormConfigDO createForm;

        private String icon;

        private boolean iconOnly;

        private String route;

        private String command;

        private Integer displayOrder;

        private ActionConfigDO action;

        private String headerUrl;
        private String externalUrl;
        private String component;
        private String selectUrl;

        private MediaType mediaType;

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder component(String component) {
            this.component = component;
            return this;
        }

        public Builder selectUrl(String url) {
            selectUrl = url;
            return this;
        }

        public Builder action(ActionConfigDO action) {
            this.action = action;
            return this;
        }

        public Builder displayOrder(Integer displayOrder) {
            this.displayOrder = displayOrder;
            return this;
        }

        public Builder command(String command) {
            this.command = command;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder help(String help) {
            this.help = help;
            return this;
        }

        public Builder code(String code) {
            this.code = code;
            return this;
        }

        public Builder permission(String permission) {
            this.permission = permission;
            return this;
        }

        public Builder maintain(MaintainConfigDO maintainConfig) {
            this.maintainConfig = maintainConfig;
            menuType = MenuType.MAINTAIN;
            return this;
        }

        public Builder elastic(ElasticConfigDO elasticConfig) {
            this.elasticConfig = elasticConfig;
            menuType = MenuType.ELASTIC;
            return this;
        }

        public Builder cassandra(CassandraConfigDO cassandraConfig) {
            this.cassandraConfig = cassandraConfig;
            menuType = MenuType.CASSANDRA;
            return this;
        }

        public Builder dataTable(DataTableConfigDO dataTableConfig) {
            this.dataTableConfig = dataTableConfig;
            menuType = MenuType.DATA_TABLE;
            return this;
        }

        public Builder blastCollectionTable(BlastCollectionConfigDO config) {
            this.blastCollectionConfig = config;
            menuType = MenuType.BLAST_COLLECTION_TABLE;
            return this;
        }

        public Builder blastCollectionForm(BlastCollectionConfigDO config) {
            this.blastCollectionConfig = config;
            menuType = MenuType.BLAST_COLLECTION_FORM;
            return this;
        }

        public Builder singlePage(SinglePageConfigDO singlePageConfigDO) {
            this.singlePage = singlePageConfigDO;
            menuType = MenuType.SINGLE_PAGE;
            return this;
        }

        public Builder simpleForm(SingleFormConfigDO singleFormConfigDO) {
            this.singleForm = singleFormConfigDO;
            menuType = MenuType.ENTITY_GROUP_SINGLE_FORM;
            return this;
        }

        public Builder createForm(CreateFormConfigDO createForm) {
            this.createForm = createForm;
            menuType = MenuType.ENTITY_GROUP_SINGLE_FORM;
            return this;
        }

        public Builder simpleQuery(SingleFormConfigDO singleFormConfigDO) {
            this.singleForm = singleFormConfigDO;
            menuType = MenuType.ENTITY_GROUP_SINGLE_FORM;
            return this;
        }

        public Builder simpleQuery(EntityGroupConfigDO entityGroupConfigDO) {
            this.entityGroupConfigDO = entityGroupConfigDO;
            menuType = MenuType.ENTITY_GROUP_SINGLE_FORM;
            return this;
        }

        public Builder entityGroup(EntityGroupConfigDO entityGroupConfigDO) {
            this.entityGroupConfigDO = entityGroupConfigDO;
            menuType = entityGroupConfigDO.getSelectTableType() == MenuType.ENTITY_GROUP_SINGLE_FORM ? MenuType.ENTITY_GROUP_SINGLE_FORM : MenuType.ENTITY_GROUP;
            return this;
        }

        public Builder ticker(TickerConfigDO ticker) {
            this.tickerConfigDO = ticker;
            menuType = MenuType.TICKER;
            return this;
        }

        public Builder icon(String icon) {
            this.icon = icon;
            return this;
        }

        public Builder iconOnly() {
            this.iconOnly = true;
            return this;
        }

        public Builder route(String route) {
            this.route = route;
            return this;
        }

        public Builder headerUrl(String url) {
            headerUrl = url;
            return this;
        }

        public Builder externalUrl(String url) {
            externalUrl = url;
            return this;
        }

        public MenuDO build() {

            if (this.code == null) {
                this.code = label.replace(" ", "_").toLowerCase();
            }

            MenuDO menuDO = MenuDO.builder()
                    .label(label)
                    .code(code)
                    .help(help)
                    .permission(permission)
                    .menuType(menuType)
                    .icon(icon)
                    .iconOnly(iconOnly)
                    .route(route)
                    .command(command)
                    .displayOrder(displayOrder)
                    .action(action)
                    .headerUrl(headerUrl)
                    .externalUrl(externalUrl)
                    .selectUrl(selectUrl)
                    .component(component)
                    .mediaType(mediaType)
                    .mobileTopSticky(mobileTopSticky)
                    .build();
            if (Objects.isNull(this.entityGroupConfigDO)) {
                EntityGroupConfigDO entityGroupConfigDO = new EntityGroupConfigDO();
                entityGroupConfigDO.setDataSources(new ArrayList<>());

                Optional.ofNullable(maintainConfig)
                        .ifPresent(mc -> entityGroupConfigDO.getDataSources().add(new DataSource<>(MenuType.MAINTAIN, mc)));

                Optional.ofNullable(elasticConfig)
                        .ifPresent(ec -> entityGroupConfigDO.getDataSources().add(new DataSource<>(MenuType.ELASTIC, ec)));

                Optional.ofNullable(tickerConfigDO)
                        .ifPresent(tc -> entityGroupConfigDO.getDataSources().add(new DataSource<>(MenuType.TICKER, tc)));

                Optional.ofNullable(cassandraConfig)
                        .ifPresent(tc -> entityGroupConfigDO.getDataSources().add(new DataSource<>(MenuType.CASSANDRA, tc)));

                Optional.ofNullable(dataTableConfig)
                        .ifPresent(tc -> entityGroupConfigDO.getDataSources().add(new DataSource<>(MenuType.DATA_TABLE, tc)));

                menuDO.setEntityGroup(entityGroupConfigDO);
            } else {
                menuDO.setEntityGroup(this.entityGroupConfigDO);
            }

            if (nonNull(this.singleForm)) {
                menuDO.setSingleForm(this.singleForm);
            }

            if (nonNull(this.createForm)) {
                menuDO.setCreateForm(this.createForm);
            }

            if (nonNull(this.singlePage)) {
                menuDO.setSinglePage(this.singlePage);
            }

            menuDO.setShowInSideBar(showInSideBar);
            menuDO.setFooter(this.footer);
            menuDO.setOverlay(this.overlay);
            return menuDO;
        }

        public Builder showInSideBar(boolean showInSideBar) {
            this.showInSideBar = showInSideBar;
            return this;
        }

        public Builder footer(boolean footer) {
            this.footer = footer;
            return this;
        }
        public Builder overlay(boolean overlay) {
            this.overlay = overlay;
            return this;
        }

        public Builder mobileTopSticky() {
            this.mobileTopSticky = true;
            return this;
        }

    }
}
