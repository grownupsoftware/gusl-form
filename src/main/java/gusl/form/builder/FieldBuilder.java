package gusl.form.builder;

import gusl.annotations.form.EnumFilter;
import gusl.annotations.form.LookupType;
import gusl.annotations.form.MediaType;
import gusl.core.annotations.DocField;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.form.model.FieldConfigDO;
import gusl.form.model.FormRowConfigDO;
import gusl.form.model.FormValidator;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class FieldBuilder {

    protected final static GUSLLogger logger = GUSLLogManager.getLogger(FieldBuilder.class);

    public static class Builder {

        private boolean formFullWidth;

        @DocField(description = "Name of the field")
        private String name;

        private String permission;

        @DocField(description = "?")
        private String child;

        @DocField(description = "The display label")
        private String label;

        @DocField(description = "The type of field")
        private String type;

        @DocField(description = "For maintain, display in table")
        private boolean displayInTable;

        @DocField(description = "Option_list show in field when selected")
        private boolean displayWhenSelected;

        @DocField(description = "For maintain, allow filtering")
        private boolean filterInTable;

        @DocField(description = "For maintain, allow advance filtering")
        private boolean advancedFilter;

        @DocField(description = "Ignore field when adding a record")
        private boolean addIgnore;

        @DocField(description = "Hide this field during edit mode")
        private boolean editHide;

        @DocField(description = "Hide this field during delete mode")
        private boolean deleteHide;

        @DocField(description = "In edit mode, field is only read only")
        private String editReadOnly;

        @DocField(description = "In add mode, field is read only")
        private boolean addReadOnly;

        @DocField(description = "Form entry place holders")
        private String placeholder;

        @DocField(description = "Form validators")
        private List<FormValidator> validators;

        @DocField(description = "css prefix")
        private String cssClassPrefix;

        @DocField(description = "for 'option' the list of options")
        private String[] options;

        @DocField(description = "display label")
        private boolean noLabel;

        @DocField(description = "type of lookup")
        private String lookup;

        @DocField(description = "the money format")
        private String moneyFormat;

        @DocField(description = "category for lookups")
        private String category;

        @DocField(description = "css for label field")
        private String labelCss;

        private String colCss;

        @DocField(description = "css for entry field")
        private String entryCss;

        @DocField(description = "lookup argument")
        private String lookupArg;

        @DocField(description = "property name")
        private String propertyName;

        @DocField(description = "format for date")
        private String dateFormat;

        @DocField(description = "number of decimal places for formatting")
        private String decimalPlaces;

        @DocField(description = "For panel, form confiuration")
        private FormRowConfigDO rowConfig;

        private String selectUrl;

        private String align;

        private boolean sortable;

        private String sortableField;

        private Boolean clipboard;

        private String filterElasticName;
        private boolean viewHide;
        private String lookupLayout;
        private String insertUrl;
        private String newTableElementUrl;
        private String expandablePanel;
        private boolean strippedRows;
        private boolean hideIfEmpty;

        private String properties;

        private String panelOptionsUrl;
        private String lookupWatcherFieldName;
        private String lookupWatcherFieldObjectId;
        private String lookupCollection;
        private String lookupSearchUrl;
        private String lookupSelectUrl;

        @DocField(description = "Matching field in row data are overwritten")
        private boolean lookupOverwritesForm;

        private String onBefore;
        private String onAfter;

        private Class optionListClass;

        private int displayOrder;

        private MediaType mediaType;

        private String blastDeltaCommand;

        private String keyField;

        public Builder blastDeltaCommand(String blastDeltaCommand) {
            this.blastDeltaCommand = blastDeltaCommand;
            return this;
        }

        public Builder keyField(String keyField) {
            this.keyField = keyField;
            return this;
        }

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder displayOrder(int displayOrder) {
            this.displayOrder = displayOrder;
            return this;
        }

        public Builder onBefore(String onBefore) {
            this.onBefore = onBefore;
            return this;
        }

        public Builder onAfter(String onAfter) {
            this.onAfter = onAfter;
            return this;
        }

        public Builder lookupCollection(String lookupCollection) {
            this.lookupCollection = lookupCollection;
            return this;
        }

        public Builder lookupSearchUrl(String lookupSearchUrl) {
            this.lookupSearchUrl = lookupSearchUrl;
            return this;
        }

        public Builder lookupSelectUrl(String lookupSelectUrl) {
            this.lookupSelectUrl = lookupSelectUrl;
            return this;
        }

        public Builder lookupOverwritesForm(boolean lookupOverwritesForm) {
            this.lookupOverwritesForm = lookupOverwritesForm;
            return this;
        }

        public Builder optionListClass(Class optionListClass) {
            this.optionListClass = optionListClass;
            return this;
        }

        public Builder lookupWatcherFieldName(String lookupWatcherFieldName) {
            this.lookupWatcherFieldName = lookupWatcherFieldName;
            return this;
        }

        public Builder lookupWatcherFieldObjectId(String lookupWatcherFieldObjectId) {
            this.lookupWatcherFieldObjectId = lookupWatcherFieldObjectId;
            return this;
        }

        public Builder properties(String properties) {
            this.properties = properties;
            return this;
        }

        public Builder panelOptionsUrl(String panelOptionsUrl) {
            this.panelOptionsUrl = panelOptionsUrl;
            return this;
        }

        public Builder filterElasticName(String filterElasticName) {
            this.filterElasticName = filterElasticName;
            return this;
        }

        public Builder selectUrl(String selectUrl) {
            this.selectUrl = selectUrl;
            return this;
        }

        public Builder formFullWidth(Boolean formFullWidth) {
            this.formFullWidth = formFullWidth;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder permission(String permission) {
            this.permission = permission;
            return this;
        }

        public Builder align(String align) {
            this.align = align;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder child(String child) {
            this.child = child;
            return this;
        }

        public Builder displayWhenSelected() {
            this.displayWhenSelected = true;
            return this;
        }

        public Builder displayInTable() {
            this.displayInTable = true;
            return this;
        }

        public Builder filterInTable() {
            this.filterInTable = true;
            return this;
        }

        public Builder advancedFilter() {
            this.advancedFilter = true;
            return this;
        }

        public Builder addIgnore() {
            this.addIgnore = true;
            return this;
        }

        public Builder editHide() {
            this.editHide = true;
            return this;
        }

        public Builder deleteHide() {
            this.deleteHide = true;
            return this;
        }

        public Builder editReadOnly(String value) {
            this.editReadOnly = value;
            return this;
        }

        public Builder addReadOnly() {
            this.addReadOnly = true;
            return this;
        }

        public Builder placeholder(String placeholder) {
            this.placeholder = placeholder;
            return this;
        }

        public Builder validators(List<FormValidator> validators) {
            this.validators = validators;
            return this;
        }

        public Builder cssClassPrefix(String cssClassPrefix) {
            this.cssClassPrefix = cssClassPrefix;
            return this;
        }

        public Builder options(String[] options) {
            this.options = options;
            return this;
        }

        public Builder optionsFromEnumFilter(Class e) {
            if (isNull(e)) {
                this.options = new String[0];
            } else {
                if (EnumFilter.class.isAssignableFrom(e)) {
                    try {
                        Method mth = e.getDeclaredMethod("getFilteredForBackOffice");
                        if (nonNull(mth)) {
                            this.options = (String[]) mth.invoke(null);
                        } else {
                            this.options = new String[0];
                        }
                    } catch (Throwable t) {
                        logger.info("Failed to filter enums using standard", t);
                        this.options = new String[0];
                    }
                }
            }
            return this;
        }

        public Builder options(Class<? extends Enum<?>> e) {
            try {

                if (EnumFilter.class.isAssignableFrom(e)) {
                    try {
                        Method mth = e.getDeclaredMethod("getFilteredForBackOffice");
                        if (nonNull(mth)) {
                            this.options = (String[]) mth.invoke(null);
                        } else {
                            this.options = Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
                        }
                    } catch (Throwable t) {
                        logger.info("Failed to filter enums using standard", t);
                        this.options = Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
                    }
                } else {
                    this.options = Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
                }
            } catch (Throwable t) {
                logger.error("Failed to create enum  options for class: {} ", e, t);
            }

            return this;
        }

        public Builder noLabel() {
            this.noLabel = true;
            return this;
        }

        public Builder lookup(LookupType lookup) {
            this.lookup = lookup.name();
            return this;
        }

        public Builder lookupCode(String lookupCode) {
            this.lookup = lookupCode;
            return this;
        }

        public Builder moneyFormat(String moneyFormat) {
            this.moneyFormat = moneyFormat;
            return this;
        }

        public Builder category(String category) {
            this.category = category;
            return this;
        }

        public Builder labelCss(String labelCss) {
            this.labelCss = labelCss;
            return this;
        }

        public Builder colCss(String colCss) {
            this.colCss = colCss;
            return this;
        }

        public Builder entryCss(String entryCss) {
            this.entryCss = entryCss;
            return this;
        }

        public Builder lookupArg(String lookupArg) {
            this.lookupArg = lookupArg;
            return this;
        }

        public Builder propertyName(String propertyName) {
            this.propertyName = propertyName;
            return this;
        }

        public Builder dateFormat(String dateFormat) {
            this.dateFormat = dateFormat;
            return this;
        }

        public Builder decimalPlaces(String decimalPlaces) {
            this.decimalPlaces = decimalPlaces;
            return this;
        }

        public Builder rowConfig(FormRowConfigDO rowConfig) {
            this.rowConfig = rowConfig;
            return this;
        }

        public Builder sortable(boolean sortable) {
            this.sortable = sortable;
            return this;
        }

        public Builder sortableField(String sortableField) {
            this.sortableField = sortableField;
            return this;
        }

        public Builder clipboard(boolean clipboard) {
            this.clipboard = clipboard;
            return this;
        }

        public FieldConfigDO build() {
            FieldConfigDO config = new FieldConfigDO();
            config.setPermission(permission);
            config.setAddIgnore(addIgnore);
            config.setChild(child);
            config.setAddReadOnly(addReadOnly);
            config.setAdvancedFilter(advancedFilter);
            config.setCategory(category);
            config.setCssClassPrefix(cssClassPrefix);
            config.setDateFormat(dateFormat);
            config.setDecimalPlaces(decimalPlaces);
            config.setDisplayInTable(displayInTable);
            config.setDisplayWhenSelected(displayWhenSelected);
            config.setEditHide(editHide);
            config.setDeleteHide(deleteHide);
            config.setEditReadOnly(editReadOnly);
            config.setEntryCss(entryCss);
            config.setFilterInTable(filterInTable);
            config.setLabel(label);
            config.setLabelCss(labelCss);
            config.setColCss(colCss);
            config.setLookup(lookup);
            config.setLookupArg(lookupArg);
            config.setMoneyFormat(moneyFormat);
            config.setName(name);
            config.setNoLabel(noLabel);
            config.setOptions(options);
            config.setPlaceholder(placeholder);
            config.setPropertyName(propertyName);
            config.setType(type);
            config.setValidators(validators);
            config.setFormFullWidth(formFullWidth);
            config.setRowConfig(rowConfig);
            config.setSelectUrl(selectUrl);
            config.setAlign(align);
            config.setSortable(sortable);
            config.setSortableField(StringUtils.isEmpty(sortableField) ? name : sortableField);
            config.setFilterElasticName(StringUtils.isEmpty(filterElasticName) ? name : filterElasticName);
            config.setViewHide(viewHide);
            config.setLookupLayout(lookupLayout);
            config.setLookupInsertUrl(insertUrl);
            config.setLookupCollection(lookupCollection);
            config.setLookupSelectUrl(lookupSelectUrl);
            config.setLookupOverwritesForm(lookupOverwritesForm);
            config.setLookupSearchUrl(lookupSearchUrl);
            config.setNewTableElementUrl(newTableElementUrl);
            config.setExpandablePanel(expandablePanel);
            config.setStrippedRows(strippedRows);
            config.setHideIfEmpty(hideIfEmpty);
            config.setProperties(properties);
            config.setPanelOptionsUrl(panelOptionsUrl);
            config.setLookupWatcherFieldName(lookupWatcherFieldName);
            config.setLookupWatcherFieldObjectId(lookupWatcherFieldObjectId);
            config.setOnBefore(onBefore);
            config.setOnAfter(onAfter);
            config.setMediaType(mediaType);
            config.setDisplayOrder(displayOrder);
            config.setBlastDeltaCommand(blastDeltaCommand);
            config.setKeyField(keyField);

            if (Boolean.TRUE.equals(clipboard)) {
                config.setClipboard(true);
            }
            return config;
        }

        public String getSelectUrl() {
            return selectUrl;
        }

        public void setSelectUrl(String selectUrl) {
            this.selectUrl = selectUrl;
        }

        public boolean isSortable() {
            return sortable;
        }

        public void setSortable(boolean sortable) {
            this.sortable = sortable;
        }

        public Builder viewHide() {
            this.viewHide = true;
            return this;
        }

        public Builder lookupModalLayout(String lookupModalLayout) {
            this.lookupLayout = lookupModalLayout;
            return this;
        }

        public void setInsertUrl(String insertUrl) {
            this.insertUrl = insertUrl;
        }

        public String getInsertUrl() {
            return insertUrl;
        }

        public void newTableElementUrl(String emptyToNull) {
            newTableElementUrl = emptyToNull;
        }

        public void expandablePanel(String expandablePanel) {
            this.expandablePanel = expandablePanel;
        }

        public void strippedRows(boolean strippedRows) {
            this.strippedRows = strippedRows;
        }

        public boolean isStrippedRows() {
            return strippedRows;
        }

        public void setStrippedRows(boolean strippedRows) {
            this.strippedRows = strippedRows;
        }

        public void hideIfEmpty(boolean hideIfEmpty) {
            this.hideIfEmpty = hideIfEmpty;
        }
    }
}
