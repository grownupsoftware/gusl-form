package gusl.form.builder.toolbar;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.form.model.MenuDO;
import gusl.form.model.MenuGroupDO;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static gusl.core.utils.Utils.safeStream;
import static java.util.stream.Collectors.toUnmodifiableList;

public abstract class AbstractToolbarBuilder {
    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private List<TickerToolbarItem> theTickers = new ArrayList<>();
    private List<ToolbarItem> theItems = new ArrayList<>();
    private List<MenuGroupDO> theMenus = new ArrayList<>();
    private boolean loaded;

    public abstract void addToolbarTickers(List<TickerToolbarItem> tickers);

    public abstract void addToolbarItems(List<ToolbarItem> items);

    public abstract void addMenuGroups(List<MenuGroupDO> menus);

    public ToolbarBuilder build(LoggedInUserDO loggedInUser) {

        if (!loaded) {
            initialise();
        }

        List<MenuGroupDO> filteredMenuGroups = buildMenuGroupsAndMenuItems(loggedInUser);
        return ToolbarBuilder.builder()
                .tickers(safeStream(theTickers).filter(tickerAllowedForUser(loggedInUser)).collect(toUnmodifiableList()))
                .items(safeStream(theItems).filter(itemAllowedForUser(loggedInUser)).collect(toUnmodifiableList()))
                .menus(filteredMenuGroups)
                .build();
    }

    private List<MenuGroupDO> buildMenuGroupsAndMenuItems(LoggedInUserDO loggedInUser) {
        return safeStream(theMenus)
                .map(menuGroup -> {
                    final List<MenuDO> menuItems = safeStream(menuGroup.getMenuItems())
                            .peek(menuItem -> logger.info("{} {}", menuItem.getCode(), menuItemAllowedForUser(loggedInUser).test(menuItem)))
                            .filter(menuItemAllowedForUser(loggedInUser))
                            .collect(toUnmodifiableList());
                    if (menuItems.isEmpty()) {
                        return null;
                    }
                    return MenuGroupDO.builder()
                            .displayOrder(menuGroup.getDisplayOrder())
                            .label(menuGroup.getLabel())
                            .menuItems(menuItems)
                            .alwaysOpen(menuGroup.isAlwaysOpen())
                            .icon(menuGroup.getIcon())
                            .iconOnly(menuGroup.isIconOnly())
                            .route(menuGroup.getRoute())
                            .permission(menuGroup.getPermission())
                            .code(menuGroup.getCode())
                            .command(menuGroup.getCommand())
                            .align(menuGroup.getAlign())
                            .action(menuGroup.getAction())
                            .footer(menuGroup.isFooter())
                            .overlay(menuGroup.isOverlay())
                            .mobileTopSticky(menuGroup.getMobileTopSticky())
                            .build();

                }).filter(Objects::nonNull)
                .collect(toUnmodifiableList());
    }

    protected Predicate<TickerToolbarItem> tickerAllowedForUser(LoggedInUserDO loggedInUser) {
        return ticker -> {
            if (loggedInUser.isSuperUser() || StringUtils.isBlank(ticker.getPermission())) {
                return true;
            }
            return loggedInUser.getPermissions().contains(ticker.getPermission());
        };
    }

    protected Predicate<ToolbarItem> itemAllowedForUser(LoggedInUserDO loggedInUser) {
        return item -> {
            if (loggedInUser.isSuperUser() || StringUtils.isBlank(item.getPermission())) {
                return true;
            }
            return loggedInUser.getPermissions().contains(item.getPermission());
        };
    }

    protected Predicate<MenuDO> menuItemAllowedForUser(LoggedInUserDO loggedInUser) {
        return menu -> {
            if (loggedInUser.isSuperUser() || StringUtils.isBlank(menu.getPermission())) {
                return true;
            }
            return loggedInUser.getPermissions().contains(menu.getPermission());
        };
    }

    private synchronized void initialise() {
        if (!loaded) {
            addToolbarTickers(theTickers);
            addToolbarItems(theItems);
            loaded = true;
        }
    }

}
