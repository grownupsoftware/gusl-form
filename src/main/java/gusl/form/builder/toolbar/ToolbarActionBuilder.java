package gusl.form.builder.toolbar;

import gusl.core.tostring.ToString;
import gusl.form.model.ActionConfigDO;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ToolbarActionBuilder {

    @Singular
    List<ActionConfigDO> actions;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
