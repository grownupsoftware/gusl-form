package gusl.form.builder.toolbar;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class ToolbarItem {
    private String externalLink;
    private String routerLink;
    private String tooltip;
    private String modal;
    private String icon;
    private boolean leftSeparator;
    private int displayOrder;
    private boolean second; // on second toolbar
    private String bespoke; // requires supporting ui code
    private String permission;

}
