package gusl.form.builder.toolbar;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class TickerToolbarItem {
    private String code;
    private String icon;
    private String permission;
    private String tooltip;
    private int displayOrder;
    private boolean second; // on second toolbar
}
