package gusl.form.builder.toolbar;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class TickerToolbar {
    @Singular
    @ToStringCount
    private List<TickerToolbarItem> items;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
