package gusl.form.builder.toolbar;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import gusl.form.model.MenuGroupDO;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ToolbarBuilder {

    @Singular
    @ToStringCount
    private List<TickerToolbarItem> tickers;

    @Singular
    @ToStringCount
    private List<ToolbarItem> items;

    @Singular("menu")
    @ToStringCount
    private List<MenuGroupDO> menus;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
