package gusl.form.builder.toolbar;

import gusl.auth.model.loggedin.LoggedInUserDO;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.form.model.MenuDO;
import gusl.form.model.MenuGroupDO;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toUnmodifiableList;

public abstract class AbstractUiToolbar {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    protected List<MenuGroupDO> filterPermissibleMenuGroups(LoggedInUserDO loggedInUser, List<MenuGroupDO> menuGroups) {
        return safeStream(menuGroups)
                .filter(Objects::nonNull)
                .map(menuGroup -> {
                    final List<MenuDO> menuItems = safeStream(menuGroup.getMenuItems())
                            // .peek(menuItem -> logger.info("{} {}", menuItem.getCode(), menuItemAllowedForUser(loggedInUser).test(menuItem)))
                            .filter(menuItemAllowedForUser(loggedInUser))
                            .collect(toUnmodifiableList());
                    if (menuItems.isEmpty()
                            && gusl.core.utils.StringUtils.isBlank(menuGroup.getCode())
                            && gusl.core.utils.StringUtils.isBlank(menuGroup.getRoute())
                            && isNull(menuGroup.getAction())
                            && gusl.core.utils.StringUtils.isBlank(menuGroup.getCommand())
                    ) {
                        return null;
                    }
                    return MenuGroupDO.builder()
                            .displayOrder(menuGroup.getDisplayOrder())
                            .label(menuGroup.getLabel())
                            .menuItems(menuItems)
                            .alwaysOpen(menuGroup.isAlwaysOpen())
                            .icon(menuGroup.getIcon())
                            .iconOnly(menuGroup.isIconOnly())
                            .route(menuGroup.getRoute())
                            .permission(menuGroup.getPermission())
                            .code(menuGroup.getCode())
                            .command(menuGroup.getCommand())
                            .align(menuGroup.getAlign())
                            .action(menuGroup.getAction())
                            .footer(menuGroup.isFooter())
                            .overlay(menuGroup.isOverlay())
                            .mobileTopSticky(menuGroup.getMobileTopSticky())
                            .build();

                }).filter(Objects::nonNull)
                .collect(toUnmodifiableList());
    }

    protected Predicate<MenuDO> menuItemAllowedForUser(LoggedInUserDO loggedInUser) {
        return menu -> {
            if (loggedInUser.isSuperUser() || StringUtils.isBlank(menu.getPermission())) {
                return true;
            }
            return loggedInUser.getPermissions().contains(menu.getPermission());
        };
    }

}
