package gusl.form.builder.builders;

import gusl.auth.model.loggedin.ILoggedInUser;
import gusl.form.model.MaintainConfigDO;

import static gusl.form.builder.FormBuilder.getConfig;

public class EntityMaintain {

    public static class Builder {

        private final Class dtoClass;
        private boolean canAdd;
        private boolean canView;
        private boolean canEdit;
        private boolean canDelete;
        private String title;

        public Builder(Class dtoClass) {
            this.dtoClass = dtoClass;
        }

        public Builder title(String title) {
            this.title = title;
            // this.canAdd = true;
            return this;
        }

        public Builder canAdd() {
            this.canAdd = true;
            return this;
        }

        public Builder canView() {
            this.canView = true;
            return this;
        }

        public Builder canEdit() {
            this.canEdit = true;
            return this;
        }

        public Builder canDelete() {
            this.canDelete = true;
            return this;
        }

        public boolean hasMode() {
            return (this.canEdit || this.canView || this.canAdd || this.canDelete);
        }

        public MaintainConfigDO build(ILoggedInUser loggedInUser) {
            if (title == null) {
                title = dtoClass.getSimpleName().replace("DTO", "").replace("DO", "");
            }
            return getConfig(title, dtoClass, loggedInUser);
        }

    }
}
