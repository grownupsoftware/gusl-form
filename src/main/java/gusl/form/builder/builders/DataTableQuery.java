package gusl.form.builder.builders;

import gusl.annotations.form.MediaType;
import gusl.annotations.form.page.OrderByActionDO;
import gusl.form.builder.FormBuilder;
import gusl.form.menus.builders.EntityGroupMenuBuilder;
import gusl.form.model.*;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static gusl.form.builder.FormBuilder.getDataTableConfig;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class DataTableQuery {

    public static class Builder {

        private Class dtoClass;
        private Class convertTo;
        private String selectUrl;
        private Boolean expandable = false;
        private String title;
        private SortDTO defaultSort;
        private String oneUrl;
        private List<ActionConfigDO> actions;
        private List<ActionConfigDO> rowActions;

        protected List<ActionConfigDO> groupActions;

        private List<OrderByActionDO> orderByActions;

        private String aggsUrl;
        private String getUrl;
        private List<RowConditionDTO> conditions;
        private List<ConditionalRowDetailsDTO> conditionals = new ArrayList<>();

        private Integer refreshRate;

        private String blastDeltaCommand;
        private TableControlDO tableControl;

        private MediaType mediaType;

        private boolean alphaList;

        private boolean flexList;

        private boolean accordionList;

        public Builder(Class dtoClass) {
            this.dtoClass = dtoClass;
        }

        public Builder alphaList() {
            this.alphaList = true;
            return this;
        }

        public Builder flexList() {
            this.flexList = true;
            return this;
        }

        public Builder accordionList() {
            this.accordionList = true;
            return this;
        }

        public Builder alphaList(boolean alphaList) {
            this.alphaList = alphaList;
            return this;
        }

        public Builder accordionList(boolean accordionList) {
            this.accordionList = accordionList;
            return this;
        }

        public Builder flexList(boolean flexList) {
            this.flexList = flexList;
            return this;
        }

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder tableControl(TableControlDO tableControl) {
            this.tableControl = tableControl;
            return this;
        }

        public Builder blastDeltaCommand(String blastDeltaCommand) {
            this.blastDeltaCommand = blastDeltaCommand;
            return this;
        }

        public Builder selectUrl(String selectUrl) {
            this.selectUrl = selectUrl;
            return this;
        }

        public Builder refreshRate(Integer refreshRate) {
            this.refreshRate = refreshRate;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder expandable(Boolean expandable) {
            this.expandable = expandable;
            return this;
        }

        public Builder defaultSort(SortDTO sort) {
            this.defaultSort = sort;
            return this;
        }

        public Builder initialConditions(List<RowConditionDTO> conditions) {
            this.conditions = conditions;
            return this;
        }

        public Builder conditionals(List<ConditionalRowDetailsDTO> conditionals) {
            this.conditionals.addAll(conditionals);
            return this;
        }

        public DataTableConfigDO build() {
            if (isNull(title)) {
                title = dtoClass.getSimpleName().replace("DTO", "").replace("DO", "");
            }
            DataTableConfigDO config = getDataTableConfig(title, dtoClass, blastDeltaCommand, refreshRate);
            config.setSelectUrl(selectUrl);
            TableViewConfigDO tableView = new TableViewConfigDO();
            if (expandable) {
                ConditionalRowDetailsDTO conditional = new ConditionalRowDetailsDTO();
                conditional.setExpandable(true);
                conditional.setDetailsConfig(FormBuilder.getRowConfig(isNull(convertTo) ? dtoClass : convertTo));
                conditionals.add(conditional);
            }
            config.setTableControl(tableControl);

            tableView.setConditionals(conditionals);
            final Class aClass = (nonNull(convertTo) && !convertTo.getSimpleName().equals("Object")) ? convertTo : dtoClass;
            tableView.setFilters(EntityGroupMenuBuilder.getFilters(aClass));

            // tableView.setFilters(prepareCassandraFilters());
            tableView.setDefaultSort(defaultSort);
            tableView.setAggsUrl(aggsUrl);
            tableView.setPaged(true);
            tableView.setRefreshRate(refreshRate);
            MaintainTableTab tab = new MaintainTableTab(new EndpointsConfigDO(StringUtils.stripToNull(oneUrl)), tableView);
            tab.setInitialConditions(conditions);
            tab.getEndpoints().setSelectAllUrl(getUrl);
            config.setTabs(Collections.singletonList(tab));
            config.setRestActions(actions);
            config.setRowActions(rowActions);
            config.setGroupActions(groupActions);
            config.setOrderByActions(orderByActions);
            config.setMediaType(mediaType);
            config.setAlphaList(alphaList);
            config.setFlexList(flexList);
            config.setAccordionList(accordionList);
            return config;
        }

        public Builder getOneUrl(String url) {
            this.oneUrl = url;
            return this;
        }

        public Builder restActions(List<ActionConfigDO> actions) {
            this.actions = actions;
            return this;
        }

        public Builder rowActions(List<ActionConfigDO> rowActions) {
            this.rowActions = rowActions;
            return this;
        }

        public Builder groupActions(List<ActionConfigDO> groupActions) {
            this.groupActions = groupActions;
            return this;
        }

        public Builder orderByActions(List<OrderByActionDO> orderByActions) {
            this.orderByActions = orderByActions;
            return this;
        }


        public Builder aggsUrl(String url) {
            this.aggsUrl = url;
            return this;
        }

        public Builder getUrl(String getUrl) {
            this.getUrl = getUrl;
            return this;
        }
    }

}
