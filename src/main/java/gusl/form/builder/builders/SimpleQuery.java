package gusl.form.builder.builders;

import gusl.form.model.SimpleQueryConfigDO;

public class SimpleQuery {
    public static class Builder {

        private Class inboundClass;

        private Class outboundClass;

        private String endpoint;

        public Builder() {
        }

        public Builder inbound(Class inbound) {
            this.inboundClass = inbound;
            return this;
        }

        public Builder outbound(Class outbound) {
            this.outboundClass = outbound;
            return this;
        }

        public Builder endpoint(String endpoint) {
            this.endpoint = endpoint;
            return this;
        }

        public SimpleQueryConfigDO build() {
            SimpleQueryConfigDO config = new SimpleQueryConfigDO();
            config.setEndpoint(endpoint);
            config.setInboundClass(inboundClass);
            config.setOutboundClass(outboundClass);
            return config;
        }

    }

}
