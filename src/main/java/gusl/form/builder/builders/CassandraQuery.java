package gusl.form.builder.builders;

import gusl.annotations.form.MediaType;
import gusl.annotations.form.page.OrderByActionDO;
import gusl.form.builder.FormBuilder;
import gusl.form.menus.builders.EntityGroupMenuBuilder;
import gusl.form.model.*;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static gusl.form.builder.FormBuilder.getCassandraConfig;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class CassandraQuery {

    public static class Builder {

        private Class dtoClass;
        private Class convertTo;
        private String selectUrl;
        private Boolean expandable = false;
        private String title;
        private SortDTO defaultSort;
        private String oneUrl;
        private List<ActionConfigDO> actions;
        private List<ActionConfigDO> rowActions;
        protected List<ActionConfigDO> groupActions;
        private List<OrderByActionDO> orderByActions;

        private String aggsUrl;
        private String sortTieBreaker = "id";
        private String elasticFilterGroup;
        private String getUrl;
        private List<RowConditionDTO> conditions;
        private List<ConditionalRowDetailsDTO> conditionals = new ArrayList<>();

        private String blastDeltaCommand;
        private boolean highlightDelta;

        private boolean disableTableControls;
        private boolean disableColumnResize;

        private Integer refreshRate;

        private TableControlDO tableControl;

        private MediaType mediaType;

        public Builder(Class dtoClass) {
            this.dtoClass = dtoClass;
        }

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder convertTo(Class convertTo) {
            this.convertTo = convertTo;
            return this;
        }

        public Builder tableControl(TableControlDO tableControl) {
            this.tableControl = tableControl;
            return this;
        }

        public Builder blastDeltaCommand(String blastDeltaCommand) {
            this.blastDeltaCommand = blastDeltaCommand;
            return this;
        }

        public Builder highlightDelta(boolean highlightDelta) {
            this.highlightDelta = highlightDelta;
            return this;
        }

        public Builder disableTableControls(boolean disableTableControls) {
            this.disableTableControls = disableTableControls;
            return this;
        }

        public Builder disableColumnResize(boolean disableColumnResize) {
            this.disableColumnResize = disableColumnResize;
            return this;
        }

        public Builder selectUrl(String selectUrl) {
            this.selectUrl = selectUrl;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder expandable(Boolean expandable) {
            this.expandable = expandable;
            return this;
        }

        public Builder defaultSort(SortDTO sort) {
            this.defaultSort = sort;
            return this;
        }

        public Builder initialConditions(List<RowConditionDTO> conditions) {
            this.conditions = conditions;
            return this;
        }

        public Builder refreshRate(Integer refreshRate) {
            this.refreshRate = refreshRate;
            return this;
        }

        public Builder conditionals(List<ConditionalRowDetailsDTO> conditionals) {
            this.conditionals.addAll(conditionals);
            return this;
        }

        public CassandraConfigDO build() {
            if (isNull(title)) {
                title = dtoClass.getSimpleName().replace("DTO", "").replace("DO", "");
            }
            CassandraConfigDO config = getCassandraConfig(
                    title,
                    dtoClass,
                    convertTo,
                    blastDeltaCommand,
                    highlightDelta,
                    disableTableControls,
                    disableColumnResize,
                    refreshRate);
            config.setTableControl(tableControl);
            config.setSelectUrl(selectUrl);
            config.setBlastDeltaCommand(blastDeltaCommand);
            TableViewConfigDO tableView = new TableViewConfigDO();
            if (expandable) {
                ConditionalRowDetailsDTO conditional = new ConditionalRowDetailsDTO();
                conditional.setExpandable(true);
                conditional.setDetailsConfig(FormBuilder.getRowConfig(isNull(convertTo) ? dtoClass : convertTo));
                conditionals.add(conditional);
            }

            tableView.setConditionals(conditionals);
            final Class aClass = (nonNull(convertTo) && !convertTo.getSimpleName().equals("Object")) ? convertTo : dtoClass;
            tableView.setFilters(EntityGroupMenuBuilder.getFilters(aClass));

            // tableView.setFilters(prepareCassandraFilters());
            tableView.setDefaultSort(defaultSort);
            tableView.setSortTieBreaker(sortTieBreaker);
            tableView.setAggsUrl(aggsUrl);
            tableView.setPaged(true);
            tableView.setBlastDeltaCommand(blastDeltaCommand);
            tableView.setRefreshRate(refreshRate);
            MaintainTableTab tab = new MaintainTableTab(new EndpointsConfigDO(StringUtils.stripToNull(oneUrl)), tableView);
            tab.setInitialConditions(conditions);
            tab.getEndpoints().setSelectAllUrl(getUrl);
            config.setTabs(Collections.singletonList(tab));
            config.setRestActions(actions);
            config.setRowActions(rowActions);
            config.setGroupActions(groupActions);
            config.setOrderByActions(orderByActions);
            config.setMediaType(mediaType);
            return config;
        }

//        private List<FieldConfigDO> prepareCassandraFilters() {
//            List<FieldConfigDO> filters = EntityGroupMenuBuilder.getFilters(isNull(convertTo) ? dtoClass : convertTo, userContext);
//            FieldConfigDO entityFilter = new FieldConfigDO();
//            entityFilter.setName("CASSANDRA_FILTER");
//            entityFilter.setType(FieldType.text);
//            entityFilter.setLabel("Cassandra filter");
//            entityFilter.setFilterElasticName("CASSANDRA_FILTER");
//            entityFilter.setValidators(Collections.emptyList());
//            filters.add(entityFilter);
//            return filters;
//        }

        public Builder getOneUrl(String url) {
            this.oneUrl = url;
            return this;
        }

        public Builder restActions(List<ActionConfigDO> actions) {
            this.actions = actions;
            return this;
        }

        public Builder rowActions(List<ActionConfigDO> rowActions) {
            this.rowActions = rowActions;
            return this;
        }

        public Builder groupActions(List<ActionConfigDO> groupActions) {
            this.groupActions = groupActions;
            return this;
        }

        public Builder orderByActions(List<OrderByActionDO> orderByActions) {
            this.orderByActions = orderByActions;
            return this;
        }

        public Builder aggsUrl(String url) {
            this.aggsUrl = url;
            return this;
        }

        public Builder sortTieBreaker(String sortTieBreaker) {
            this.sortTieBreaker = sortTieBreaker;
            return this;
        }

        public Builder elasticFilterGroup(String elasticFilterGroup) {
            this.elasticFilterGroup = elasticFilterGroup;
            return this;
        }

        public Builder getUrl(String getUrl) {
            this.getUrl = getUrl;
            return this;
        }
    }

}
