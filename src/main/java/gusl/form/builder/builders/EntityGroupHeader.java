package gusl.form.builder.builders;

import gusl.form.model.EntityGroupHeaderDO;
import gusl.form.model.FormRowConfigDO;

import java.util.LinkedList;
import java.util.List;

public class EntityGroupHeader {

    public static class Builder {

        private String backgroundColor;
        private List<FormRowConfigDO> rows = new LinkedList<>();

        public Builder() {
        }

        public Builder backgroundColor(String backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        public Builder row(FormRowConfigDO row) {
            rows.add(row);
            return this;
        }

        public EntityGroupHeaderDO build() {
            EntityGroupHeaderDO header = new EntityGroupHeaderDO();
            header.setBackgroundColor(backgroundColor);
            header.setRows(rows);

            return header;

        }
    }
}
