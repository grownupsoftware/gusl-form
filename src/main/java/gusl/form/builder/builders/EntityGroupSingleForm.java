package gusl.form.builder.builders;

import gusl.form.model.EndPointDO;
import gusl.form.model.FormColumnConfigDO;
import gusl.form.model.FormRowConfigDO;
import gusl.form.model.SingleFormConfigDO;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static gusl.form.builder.FormBuildHelper.getUpdateUrl;

public class EntityGroupSingleForm {

    public static final String SIMPLE_FORM_CODE = "content";

    public static class Builder {

        private String title;
        private EndPointDO updateUrl;
        private List<FormRowConfigDO> rows = new LinkedList<>();
        private String jsEditCondition;
        private boolean multipartForm;

        private boolean noBanner;

        //that basically mean that we built it from one flat class with @UiField annotations
        //because of that there has to be only one row containing only one column
        private boolean simple;

        public Builder noBanner() {
            this.noBanner = true;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder multipartForm(boolean multipartForm) {
            this.multipartForm = multipartForm;
            return this;
        }

        public Builder updateUrl(EndPointDO endpoint) {
            this.updateUrl = endpoint;
            return this;
        }

        public Builder updateUrl(String endpoint) {
            this.updateUrl = getUpdateUrl(endpoint);
            return this;
        }

        public Builder row(FormRowConfigDO row) {
            if (row != null) {
                rows.add(row);
            }
            return this;
        }

        public Builder rows(List<FormRowConfigDO> rows) {
            if (rows != null) {
                this.rows.addAll(rows);
            }
            return this;
        }

        public Builder jsEditCondition(String jsEditCondition) {
            this.jsEditCondition = jsEditCondition;
            return this;
        }

        public SingleFormConfigDO build() {
            SingleFormConfigDO singleForm = new SingleFormConfigDO();

            handleSimpleSingleForm();
            singleForm.setRows(rows);
            singleForm.setMultipartForm(multipartForm);
            singleForm.setTitle(title);
            singleForm.setUpdateUrl(this.updateUrl);
            singleForm.setJsEditCondition(jsEditCondition);
            singleForm.setNoBanner(noBanner);
            return singleForm;
        }

        private void handleSimpleSingleForm() {
            Optional<FormColumnConfigDO> firstColInFirstRow = Optional.ofNullable(this.rows)
                    .map(r -> r.get(0))
                    .map(FormRowConfigDO::getColumns)
                    .map(c -> c.get(0));

            Optional<String> updateUrl = Optional.ofNullable(this.updateUrl)
                    .map(EndPointDO::getUrl);

            if (this.simple && firstColInFirstRow.isPresent() && updateUrl.isPresent()) {
                firstColInFirstRow.get().setUpdateUrl(updateUrl.get());
                firstColInFirstRow.get().setCode(SIMPLE_FORM_CODE);
                firstColInFirstRow.get().setNested(true);
            }
        }

        public Builder simple(boolean b) {
            this.simple = b;
            return this;
        }
    }

}
