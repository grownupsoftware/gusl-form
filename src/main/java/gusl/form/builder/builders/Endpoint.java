package gusl.form.builder.builders;

import gusl.form.model.EndPointDO;
import gusl.form.model.EndPointType;
import gusl.form.utils.FormUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static gusl.core.utils.Utils.safeStream;
import static java.util.stream.Collectors.toList;

public class Endpoint {

    public static class Builder {

        private String url;
        private EndPointType method;
        private List<String> fields = new ArrayList<>();

        public Builder() {
            fields = new LinkedList<>();
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder method(EndPointType method) {
            this.method = method;
            return this;
        }

        public Builder requestFields(Class requestClass) {
            fields.addAll(getFieldsFor(requestClass));
            return this;
        }

        private List<String> getFieldsFor(Class<?> clazz) {
            List<Field> classFields = FormUtils.getFieldsFor(clazz);
            return safeStream(classFields).map(field -> field.getName()).collect(toList());
        }

        public EndPointDO build() {
            EndPointDO endpoint = new EndPointDO();
            endpoint.setUrl(url);
            endpoint.setMethod(method);
            endpoint.setFields(fields);

            return endpoint;
        }
    }
}
