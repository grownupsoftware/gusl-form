package gusl.form.builder.builders;

import gusl.annotations.form.MediaType;
import gusl.annotations.form.page.OrderByActionDO;
import gusl.form.model.ActionConfigDO;
import gusl.form.model.DataTableConfigDO;
import gusl.form.model.FormRowConfigDO;
import gusl.form.model.TableControlDO;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.util.Objects.isNull;

public class DataTableBuilder {
    public static class Builder {

        private List<FormRowConfigDO> rows = new LinkedList<>();
        private String dtoClass;
        private String blastDeltaCommand;

        private Integer refreshRate;

        private String selectUrl;

        private MediaType mediaType;

        private boolean alphaList;

        private boolean flexList;

        private boolean accordionList;

        private String defaultSortString;

        protected List<OrderByActionDO> orderByActions;

        protected List<ActionConfigDO> tableActions;
        protected List<ActionConfigDO> overallRestActions;
        protected List<ActionConfigDO> rowActions;

        private TableControlDO tableControl;

        public Builder row(FormRowConfigDO row) {
            rows.add(row);
            return this;
        }

        public Builder rows(List<FormRowConfigDO> rows) {
            this.rows.addAll(rows);
            return this;
        }

        public Builder selectUrl(String selectUrl) {
            this.selectUrl = selectUrl;
            return this;
        }

        public Builder orderByActions(List<OrderByActionDO> orderByActions) {
            this.orderByActions = orderByActions;
            return this;
        }

        public Builder tableActions(List<ActionConfigDO> tableActions) {
            this.tableActions = tableActions;
            return this;
        }

        public Builder overallRestActions(List<ActionConfigDO> overallRestActions) {
            this.overallRestActions = overallRestActions;
            return this;
        }

        public Builder rowActions(List<ActionConfigDO> rowActions) {
            this.rowActions = rowActions;
            return this;
        }

        public Builder tableAction(ActionConfigDO tableAction) {
            if (isNull(this.tableActions)) {
                this.tableActions = new ArrayList<>();
            }
            this.tableActions.add(tableAction);
            return this;
        }

        public Builder overallRestAction(ActionConfigDO overallRestAction) {
            if (isNull(this.overallRestActions)) {
                this.overallRestActions = new ArrayList<>();
            }
            this.overallRestActions.add(overallRestAction);
            return this;
        }

        public Builder rowAction(ActionConfigDO rowAction) {
            if (isNull(this.rowActions)) {
                this.rowActions = new ArrayList<>();
            }
            this.rowActions.add(rowAction);
            return this;
        }

        public Builder orderByAction(OrderByActionDO orderByAction) {
            if (isNull(this.orderByActions)) {
                this.orderByActions = new ArrayList<>();
            }
            this.orderByActions.add(orderByAction);
            return this;
        }

        public Builder defaultSortString(String defaultSortString) {
            this.defaultSortString = defaultSortString;
            return this;
        }

        public Builder alphaList() {
            this.alphaList = true;
            return this;
        }

        public Builder flexList() {
            this.flexList = true;
            return this;
        }

        public Builder accordionList() {
            this.accordionList = true;
            return this;
        }

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder blastDeltaCommand(String blastDeltaCommand) {
            this.blastDeltaCommand = blastDeltaCommand;
            return this;
        }

        public Builder dtoClass(String dtoClass) {
            this.dtoClass = dtoClass;
            return this;
        }

        public Builder refreshRate(Integer refreshRate) {
            this.refreshRate = refreshRate;
            return this;
        }

        public Builder tableControl(TableControlDO tableControl) {
            this.tableControl = tableControl;
            return this;
        }

        public DataTableConfigDO build() {
            DataTableConfigDO config = new DataTableConfigDO();
            config.setRows(rows);
            config.setDtoClass(dtoClass);
            config.setBlastDeltaCommand(blastDeltaCommand);
            config.setRefreshRate(refreshRate);
            config.setMediaType(mediaType);
            config.setSelectUrl(selectUrl);
            config.setAlphaList(alphaList);
            config.setFlexList(flexList);
            config.setAccordionList(accordionList);
            config.setDefaultSortString(defaultSortString);
            config.setOrderByActions(orderByActions);
            config.setRestActions(tableActions);
            config.setOverallRestActions(overallRestActions);
            config.setRowActions(rowActions);
            config.setTableControl(tableControl);
            return config;
        }
    }
}
