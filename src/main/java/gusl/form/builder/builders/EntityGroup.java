package gusl.form.builder.builders;

import gusl.form.builder.SubMenuBuilder;
import gusl.form.model.*;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EntityGroup {

    public static class Builder {

        private List<MenuDO> menuItems = new ArrayList<>();

        private Class dtoClass;
        private String title;
        private String help;
        private EntityGroupHeaderDO header;
        private String selectUrl;
        private MenuDO createConfig;
        private MenuDO entryFormConfig;
        private boolean singleton;
        private String titleTag;
        private List<DataSource> dataSources = new ArrayList<>();

        private String headerUrl;

        public Builder() {
        }

        public Builder(Class dtoClass) {
            this.dtoClass = dtoClass;
        }

        public Builder createConfig(MenuDO createConfig) {
            this.createConfig = createConfig;
            return this;
        }

        public Builder newRecordForm(MenuDO createConfig) {
            this.createConfig = createConfig;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder selectUrl(String selectUrl) {
            this.selectUrl = selectUrl;
            return this;
        }

        public Builder help(String help) {
            this.help = help;
            return this;
        }

        public Builder header(EntityGroupHeaderDO header) {
            this.header = header;
            return this;
        }

        public Builder headerUrl(String url) {
            headerUrl = url;
            return this;
        }

        public static SubMenuBuilder.Builder subMenuItem() {
            return new SubMenuBuilder.Builder();
        }

        public Builder subMenu(MenuDO... menu) {
            if (menu != null) {
                this.menuItems.addAll(Arrays.asList(menu));
            }
            return this;
        }

        public Builder entryForm(MenuDO menu) {
            this.entryFormConfig = menu;
            return this;
        }

        public Builder tabs(MenuDO... menu) {
            if (menu != null) {
                this.menuItems.addAll(Arrays.asList(menu));
            }
            return this;
        }

        public Builder subMenus(List<MenuDO> menus) {
            this.menuItems.addAll(menus);
            return this;
        }

        public Builder clearSubMenus() {
            this.menuItems.clear();
            return this;
        }

        public Builder tabs(List<MenuDO> menus) {
            this.menuItems.addAll(menus);
            return this;
        }

        public Builder selectConfigMaintain(MaintainConfigDO rowDefinition) {
            dataSources.add(new DataSource<>(MenuType.MAINTAIN, rowDefinition));
            return this;
        }

        public Builder selectConfigTicker(TickerConfigDO rowDefinition) {
            dataSources.add(new DataSource<>(MenuType.TICKER, rowDefinition));
            return this;
        }

        public Builder selectConfigElastic(ElasticConfigDO rowDefinition) {
            dataSources.add(new DataSource<>(MenuType.ELASTIC, rowDefinition));
            return this;
        }

        public Builder selectConfigCassandra(CassandraConfigDO rowDefinition) {
            dataSources.add(new DataSource<>(MenuType.CASSANDRA, rowDefinition));
            return this;
        }

        public Builder selectConfigDataTable(DataTableConfigDO rowDefinition) {
            dataSources.add(new DataSource<>(MenuType.DATA_TABLE, rowDefinition));
            return this;
        }

        public Builder selectConfigBlastCollectionTable(BlastCollectionConfigDO config) {
            dataSources.add(new DataSource<>(MenuType.BLAST_COLLECTION_TABLE, config));
            return this;
        }

        public EntityGroupConfigDO build() {
            EntityGroupConfigDO entityGroupConfigDO = buildSingletonEntityConfig();

            if (singleton) {
                return entityGroupConfigDO;
            }

            entityGroupConfigDO.setCreateConfig(createConfig);

            // create the initial display table
            //maintainRowDef = Objects.isNull(maintainRowDef) ? getEntityGroupConfig(title, dtoClass) : maintainRowDef;

            entityGroupConfigDO.setDataSources(dataSources);

            if (dataSources.size() > 0) {
                entityGroupConfigDO.setSelectTableType(MenuType.MULTIPLE);
            }
            return entityGroupConfigDO;
        }

        public EntityGroupConfigDO buildSingletonEntityConfig() {
            if (StringUtils.isEmpty(title) && dtoClass != null) {
                title = dtoClass.getSimpleName().replace("DTO", "").replace("DO", "");
            }
            EntityGroupConfigDO entityGroupConfigDO = new EntityGroupConfigDO();
            entityGroupConfigDO.setDataSources(new ArrayList<>());
            entityGroupConfigDO.setTitle(title);
            entityGroupConfigDO.setHelp(help);
            entityGroupConfigDO.setMenuItems(menuItems);
            entityGroupConfigDO.setHeader(header);
            entityGroupConfigDO.setSelectUrl(selectUrl);
            entityGroupConfigDO.setSelectTableType(MenuType.ENTITY_GROUP_SINGLE_FORM);
            entityGroupConfigDO.setTitleTag(titleTag);
            entityGroupConfigDO.setDataSources(dataSources);
            entityGroupConfigDO.setEntryForm(entryFormConfig);
            entityGroupConfigDO.setHeaderUrl(headerUrl);

            return entityGroupConfigDO;
        }

        public Builder singleton(boolean singleton) {
            this.singleton = singleton;

            return this;
        }

        public Builder titleTag(String titleTag) {
            this.titleTag = titleTag;
            return this;
        }

        public Builder entryFormConfig(MenuDO queryConfig) {
            this.entryFormConfig = queryConfig;
            return this;
        }
    }

}
