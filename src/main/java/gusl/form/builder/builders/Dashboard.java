package gusl.form.builder.builders;

import gusl.form.model.DashboardConfigDO;

import static gusl.form.builder.FormBuilder.getDashboardConfig;

public class Dashboard {

    public static class Builder {

        private final Class dtoClass;
        private String title;

        public Builder(Class dtoClass) {
            this.dtoClass = dtoClass;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public DashboardConfigDO build() {
            if (title == null) {
                title = dtoClass.getSimpleName().replace("DTO", "").replace("DO", "");
            }
            return getDashboardConfig(title, dtoClass);
        }
    }

}
