package gusl.form.builder.builders;

import gusl.form.model.FormRowConfigDO;
import gusl.form.model.SingleFormConfigDO;

import java.util.LinkedList;
import java.util.List;

public class SingleForm {

    public static class Builder {

        private final Class dtoClass;
        private boolean canAdd;
        private boolean canView;
        private boolean canEdit;
        private boolean canDelete;
        private String title;
        private List<FormRowConfigDO> rows = new LinkedList<>();

        public Builder() {
            this.dtoClass = null;
        }

        public Builder(Class dtoClass) {
            this.dtoClass = dtoClass;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder canEdit() {
            this.canEdit = true;
            return this;
        }

        public Builder row(FormRowConfigDO row) {
            rows.add(row);
            return this;
        }

        public SingleFormConfigDO build() {
            SingleFormConfigDO singleFormConfigDO = new SingleFormConfigDO();
            singleFormConfigDO.setRows(rows);
            return singleFormConfigDO;
        }
    }

}
