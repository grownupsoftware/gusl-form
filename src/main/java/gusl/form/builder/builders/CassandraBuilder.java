package gusl.form.builder.builders;

import gusl.form.model.CassandraConfigDO;
import gusl.form.model.FormRowConfigDO;
import gusl.form.model.TableControlDO;

import java.util.LinkedList;
import java.util.List;

public class CassandraBuilder {
    public static class Builder<T> {

        private List<FormRowConfigDO> rows = new LinkedList<>();
        private String convertTo;
        private String dtoClass;

        private String blastDeltaCommand;

        private boolean highlightDelta;

        private boolean disableTableControls;
        private boolean disableColumnResize;

        private Integer refreshRate;

        private TableControlDO tableControl;


        public Builder row(FormRowConfigDO row) {
            rows.add(row);
            return this;
        }

        public Builder rows(List<FormRowConfigDO> rows) {
            this.rows.addAll(rows);
            return this;
        }

        public Builder convertTo(String convertTo) {
            this.convertTo = convertTo;
            return this;
        }

        public Builder dtoClass(String dtoClass) {
            this.dtoClass = dtoClass;
            return this;
        }

        public Builder blastDeltaCommand(String blastDeltaCommand) {
            this.blastDeltaCommand = blastDeltaCommand;
            return this;
        }

        public Builder highlightDelta(boolean highlightDelta) {
            this.highlightDelta = highlightDelta;
            return this;
        }

        public Builder disableTableControls(boolean disableTableControls) {
            this.disableTableControls = disableTableControls;
            return this;
        }

        public Builder refreshRate(Integer refreshRate) {
            this.refreshRate = refreshRate;
            return this;
        }

        public Builder disableColumnResize(boolean disableColumnResize) {
            this.disableColumnResize = disableColumnResize;
            return this;
        }
        public Builder tableControl(TableControlDO tableControl) {
            this.tableControl = tableControl;
            return this;
        }


        public CassandraConfigDO build() {
            CassandraConfigDO config = new CassandraConfigDO();
            config.setRows(rows);
            config.setConvertTo(convertTo);
            config.setDtoClass(dtoClass);
            config.setBlastDeltaCommand(blastDeltaCommand);
            config.setHighlightDelta(highlightDelta);
            config.setDisableTableControls(disableTableControls);
            config.setDisableColumnResize(disableColumnResize);
            config.setRefreshRate(refreshRate);
            config.setTableControl(tableControl);
            return config;
        }
    }
}
