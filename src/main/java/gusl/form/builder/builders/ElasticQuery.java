package gusl.form.builder.builders;

import gusl.annotations.form.MediaType;
import gusl.annotations.form.page.OrderByActionDO;
import gusl.form.builder.FormBuilder;
import gusl.form.menus.builders.EntityGroupMenuBuilder;
import gusl.form.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static gusl.form.builder.FormBuilder.getElasticConfig;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.stripToNull;

public class ElasticQuery {

    public static class Builder {

        private final Class dtoClass;
        private Class convertTo;
        private String title;
        private boolean withTimestamp = false;
        private Boolean expandable = false;
        private List<RowConditionDTO> conditions = new ArrayList<>();
        private String index;
        private List<ConditionalRowDetailsDTO> conditionals = new ArrayList<>();
        private SortDTO defaultSort;
        private String oneUrl;
        private List<ActionConfigDO> actions = new ArrayList<>();

        private List<ActionConfigDO> groupActions;

        private List<OrderByActionDO> orderByActions;

        private String aggsUrl;
        private String sortTieBreaker = "id";
        private String elasticFilterGroup;
        private String getUrl;

        private Integer refreshRate;
        private TableControlDO tableControl;

        private MediaType mediaType;

        public Builder(Class dtoClass) {
            this.dtoClass = dtoClass;
            this.conditions = new ArrayList<>();
            this.conditionals = new ArrayList<>();
            this.actions = new ArrayList<>();
            this.groupActions = new ArrayList<>();
        }

        public Builder tableControl(TableControlDO tableControl) {
            this.tableControl = tableControl;
            return this;
        }

        public Builder convertTo(Class convertTo) {
            this.convertTo = convertTo;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public Builder refreshRate(Integer refreshRate) {
            this.refreshRate = refreshRate;
            return this;
        }

        public Builder withTimestamp() {
            this.withTimestamp = true;
            return this;
        }

        public Builder withTimestamp(boolean withTimestamp) {
            this.withTimestamp = withTimestamp;
            return this;
        }

        public Builder expandable(Boolean expandable) {
            this.expandable = expandable;
            return this;
        }

        public Builder initialConditions(List<RowConditionDTO> conditions) {
            this.conditions = conditions;
            return this;
        }

        public Builder index(String index) {
            this.index = index;
            return this;
        }

        public Builder conditionals(List<ConditionalRowDetailsDTO> conditionals) {
            this.conditionals.addAll(conditionals);
            return this;
        }

        public Builder defaultSort(SortDTO sort) {
            this.defaultSort = sort;
            return this;
        }

        public ElasticConfigDO build() {
            if (title == null) {
                title = dtoClass.getSimpleName().replace("DTO", "").replace("DO", "");
            }
            ElasticConfigDO elasticConfig = getElasticConfig(title, dtoClass, convertTo, withTimestamp, refreshRate);

            TableViewConfigDO tableView = new TableViewConfigDO();
            if (expandable) {
                ConditionalRowDetailsDTO conditional = new ConditionalRowDetailsDTO();
                conditional.setExpandable(true);
                conditional.setDetailsConfig(FormBuilder.getRowConfig(Objects.isNull(convertTo) ? dtoClass : convertTo));
                conditionals.add(conditional);
            }
            elasticConfig.setTableControl(tableControl);

            tableView.setConditionals(conditionals);
            final Class aClass = (nonNull(convertTo) && !convertTo.getSimpleName().equals("Object")) ? convertTo : dtoClass;
            tableView.setFilters(EntityGroupMenuBuilder.getFilters(aClass));
            tableView.setDefaultSort(defaultSort);
            tableView.setSortTieBreaker(sortTieBreaker);
            tableView.setAggsUrl(aggsUrl);
            tableView.setRefreshRate(refreshRate);
            MaintainTableTab tab = new MaintainTableTab(new EndpointsConfigDO(stripToNull(oneUrl)), tableView);
            tab.setInitialConditions(conditions);
            tab.getEndpoints().setSelectAllUrl(getUrl);
            elasticConfig.setTabs(Collections.singletonList(tab));
            elasticConfig.setIndexName("lm-" + index);
            elasticConfig.setRestActions(actions);
            elasticConfig.setGroupActions(groupActions);
            elasticConfig.setElasticFilterGroup(elasticFilterGroup);
            elasticConfig.setMediaType(mediaType);
            return elasticConfig;
        }

        public Builder getOneUrl(String url) {
            this.oneUrl = url;
            return this;
        }

        public Builder restActions(List<ActionConfigDO> actions) {
            this.actions = actions;
            return this;
        }

        public Builder groupActions(List<ActionConfigDO> groupActions) {
            this.groupActions = groupActions;
            return this;
        }

        public Builder aggsUrl(String url) {
            this.aggsUrl = url;
            return this;
        }

        public Builder sortTieBreaker(String sortTieBreaker) {
            this.sortTieBreaker = sortTieBreaker;
            return this;
        }

        public Builder elasticFilterGroup(String elasticFilterGroup) {
            this.elasticFilterGroup = elasticFilterGroup;
            return this;
        }

        public Builder getUrl(String getUrl) {
            this.getUrl = getUrl;
            return this;
        }

        public Builder orderByActions(List<OrderByActionDO> orderByActions) {
            this.orderByActions = orderByActions;
            return this;
        }

    }

}
