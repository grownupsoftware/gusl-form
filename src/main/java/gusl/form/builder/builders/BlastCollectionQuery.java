package gusl.form.builder.builders;

import gusl.annotations.form.MediaType;
import gusl.annotations.form.page.OrderByActionDO;
import gusl.core.annotations.DocField;
import gusl.form.model.ActionConfigDO;
import gusl.form.model.BlastCollectionConfigDO;

import java.util.List;

import static gusl.form.builder.FormBuilder.getRowsConfig;
import static java.util.Objects.isNull;

public class BlastCollectionQuery {

    public static class Builder {

        @DocField(description = "Main row class")
        private Class rowClass;

        private String collectionName;

        private List<ActionConfigDO> tableActions;

        private List<ActionConfigDO> rowActions;

        protected List<ActionConfigDO> groupActions;
        private List<OrderByActionDO> orderByActions;

        private Boolean expandable = false;

        private String title;

        @DocField(description = "URL to get expanded component")
        private String getUrl;

        @DocField(description = "Class defining expanded row")
        private Class expandedClass;

        private MediaType mediaType;

        public Builder(Class rowClass) {
            this.rowClass = rowClass;
        }

        public BlastCollectionQuery.Builder getUrl(String getUrl) {
            this.getUrl = getUrl;
            return this;
        }

        public BlastCollectionQuery.Builder mediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public BlastCollectionQuery.Builder expandedClass(Class expandedClass) {
            this.expandedClass = expandedClass;
            return this;
        }

        public BlastCollectionQuery.Builder collectionName(String collectionName) {
            this.collectionName = collectionName;
            return this;
        }

        public BlastCollectionQuery.Builder title(String title) {
            this.title = title;
            return this;
        }

        public BlastCollectionQuery.Builder expandable(Boolean expandable) {
            this.expandable = expandable;
            return this;
        }

        public BlastCollectionQuery.Builder tableActions(List<ActionConfigDO> tableActions) {
            this.tableActions = tableActions;
            return this;
        }

        public BlastCollectionQuery.Builder rowActions(List<ActionConfigDO> actions) {
            this.rowActions = rowActions;
            return this;
        }

        public BlastCollectionQuery.Builder groupActions(List<ActionConfigDO> groupActions) {
            this.groupActions = groupActions;
            return this;
        }

        public BlastCollectionQuery.Builder orderByActions(List<OrderByActionDO> orderByActions) {
            this.orderByActions = orderByActions;
            return this;
        }


        public BlastCollectionConfigDO build() {
            if (isNull(title)) {
                title = rowClass.getSimpleName().replace("DTO", "").replace("DO", "");
            }
            return BlastCollectionConfigDO.builder()
                    .collectionName(collectionName)
                    .title(title)
                    .tableActions(tableActions)
                    .rowActions(rowActions)
                    .groupActions(groupActions)
                    .orderByActions(orderByActions)
                    .rows(getRowsConfig(rowClass))
                    .expandedRows(isNull(expandedClass) ? null : getRowsConfig(expandedClass))
                    .expandable(expandable)
                    .getUrl(getUrl)
                    .mediaType(mediaType)
                    .build();
        }
    }

}
